package com.xsis.batch23x.configuration;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServerSetup {
	
	@Bean
	public ModelMapper modelmapper() {
		ModelMapper modelmapper= new ModelMapper();
		modelmapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
		modelmapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return modelmapper;
		
	}
	
}
