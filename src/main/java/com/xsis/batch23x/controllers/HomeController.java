package com.xsis.batch23x.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.batch23x.models.RiwayatPendidikanModel;
import com.xsis.batch23x.repositories.PendidikanRepo;


@Controller
public class HomeController {
	
	@GetMapping("/")
	public String viewHome() {
		return "index";
	}
	@GetMapping("/menu1")
	public String menu1() {
		return "menu1/menu1";
	}
	
	@GetMapping("/menu2")
	public String menu2() {
		return "menu2/menu2";
	}
	@GetMapping("/menu3")
	public String menu3() {
		return "menu3/menu3";
	}
	
	@GetMapping("/menu4")
	public String menu4() {
		return "menu4/menu4";
	}
	@GetMapping("/skill_level")
	public String skill_level() {
		return "skill_level";
	}
	
	@GetMapping("/notetype")
	public String notetype() {
		return "notetype";
	}
	
	@GetMapping("/catatan")
	public String catatan() {
		return "catatan";
	}
	
	@GetMapping("/keahlian")
	public String keahlian() {
		return "keahlian";
	}
	
	@GetMapping("/form1")
	public String form1() {
		return "menu1/form1";
	}
	@GetMapping("/formorganisasi")
	public String formorganisasi() {
		return "menu2/formorganisasi";
	}
	
	@GetMapping("/sertifikasi")
	public String sertifikasi() {
		return "sertifikasi/sertifikasi";
	}
	
	@GetMapping("/biodata")
	public String biodata() {
		return "biodata/biodata";
	}
	
	
	@GetMapping("/viewmodal")
	public String viewmodal() {
		return "menu1/viewmodal";
	}
	@GetMapping("/modaldetailpelamar")
	public String modaldetailpelamar() {
		return "menu2/modaldetailpelamar";
	}
	
	@GetMapping("/pelatihan")
	public String modulshowpelatihan() {
		return "pelatihan/show";
	}
	@GetMapping("/keterangan/edit")
	public String editKeterangan() {
		return "keterangan/edit";
	}
	

	@GetMapping("/referensi/tambah")
	public String editReferensi() {
		return "referensi/tambah";
	}

	@GetMapping("/referensi/show")
	public String showReferensiOnLain() {
		return "referensi/show";
	}
	

	@GetMapping("/keterangan/show")
	public String showKeteranganOnLain() {
		return "keterangan/show";
	}
	
	
	@GetMapping("/pendidikan")
	public String pendidikan() {
		return "pendidikan/pendidikan";
	}
	
	@GetMapping("/profil")
	public String profil() {
		return "profil/profil";
	}
	
	@GetMapping("/role")
	public String role() {
		return "role/role";
	}
	
	@GetMapping("/pelatihan/tambah")
	public String ctrladdpelatihan() {
		return "pelatihan/tambah";
	}
	@GetMapping("/lainnya")
	public String modulshowlainnya() {
		return "lain-lain/show";
	}
	
	@GetMapping("/form2")
	public String form2() {
		return "menu1/form2";
	}
	
	@GetMapping("/send")
	public String send() {
		return "send";
	}
	
	@GetMapping("/religion")
	public String religion() {
		return "menu4/Religion/Religion";
	}
	
	@GetMapping("/sumber")
	public String sumberLoker() {
		return "menu4/SumberLoker";
	}
	
	@GetMapping("/leavereq")
	public String leave_request() {
		return "menu4/LeaveRequest";
	}
	
	@GetMapping("/approval")
	public String Timesheet_Approval() {
		return "menu4/TimeSheetApproval";
	}
	
	@GetMapping("/PenUndangan")
	public String PenUndangan() {
		return "penjadwalan/undangan";
	}
	@GetMapping("/employeL")
	public String employeL() {
		return "employe/employeL";
	}
	@GetMapping("/reschedule")
	public String reschedule() {
		return "penjadwalan/reschedule";
	}
	@GetMapping("/timesheet")
	public String rencana() {
		return "timesheet/timesheet";
	}
	@GetMapping("/timesheet_detail")
	public String timesheet_detail() {
		return "timesheet/timesheet_detail";
	}
	
	@GetMapping("/timesheet_submission")
	public String TimesheetSub() {
		return "timesheet/timesheet_submission";
	}
	@GetMapping("/timesheet_collection")
	public String TimesheetCollect() {
		return "timesheet/timesheet_collection";
	}
	
	@GetMapping("/employee_training")
	public String employee_training() {
		return "employee_training/employee_training";
	}
	
	@GetMapping("/prosesP")
	public String prosesP() {
		return "pelamar/prosesP";
	}
	
	@GetMapping("/viewJadwal")
	public String viewJadwal() {
		return "pelamar/viewJadwal";
	}
	
	@GetMapping("/viewUndangan")
	public String viewUndangan() {
		return "pelamar/viewUndangan";
	}
	
	@GetMapping("/modalD")
	public String modalD() {
		return "pelamar/modalD";
	}

	@GetMapping("/keluarga")
	public String keluarga() {
		return "keluarga/keluarga";
	}
	
	@GetMapping("/family_type")
	public String family_type() {
		return "family/familytype";
	}
}
