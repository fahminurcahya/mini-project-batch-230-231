package com.xsis.batch23x.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.batch23x.models.EducationLevelModel;
import com.xsis.batch23x.models.RiwayatPendidikanModel;
import com.xsis.batch23x.repositories.EducationLevelRepo;
import com.xsis.batch23x.repositories.PendidikanRepo;


@RestController
@RequestMapping(path="/api/pendidikan/",produces="application/json")
@CrossOrigin(origins="*")
public class PendidikanController {
	
	@Autowired
	private PendidikanRepo pendidikanRepo;
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute RiwayatPendidikanModel riwayatPendidikanModel, BindingResult bindingresult) {
//		RiwayatPendidikanModel riwayat = new RiwayatPendidikanModel();
//		riwayat.setCity(riwayatPendidikanModel.getCity());
		
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:../index/");
		} else {
			this.pendidikanRepo.save(riwayatPendidikanModel);
			return new ModelAndView("redirect:../index/");
		}
	}
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/category/form");
		RiwayatPendidikanModel riwayatPendidikanModel=this.pendidikanRepo.findById(id).orElse(null);
		view.addObject("riwayatPendidikan", riwayatPendidikanModel);
		return view;
	}

}
