package com.xsis.batch23x.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PengalamanKerjaController {
	

	@GetMapping("/pengalaman")
	public String pengalaman() {
		return "pengalaman";
	}
	
	@GetMapping("/proyek/form")
	public String proyekForm() {
		return "proyek/form";
	}

}
