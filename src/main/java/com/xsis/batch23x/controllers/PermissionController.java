package com.xsis.batch23x.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PermissionController {

	
	@GetMapping("/permission")
	public String permission() {
		return "permission";
	}
	
	@GetMapping("/permission/form")
	public String permissionform() {
		return "permission/form";
	}
	
	@GetMapping("/permission/history")
	public String permissionhistory() {
		return "permission/history";
	}
	
	@GetMapping("/permission/approval")
	public String permissionapproval() {
		return "permission/approval";
	}
	
}
