package com.xsis.batch23x.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.repositories.ProfilRepo;

@RestController
@RequestMapping(path="/api/profil/",produces="application/json")
@CrossOrigin(origins="*")
public class ProfilController {
	
	@Autowired
	private ProfilRepo profilRepo;

}
