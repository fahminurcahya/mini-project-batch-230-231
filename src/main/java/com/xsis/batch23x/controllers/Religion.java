package com.xsis.batch23x.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.batch23x.models.ReligionModel;
import com.xsis.batch23x.repositories.ReligionRepo;

/*@Controller
@RequestMapping(value="/religion/")
public class Religion {

	@Autowired
	private ReligionRepo rr;
	
	@GetMapping(value="")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/menu4/Religion/Religion");
		List<ReligionModel> rm = this.rr.findAll();
		mv.addObject("religion", rm);
		return mv;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView mv = new ModelAndView("/menu4/Religion/form");
		ReligionModel rm = new ReligionModel();
		mv.addObject("religion", rm);
		return mv;
	}
	
	@GetMapping(value="api")
	public ModelAndView api() {
		ModelAndView mv = new ModelAndView("/menu4/Religion/api");
		return mv;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute ReligionModel rm, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:../");
		} else {
			this.rr.save(rm);
			return new ModelAndView("redirect:../");
		}
	}
	
	@GetMapping(value="edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/menu4/Religion/form");
		ReligionModel rm = this.rr.findById(id).orElse(null);
		mv.addObject("religion", rm);
		return mv;
	}
	
	@GetMapping(value="delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/menu4/Religion/delete");
		ReligionModel rm = this.rr.findById(id).orElse(null);
		mv.addObject("religion", rm);
		return mv;
	}
	
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute ReligionModel rm, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:../");
		} else {
			this.rr.delete(rm);
			return new ModelAndView("redirect:../");
		}
	}*/
//}
