package com.xsis.batch23x.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ResourceProjectController {

	
	@GetMapping("/resource")
	public String res() {
		return "menu3/menu3";
	}
	
	@GetMapping("/resource/form")
	public String resForm() {
		return "menu3/form";
	}
	
	@GetMapping("/resource/detail")
	public String resDetail() {
		return "menu3/detail";
	}
	
	
}
