package com.xsis.batch23x.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.batch23x.models.SumberLokerModel;
import com.xsis.batch23x.repositories.SumberLokerRepo;

@Controller
@RequestMapping(value="/sumber")
public class SumberLokerController {

	@Autowired
	private SumberLokerRepo slr;
	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/menu4/SumberLoker");
		SumberLokerModel slm = new SumberLokerModel();
		mv.addObject("sumber", slm);
		return mv;
	}
	
	@PostMapping(value="/save")
	public ModelAndView save(@ModelAttribute SumberLokerModel slm, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:../");
		} else {
			this.slr.save(slm);
			return new ModelAndView("redirect:../");
		}
	}
}
