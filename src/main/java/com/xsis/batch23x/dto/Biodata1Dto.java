package com.xsis.batch23x.dto;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.xsis.batch23x.models.AddressModel;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.models.IdentityTypeModel;
import com.xsis.batch23x.models.MaritalStatusModel;
import com.xsis.batch23x.models.ReligionModel;

public class Biodata1Dto {

	private Long id;
	private String fullname;
	private String nickName;
	private String pob;
	private LocalDate dob;
	private Integer high;
	private Integer weight;
	private String nationality;
	private String ethnic;
	private String hobby;
	private String identityNo;
	private String email;
	private String phoneNumber1;
	private String phoneNumber2;
	private String parentPhoneNumber;
	private String childSequence;
	private String howManyBrothers;
	private String marriageYear;
	
	private Long id2;
	private String address1;
	private String postalCode1;
	private String rt1;
	private String rw1;
	private String kelurahan1;
	private String kecamatan1;
	private String region1;
	
	private String address2;
	private String postalCode2;
	private String rt2;
	private String rw2;
	private String kelurahan2;
	private String kecamatan2;
	private String region2;
	
	private List<AddressModel> address;
	private BiodataModel biodataId;
	
	private ReligionModel religion;
	private IdentityTypeModel identity;
	private MaritalStatusModel marital;
	
	public Long getId() {
		return id;
	}
	public String getFullname() {
		return fullname;
	}
	public String getNickName() {
		return nickName;
	}
	public String getPob() {
		return pob;
	}
	public LocalDate getDob() {
		return dob;
	}
	public Integer getHigh() {
		return high;
	}
	public Integer getWeight() {
		return weight;
	}
	public String getNationality() {
		return nationality;
	}
	public String getEthnic() {
		return ethnic;
	}
	public String getHobby() {
		return hobby;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public String getEmail() {
		return email;
	}
	public String getPhoneNumber1() {
		return phoneNumber1;
	}
	public String getPhoneNumber2() {
		return phoneNumber2;
	}
	public String getParentPhoneNumber() {
		return parentPhoneNumber;
	}
	public String getChildSequence() {
		return childSequence;
	}
	public String getHowManyBrothers() {
		return howManyBrothers;
	}
	public String getMarriageYear() {
		return marriageYear;
	}
	public Long getId2() {
		return id2;
	}
	public String getAddress1() {
		return address1;
	}
	public String getPostalCode1() {
		return postalCode1;
	}
	public String getRt1() {
		return rt1;
	}
	public String getRw1() {
		return rw1;
	}
	public String getKelurahan1() {
		return kelurahan1;
	}
	public String getKecamatan1() {
		return kecamatan1;
	}
	public String getRegion1() {
		return region1;
	}
	public String getAddress2() {
		return address2;
	}
	public String getPostalCode2() {
		return postalCode2;
	}
	public String getRt2() {
		return rt2;
	}
	public String getRw2() {
		return rw2;
	}
	public String getKelurahan2() {
		return kelurahan2;
	}
	public String getKecamatan2() {
		return kecamatan2;
	}
	public String getRegion2() {
		return region2;
	}
	public List<AddressModel> getAddress() {
		return address;
	}
	public BiodataModel getBiodataId() {
		return biodataId;
	}
	public ReligionModel getReligion() {
		return religion;
	}
	public IdentityTypeModel getIdentity() {
		return identity;
	}
	public MaritalStatusModel getMarital() {
		return marital;
	}
	
	
	
}
