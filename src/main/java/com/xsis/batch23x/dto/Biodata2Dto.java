package com.xsis.batch23x.dto;

import java.time.LocalDate;
import java.util.Date;

public class Biodata2Dto {
	
	private Long id;
	private String fullname;
	private String nickName;
	private String email;
	private String pob;
	private LocalDate dob;
	private boolean gender;
	private Integer high;
	private Integer weight;
	private String agama;
	private String nationality;
	private String ethnic;
	private String hobby;
	private String jenisId;
	private String identityNo;
	private String martial;
	private String marriageYear;
	
	private Long maritalId;
	private Long religionId;
	private Long identityTypeId;
	private String phoneNumber1;
	private String phoneNumber2;
	private String parentPhoneNumber;
	private String childSequence;
	private String howManyBrothers;
	
	private Long id2;
	private String address1;
	private String postalCode1;
	private String rt1;
	private String rw1;
	private String kelurahan1;
	private String kecamatan1;
	private String region1;
	
	private String address2;
	private String postalCode2;
	private String rt2;
	private String rw2;
	private String kelurahan2;
	private String kecamatan2;
	private String region2;
	private Date createdOn;
	private Long createdBy;
	
	public Biodata2Dto(Long id, String fullname, String nickName, String email, String pob, LocalDate dob,
			boolean gender, Integer high, Integer weight, String agama, String nationality, String ethnic, String hobby,
			String jenisId, String identityNo, String martial, String marriageYear) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.nickName = nickName;
		this.email = email;
		this.pob = pob;
		this.dob = dob;
		this.gender = gender;
		this.high = high;
		this.weight = weight;
		this.agama = agama;
		this.nationality = nationality;
		this.ethnic = ethnic;
		this.hobby = hobby;
		this.jenisId = jenisId;
		this.identityNo = identityNo;
		this.martial = martial;
		this.marriageYear = marriageYear;
	}
	
	
	public Biodata2Dto(Long id, String fullname, String nickName, String email, String pob, LocalDate dob,
			boolean gender, Integer high, Integer weight, String agama, String nationality, String ethnic, String hobby,
			String jenisId, String identityNo, String martial, String marriageYear, Long maritalId, Long religionId,
			Long identityTypeId, String phoneNumber1, String phoneNumber2, String parentPhoneNumber,
			String childSequence, String howManyBrothers, Long id2, String address1, String postalCode1, String rt1,
			String rw1, String kelurahan1, String kecamatan1, String region1, String address2, String postalCode2,
			String rt2, String rw2, String kelurahan2, String kecamatan2, String region2, Date createdOn, Long createdBy) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.nickName = nickName;
		this.email = email;
		this.pob = pob;
		this.dob = dob;
		this.gender = gender;
		this.high = high;
		this.weight = weight;
		this.agama = agama;
		this.nationality = nationality;
		this.ethnic = ethnic;
		this.hobby = hobby;
		this.jenisId = jenisId;
		this.identityNo = identityNo;
		this.martial = martial;
		this.marriageYear = marriageYear;
		this.maritalId = maritalId;
		this.religionId = religionId;
		this.identityTypeId = identityTypeId;
		this.phoneNumber1 = phoneNumber1;
		this.phoneNumber2 = phoneNumber2;
		this.parentPhoneNumber = parentPhoneNumber;
		this.childSequence = childSequence;
		this.howManyBrothers = howManyBrothers;
		this.id2 = id2;
		this.address1 = address1;
		this.postalCode1 = postalCode1;
		this.rt1 = rt1;
		this.rw1 = rw1;
		this.kelurahan1 = kelurahan1;
		this.kecamatan1 = kecamatan1;
		this.region1 = region1;
		this.address2 = address2;
		this.postalCode2 = postalCode2;
		this.rt2 = rt2;
		this.rw2 = rw2;
		this.kelurahan2 = kelurahan2;
		this.kecamatan2 = kecamatan2;
		this.region2 = region2;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
	}

	

	public Biodata2Dto(Long id) {
		super();
		this.id = id;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPob() {
		return pob;
	}
	public void setPob(String pob) {
		this.pob = pob;
	}
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	public boolean isGender() {
		return gender;
	}
	public void setGender(boolean gender) {
		this.gender = gender;
	}
	public Integer getHigh() {
		return high;
	}
	public void setHigh(Integer high) {
		this.high = high;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getEthnic() {
		return ethnic;
	}
	public void setEthnic(String ethnic) {
		this.ethnic = ethnic;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public String getJenisId() {
		return jenisId;
	}
	public void setJenisId(String jenisId) {
		this.jenisId = jenisId;
	}
	public String getIdentityNo() {
		return identityNo;
	}
	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}
	public String getMartial() {
		return martial;
	}
	public void setMartial(String martial) {
		this.martial = martial;
	}
	public String getMarriageYear() {
		return marriageYear;
	}
	public void setMarriageYear(String marriageYear) {
		this.marriageYear = marriageYear;
	}


	public Long getReligionId() {
		return religionId;
	}


	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}


	public Long getIdentityTypeId() {
		return identityTypeId;
	}


	public void setIdentityTypeId(Long identityTypeId) {
		this.identityTypeId = identityTypeId;
	}


	public String getPhoneNumber1() {
		return phoneNumber1;
	}


	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}


	public String getPhoneNumber2() {
		return phoneNumber2;
	}


	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}


	public String getParentPhoneNumber() {
		return parentPhoneNumber;
	}


	public void setParentPhoneNumber(String parentPhoneNumber) {
		this.parentPhoneNumber = parentPhoneNumber;
	}


	public String getChildSequence() {
		return childSequence;
	}


	public void setChildSequence(String childSequence) {
		this.childSequence = childSequence;
	}


	public String getHowManyBrothers() {
		return howManyBrothers;
	}


	public void setHowManyBrothers(String howManyBrothers) {
		this.howManyBrothers = howManyBrothers;
	}


	public Long getId2() {
		return id2;
	}


	public void setId2(Long id2) {
		this.id2 = id2;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getPostalCode1() {
		return postalCode1;
	}


	public void setPostalCode1(String postalCode1) {
		this.postalCode1 = postalCode1;
	}


	public String getRt1() {
		return rt1;
	}


	public void setRt1(String rt1) {
		this.rt1 = rt1;
	}


	public String getRw1() {
		return rw1;
	}


	public void setRw1(String rw1) {
		this.rw1 = rw1;
	}


	public String getKelurahan1() {
		return kelurahan1;
	}


	public void setKelurahan1(String kelurahan1) {
		this.kelurahan1 = kelurahan1;
	}


	public String getKecamatan1() {
		return kecamatan1;
	}


	public void setKecamatan1(String kecamatan1) {
		this.kecamatan1 = kecamatan1;
	}


	public String getRegion1() {
		return region1;
	}


	public void setRegion1(String region1) {
		this.region1 = region1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getPostalCode2() {
		return postalCode2;
	}


	public void setPostalCode2(String postalCode2) {
		this.postalCode2 = postalCode2;
	}


	public String getRt2() {
		return rt2;
	}


	public void setRt2(String rt2) {
		this.rt2 = rt2;
	}


	public String getRw2() {
		return rw2;
	}


	public void setRw2(String rw2) {
		this.rw2 = rw2;
	}


	public String getKelurahan2() {
		return kelurahan2;
	}


	public void setKelurahan2(String kelurahan2) {
		this.kelurahan2 = kelurahan2;
	}


	public String getKecamatan2() {
		return kecamatan2;
	}


	public void setKecamatan2(String kecamatan2) {
		this.kecamatan2 = kecamatan2;
	}


	public String getRegion2() {
		return region2;
	}


	public void setRegion2(String region2) {
		this.region2 = region2;
	}


	public Long getMaritalId() {
		return maritalId;
	}


	public void setMaritalId(Long maritalId) {
		this.maritalId = maritalId;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public Long getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}



	
	
	
	
	

}
