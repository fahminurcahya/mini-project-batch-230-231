package com.xsis.batch23x.dto;

import java.time.LocalDate;
import java.util.Date;

import com.xsis.batch23x.models.AddrbookModel;

public class BiodataDto {
	private Long id;
	private String fullname;
	private String nickName;
	private String email;
	private String phoneNumber1;
	private String major;
	private String schoolName;
	private Long idPendidikan;
	private Boolean isProcess;
	private Boolean isComplete;
	private Long createdBy;
	private Date createdOn;
	private Long companyId;
	private String token;
	private Long maritalStatusId;
	private String parentPhoneNumber;
	private String identityNo;
	private Long identityTypeId;
	private Long religionId;
	private Boolean gender;
	private LocalDate dob;
	private String pob;
	private String graduationYear;
	
	





	public BiodataDto(Long id, String fullname, String nickName, String email, String phoneNumber1, String major,
			String schoolName, Long idPendidikan, String graduationYear) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.nickName = nickName;
		this.email = email;
		this.phoneNumber1 = phoneNumber1;
		this.major = major;
		this.schoolName = schoolName;
		this.idPendidikan = idPendidikan;
		this.graduationYear = graduationYear;
	}




	public String getGraduationYear() {
		return graduationYear;
	}




	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}




	//construk for proses pelamar
	public BiodataDto(Long id, String fullname, String nickName, String email, String phoneNumber1, String major,
			String schoolName, Long idPendidikan, Boolean isProcess, Boolean isComplete,
			Long createdBy,Date createdOn,Long companyId,String token,Long maritalStatusId
			,String parentPhoneNumber,String identityNo, Long identityTypeId
			,Long religionId, Boolean gender,LocalDate dob, String pob) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.nickName = nickName;
		this.email = email;
		this.phoneNumber1 = phoneNumber1;
		this.major = major;
		this.schoolName = schoolName;
		this.idPendidikan = idPendidikan;
		this.isProcess = isProcess;
		this.isComplete = isComplete;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.companyId = companyId;
		this.token= token;
		this.maritalStatusId= maritalStatusId;
		this.parentPhoneNumber= parentPhoneNumber;
		this.identityNo= identityNo;
		this.identityTypeId = identityTypeId;
		this.religionId = religionId;
		this.gender = gender;
		this.dob = dob;
		this.pob = pob;
	}

	

	
public String getToken() {
		return token;
	}




	public void setToken(String token) {
		this.token = token;
	}




	public Long getMaritalStatusId() {
		return maritalStatusId;
	}




	public void setMaritalStatusId(Long maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}




	public String getParentPhoneNumber() {
		return parentPhoneNumber;
	}




	public void setParentPhoneNumber(String parentPhoneNumber) {
		this.parentPhoneNumber = parentPhoneNumber;
	}




	public String getIdentityNo() {
		return identityNo;
	}




	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}




	public Long getIdentityTypeId() {
		return identityTypeId;
	}




	public void setIdentityTypeId(Long identityTypeId) {
		this.identityTypeId = identityTypeId;
	}




	public Long getReligionId() {
		return religionId;
	}




	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}




	public Boolean getGender() {
		return gender;
	}




	public void setGender(Boolean gender) {
		this.gender = gender;
	}




	public LocalDate getDob() {
		return dob;
	}




	public void setDob(LocalDate dob) {
		this.dob = dob;
	}




	public String getPob() {
		return pob;
	}




	public void setPob(String pob) {
		this.pob = pob;
	}




public Long getCompanyId() {
		return companyId;
	}




	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}




public Long getCreatedBy() {
		return createdBy;
	}




	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}




	public Date getCreatedOn() {
		return createdOn;
	}




	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}




public Boolean getIsProcess() {
		return isProcess;
	}




	public void setIsProcess(Boolean isProcess) {
		this.isProcess = isProcess;
	}




	public Boolean getIsComplete() {
		return isComplete;
	}




	public void setIsComplete(Boolean isComplete) {
		this.isComplete = isComplete;
	}




	//construk	
	public BiodataDto(Long id, String fullname, String nickName, String email, String phoneNumber1, String major,
			String schoolName,Long idPendidikan) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.nickName = nickName;
		this.email = email;
		this.phoneNumber1 = phoneNumber1;
		this.major = major;
		this.schoolName = schoolName;
		this.idPendidikan = idPendidikan;
		
	}

	//geter and seter
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber1() {
		return phoneNumber1;
	}
	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public Long getIdPendidikan() {
		return idPendidikan;
	}
	public void setIdPendidikan(Long idPendidikan) {
		this.idPendidikan = idPendidikan;
	}

	
	
	
}
