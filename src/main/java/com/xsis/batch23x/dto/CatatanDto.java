package com.xsis.batch23x.dto;

import java.util.Date;

public class CatatanDto {
	
	private Long id;
	private String title;
	private String notes;
	private Long biodataId;
	private Long noteTypeId;
	private String NameTypeNote;
	private String nickName;
	private Long createdBy;
	private Date createdOn;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public Long getNoteTypeId() {
		return noteTypeId;
	}
	public void setNoteTypeId(Long noteTypeId) {
		this.noteTypeId = noteTypeId;
	}
	
	public String getNameTypeNote() {
		return NameTypeNote;
	}
	public void setNameTypeNote(String nameTypeNote) {
		this.NameTypeNote = nameTypeNote;
	}
	
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public CatatanDto(Long id, String title, String notes, Long biodataId, Long noteTypeId, String nameTypeNote,
			String nickName, Long createdBy, Date createdOn) {
		super();
		this.id = id;
		this.title = title;
		this.notes = notes;
		this.biodataId = biodataId;
		this.noteTypeId = noteTypeId;
		NameTypeNote = nameTypeNote;
		this.nickName = nickName;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
	}

}
