package com.xsis.batch23x.dto;

public class CutiComboDto {
	
	private Long id;
	private String karyawan;
	
	
	
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getKaryawan() {
		return karyawan;
	}



	public void setKaryawan(String karyawan) {
		this.karyawan = karyawan;
	}



	public CutiComboDto(Long id, String karyawan) {
		super();
		this.id = id;
		this.karyawan = karyawan;
	}
	
	

}
