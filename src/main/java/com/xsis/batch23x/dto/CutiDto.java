package com.xsis.batch23x.dto;

import java.util.Date;

public class CutiDto {
	
	private Long id;
	private Date modifiedOn;
	private String karyawan;
	private Long employeeId;	
	private Integer regularQuota;
	private Integer annualCollectiveLeave;
	private Integer leaveAlreadyTaken;
	private Integer tahunLalu;
	private Long createdBy;
	private Date createdOn;
	

	public CutiDto(Long id, Date modifiedOn, String karyawan, Long employeeId, Integer regularQuota,
			Integer annualCollectiveLeave, Integer leaveAlreadyTaken, Integer tahunLalu, Long createdBy, Date createdOn) {
		super();
		this.id = id;
		this.modifiedOn = modifiedOn;
		this.karyawan = karyawan;
		this.employeeId = employeeId;
		this.regularQuota = regularQuota;
		this.annualCollectiveLeave = annualCollectiveLeave;
		this.leaveAlreadyTaken = leaveAlreadyTaken;
		this.tahunLalu = tahunLalu;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
	}







	public CutiDto(Long id, Date modifiedOn, String karyawan, Long employeeId, Integer regularQuota,
			Integer annualCollectiveLeave, Integer leaveAlreadyTaken, Long createdBy, Date createdOn) {
		super();
		this.id = id;
		this.modifiedOn = modifiedOn;
		this.karyawan = karyawan;
		this.employeeId = employeeId;
		this.regularQuota = regularQuota;
		this.annualCollectiveLeave = annualCollectiveLeave;
		this.leaveAlreadyTaken = leaveAlreadyTaken;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
	}







	public Integer getTahunLalu() {
		return tahunLalu;
	}



	public void setTahunLalu(Integer tahunLalu) {
		this.tahunLalu = tahunLalu;
	}


	public CutiDto(Long id, Date modifiedOn, String karyawan, Long employeeId, Integer regularQuota,
			Integer annualCollectiveLeave, Integer leaveAlreadyTaken, Integer tahunLalu) {
		super();
		this.id = id;
		this.modifiedOn = modifiedOn;
		this.karyawan = karyawan;
		this.employeeId = employeeId;
		this.regularQuota = regularQuota;
		this.annualCollectiveLeave = annualCollectiveLeave;
		this.leaveAlreadyTaken = leaveAlreadyTaken;
		this.tahunLalu = tahunLalu;
	}






	public CutiDto(Long id, Date modifiedOn, String karyawan, Long employeeId, Integer regularQuota,
			Integer annualCollectiveLeave, Integer leaveAlreadyTaken) {
		super();
		this.id = id;
		this.modifiedOn = modifiedOn;
		this.karyawan = karyawan;
		this.employeeId = employeeId;
		this.regularQuota = regularQuota;
		this.annualCollectiveLeave = annualCollectiveLeave;
		this.leaveAlreadyTaken = leaveAlreadyTaken;
	}
	
	




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getRegularQuota() {
		return regularQuota;
	}

	public void setRegularQuota(Integer regularQuota) {
		this.regularQuota = regularQuota;
	}

	public Integer getAnnualCollectiveLeave() {
		return annualCollectiveLeave;
	}

	public void setAnnualCollectiveLeave(Integer annualCollectiveLeave) {
		this.annualCollectiveLeave = annualCollectiveLeave;
	}

	public Integer getLeaveAlreadyTaken() {
		return leaveAlreadyTaken;
	}

	public void setLeaveAlreadyTaken(Integer leaveAlreadyTaken) {
		this.leaveAlreadyTaken = leaveAlreadyTaken;
	}

	public String getKaryawan() {
		return karyawan;
	}

	public void setKaryawan(String karyawan) {
		this.karyawan = karyawan;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	
	

}
