package com.xsis.batch23x.dto;

import java.time.LocalDate;

public class EmployeeTrainingDto {
	
	private Long id; 
	private Long certificationTypeId;
	private Long idTipeSertifikasi;
	private String certificationName;
	private Long employeeId;
	private Long idKaryawan;
	private Long biodataId;
	private Long idBiodata;
	private String nickname;
	private String status;
	private Long trainingId;
	private Long idPelatihan;
	private String trainingName;
	private Long trainingOrganizerId;
	private Long idPenyelenggara;
	private String organizerName;
	private Long trainingTypeId;
	private Long idTipePelatihan;
	private String trainingTypename;
	private LocalDate trainingDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCertificationTypeId() {
		return certificationTypeId;
	}
	public void setCertificationTypeId(Long certificationTypeId) {
		this.certificationTypeId = certificationTypeId;
	}
	public Long getIdTipeSertifikasi() {
		return idTipeSertifikasi;
	}
	public void setIdTipeSertifikasi(Long idTipeSertifikasi) {
		this.idTipeSertifikasi = idTipeSertifikasi;
	}
	public String getCertificationName() {
		return certificationName;
	}
	public void setCertificationName(String certificationName) {
		this.certificationName = certificationName;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public Long getIdKaryawan() {
		return idKaryawan;
	}
	public void setIdKaryawan(Long idKaryawan) {
		this.idKaryawan = idKaryawan;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public Long getIdBiodata() {
		return idBiodata;
	}
	public void setIdBiodata(Long idBiodata) {
		this.idBiodata = idBiodata;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}
	public Long getIdPelatihan() {
		return idPelatihan;
	}
	public void setIdPelatihan(Long idPelatihan) {
		this.idPelatihan = idPelatihan;
	}
	public String getTrainingName() {
		return trainingName;
	}
	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}
	public Long getTrainingOrganizerId() {
		return trainingOrganizerId;
	}
	public void setTrainingOrganizerId(Long trainingOrganizerId) {
		this.trainingOrganizerId = trainingOrganizerId;
	}
	public Long getIdPenyelenggara() {
		return idPenyelenggara;
	}
	public void setIdPenyelenggara(Long idPenyelenggara) {
		this.idPenyelenggara = idPenyelenggara;
	}
	public String getOrganizerName() {
		return organizerName;
	}
	public void setOrganizerName(String organizerName) {
		this.organizerName = organizerName;
	}
	public Long getTrainingTypeId() {
		return trainingTypeId;
	}
	public void setTrainingTypeId(Long trainingTypeId) {
		this.trainingTypeId = trainingTypeId;
	}
	public Long getIdTipePelatihan() {
		return idTipePelatihan;
	}
	public void setIdTipePelatihan(Long idTipePelatihan) {
		this.idTipePelatihan = idTipePelatihan;
	}
	public String getTrainingTypename() {
		return trainingTypename;
	}
	public void setTrainingTypename(String trainingTypename) {
		this.trainingTypename = trainingTypename;
	}
	public LocalDate getTrainingDate() {
		return trainingDate;
	}
	public void setTrainingDate(LocalDate trainingDate) {
		this.trainingDate = trainingDate;
	}
	
	public EmployeeTrainingDto(Long id, Long certificationTypeId, Long idTipeSertifikasi, String certificationName,
			Long employeeId, Long idKaryawan, Long biodataId, Long idBiodata, String nickname, String status,
			Long trainingId, Long idPelatihan, String trainingName, Long trainingOrganizerId, Long idPenyelenggara,
			String organizerName, Long trainingTypeId, Long idTipePelatihan, String trainingTypename,
			LocalDate trainingDate) {
		super();
		this.id = id;
		this.certificationTypeId = certificationTypeId;
		this.idTipeSertifikasi = idTipeSertifikasi;
		this.certificationName = certificationName;
		this.employeeId = employeeId;
		this.idKaryawan = idKaryawan;
		this.biodataId = biodataId;
		this.idBiodata = idBiodata;
		this.nickname = nickname;
		this.status = status;
		this.trainingId = trainingId;
		this.idPelatihan = idPelatihan;
		this.trainingName = trainingName;
		this.trainingOrganizerId = trainingOrganizerId;
		this.idPenyelenggara = idPenyelenggara;
		this.organizerName = organizerName;
		this.trainingTypeId = trainingTypeId;
		this.idTipePelatihan = idTipePelatihan;
		this.trainingTypename = trainingTypename;
		this.trainingDate = trainingDate;
	}
	
	
}
