package com.xsis.batch23x.dto;

public class KeahlianDto {
	private Long id;
	private Long biodataId;
	private String skillName;
	private Long skillLevelId;
	private String notes;
	private String name;
	
	public KeahlianDto(Long id, Long biodataId, String skillName, Long skillLevelId, String notes, String name) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.skillName = skillName;
		this.skillLevelId = skillLevelId;
		this.name = name;
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public Long getSkillLevelId() {
		return skillLevelId;
	}

	public void setSkillLevelId(Long skillLevelId) {
		this.skillLevelId = skillLevelId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
