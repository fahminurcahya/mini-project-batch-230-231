package com.xsis.batch23x.dto;

import java.time.LocalDate;

public class KeluargaDto {
	
	private Long id;
	private Long biodataId;
	private LocalDate dob;
	private Long educationLevelId;
	private Long familyrelationId;
	private Long familyTreeTypeId;
	private Boolean gender;
	private String job;
	private String name;
	private Long idBiodata;
	private Long idEducationLevel;
	private Long idFamilyRelation;
	private Long idFamilyTreeType;
	private String familyTreeTypeName;
	private String familyRelationName;
	private String educationLevelName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	public Long getEducationLevelId() {
		return educationLevelId;
	}
	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}
	public Long getFamilyrelationId() {
		return familyrelationId;
	}
	public void setFamilyrelationId(Long familyrelationId) {
		this.familyrelationId = familyrelationId;
	}
	public Long getFamilyTreeTypeId() {
		return familyTreeTypeId;
	}
	public void setFamilyTreeTypeId(Long familyTreeTypeId) {
		this.familyTreeTypeId = familyTreeTypeId;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getIdBiodata() {
		return idBiodata;
	}
	public void setIdBiodata(Long idBiodata) {
		this.idBiodata = idBiodata;
	}
	public Long getIdEducationLevel() {
		return idEducationLevel;
	}
	public void setIdEducationLevel(Long idEducationLevel) {
		this.idEducationLevel = idEducationLevel;
	}
	public Long getIdFamilyRelation() {
		return idFamilyRelation;
	}
	public void setIdFamilyRelation(Long idFamilyRelation) {
		this.idFamilyRelation = idFamilyRelation;
	}
	public Long getIdFamilyTreeType() {
		return idFamilyTreeType;
	}
	public void setIdFamilyTreeType(Long idFamilyTreeType) {
		this.idFamilyTreeType = idFamilyTreeType;
	}
	public String getFamilyTreeTypeName() {
		return familyTreeTypeName;
	}
	public void setFamilyTreeTypeName(String familyTreeTypeName) {
		this.familyTreeTypeName = familyTreeTypeName;
	}
	public String getFamilyRelationName() {
		return familyRelationName;
	}
	public void setFamilyRelationName(String familyRelationName) {
		this.familyRelationName = familyRelationName;
	}
	public String getEducationLevelName() {
		return educationLevelName;
	}
	public void setEducationLevelName(String educationLevelName) {
		this.educationLevelName = educationLevelName;
	}
	
	public KeluargaDto(Long id, Long biodataId, LocalDate dob, Long educationLevelId, Long familyrelationId,
			Long familyTreeTypeId, Boolean gender, String job, String name, Long idBiodata, Long idEducationLevel,
			Long idFamilyRelation, Long idFamilyTreeType, String familyTreeTypeName, String familyRelationName,
			String educationLevelName) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.dob = dob;
		this.educationLevelId = educationLevelId;
		this.familyrelationId = familyrelationId;
		this.familyTreeTypeId = familyTreeTypeId;
		this.gender = gender;
		this.job = job;
		this.name = name;
		this.idBiodata = idBiodata;
		this.idEducationLevel = idEducationLevel;
		this.idFamilyRelation = idFamilyRelation;
		this.idFamilyTreeType = idFamilyTreeType;
		this.familyTreeTypeName = familyTreeTypeName;
		this.familyRelationName = familyRelationName;
		this.educationLevelName = educationLevelName;
	}
}
