package com.xsis.batch23x.dto;

import java.util.Date;

public class PendidikanDto {
	
	private Long id;
	private Long biodataId;
	private Double gpa;
	private String schoolName;
	private String country;
	private String entryYear;
	private String graduationYear;
	private String major;
	private Long educationLevelId;
	private String NameEducation;
	private Long createdBy;
	private Date createdOn;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public Double getGpa() {
		return gpa;
	}
	public void setGpa(Double gpa) {
		this.gpa = gpa;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEntryYear() {
		return entryYear;
	}
	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public Long getEducationLevelId() {
		return educationLevelId;
	}
	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}
	public String getNameEducation() {
		return NameEducation;
	}
	public void setNameEducation(String nameEducation) {
		NameEducation = nameEducation;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public PendidikanDto(Long id, Long biodataId, Double gpa, String schoolName, String country, String entryYear,
			String graduationYear, String major, Long educationLevelId, String nameEducation, Long createdBy,
			Date createdOn) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.gpa = gpa;
		this.schoolName = schoolName;
		this.country = country;
		this.entryYear = entryYear;
		this.graduationYear = graduationYear;
		this.major = major;
		this.educationLevelId = educationLevelId;
		NameEducation = nameEducation;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
	}


	
}
