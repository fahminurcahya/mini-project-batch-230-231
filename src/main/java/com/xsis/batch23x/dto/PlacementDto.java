package com.xsis.batch23x.dto;

import java.util.Date;

public class PlacementDto {

	private Long id;
	private String name;
	private String userEmail;
	private boolean isPlacementActive;
	private Long employeeId;
	private String fuulname;
	public PlacementDto(Long id, String name, String userEmail, boolean isPlacementActive, Long employeeId,
			String fuulname) {
		super();
		this.id = id;
		this.name = name;
		this.userEmail = userEmail;
		this.isPlacementActive = isPlacementActive;
		this.employeeId = employeeId;
		this.fuulname = fuulname;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public boolean isPlacementActive() {
		return isPlacementActive;
	}
	public void setPlacementActive(boolean isPlacementActive) {
		this.isPlacementActive = isPlacementActive;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public String getFuulname() {
		return fuulname;
	}
	public void setFuulname(String fuulname) {
		this.fuulname = fuulname;
	}
	
	
	
	
	
}
