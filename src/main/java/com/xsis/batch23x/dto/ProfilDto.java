package com.xsis.batch23x.dto;

import java.time.LocalDate;

public class ProfilDto {
	
	private Long id;
	private String fullname;
	private LocalDate dob;
	private Boolean gender;
	private Long PendidikanBioId;
	private String entryYear;
	private String graduationYear;
	private String schoolName;
	private String country;
	private String major;
	private Double gpa;
	private Long PekerjaanBioId;
	private String joinYear;
	private String resignYear;
	private String lastPosition;
	private String companyName;
	private Long KeahlianBioId;
//	private Long skillLevelId;
	private String skillName;
	private Long PelatihanBioId;
	private String trainingName;
	private String organizer;
	private String trainingYear;
	private Long SertifikasiBioId;
	private String certificateName;
	private String publisher;
	private Integer validStartYear;
	private Integer untilYear;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public LocalDate getDob() {
		return dob;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public Long getPendidikanBioId() {
		return PendidikanBioId;
	}
	public void setPendidikanBioId(Long pendidikanBioId) {
		PendidikanBioId = pendidikanBioId;
	}
	public String getEntryYear() {
		return entryYear;
	}
	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}
	public String getGraduationYear() {
		return graduationYear;
	}
	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public Double getGpa() {
		return gpa;
	}
	public void setGpa(Double gpa) {
		this.gpa = gpa;
	}
	public Long getPekerjaanBioId() {
		return PekerjaanBioId;
	}
	public void setPekerjaanBioId(Long pekerjaanBioId) {
		PekerjaanBioId = pekerjaanBioId;
	}
	public String getJoinYear() {
		return joinYear;
	}
	public void setJoinYear(String joinYear) {
		this.joinYear = joinYear;
	}
	public String getResignYear() {
		return resignYear;
	}
	public void setResignYear(String resignYear) {
		this.resignYear = resignYear;
	}
	public String getLastPosition() {
		return lastPosition;
	}
	public void setLastPosition(String lastPosition) {
		this.lastPosition = lastPosition;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getKeahlianBioId() {
		return KeahlianBioId;
	}
	public void setKeahlianBioId(Long keahlianBioId) {
		KeahlianBioId = keahlianBioId;
	}
//	public Long getSkillLevelId() {
//		return skillLevelId;
//	}
//	public void setSkillLevelId(Long skillLevelId) {
//		this.skillLevelId = skillLevelId;
//	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public Long getPelatihanBioId() {
		return PelatihanBioId;
	}
	public void setPelatihanBioId(Long pelatihanBioId) {
		PelatihanBioId = pelatihanBioId;
	}
	public String getTrainingName() {
		return trainingName;
	}
	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}
	public String getOrganizer() {
		return organizer;
	}
	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}
	public String getTrainingYear() {
		return trainingYear;
	}
	public void setTrainingYear(String trainingYear) {
		this.trainingYear = trainingYear;
	}
	public Long getSertifikasiBioId() {
		return SertifikasiBioId;
	}
	public void setSertifikasiBioId(Long sertifikasiBioId) {
		SertifikasiBioId = sertifikasiBioId;
	}
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public Integer getValidStartYear() {
		return validStartYear;
	}
	public void setValidStartYear(Integer validStartYear) {
		this.validStartYear = validStartYear;
	}
	public Integer getUntilYear() {
		return untilYear;
	}
	public void setUntilYear(Integer untilYear) {
		this.untilYear = untilYear;
	}
	public ProfilDto(Long id, String fullname, LocalDate dob, Boolean gender, Long pendidikanBioId, String entryYear,
			String graduationYear, String schoolName, String country, String major, Double gpa, Long pekerjaanBioId,
			String joinYear, String resignYear, String lastPosition, String companyName, Long keahlianBioId,
			String skillName, Long pelatihanBioId, String trainingName, String organizer,
			String trainingYear, Long sertifikasiBioId, String certificateName, String publisher,
			Integer validStartYear, Integer untilYear) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.dob = dob;
		this.gender = gender;
		PendidikanBioId = pendidikanBioId;
		this.entryYear = entryYear;
		this.graduationYear = graduationYear;
		this.schoolName = schoolName;
		this.country = country;
		this.major = major;
		this.gpa = gpa;
		PekerjaanBioId = pekerjaanBioId;
		this.joinYear = joinYear;
		this.resignYear = resignYear;
		this.lastPosition = lastPosition;
		this.companyName = companyName;
		KeahlianBioId = keahlianBioId;
//		this.skillLevelId = skillLevelId;
		this.skillName = skillName;
		PelatihanBioId = pelatihanBioId;
		this.trainingName = trainingName;
		this.organizer = organizer;
		this.trainingYear = trainingYear;
		SertifikasiBioId = sertifikasiBioId;
		this.certificateName = certificateName;
		this.publisher = publisher;
		this.validStartYear = validStartYear;
		this.untilYear = untilYear;
	}
	
	
	
}
