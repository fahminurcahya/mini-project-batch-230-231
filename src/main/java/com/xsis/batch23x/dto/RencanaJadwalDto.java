package com.xsis.batch23x.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class RencanaJadwalDto {
	
	private Long id;
	private LocalDate scheduleDate;
	private String time;
	private String schName;
	private String location;
	private Long ro;
	private Long tro;
	private Long idDetail;
	private String scheduleCode;
	private String namaRo;
	private String namaTro;
	
	
	
	
	public RencanaJadwalDto(Long id, LocalDate scheduleDate, String time, String schName, String location, Long ro,
			Long tro, Long idDetail,String scheduleCode, String namaRo, String namaTro) {
		super();
		this.id = id;
		this.scheduleDate = scheduleDate;
		this.time = time;
		this.schName = schName;
		this.location = location;
		this.ro = ro;
		this.tro = tro;
		this.idDetail = idDetail;
		this.scheduleCode = scheduleCode;
		this.namaRo = namaRo;
		this.namaTro = namaTro;
		
		
	}
	
	
	public String getScheduleCode() {
		return scheduleCode;
	}


	public void setScheduleCode(String scheduleCode) {
		this.scheduleCode = scheduleCode;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDate getScheduleDate() {
		return scheduleDate;
	}
	public void setScheduleDate(LocalDate scheduleDate) {
		this.scheduleDate = scheduleDate;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getSchName() {
		return schName;
	}
	public void setSchName(String schName) {
		this.schName = schName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Long getRo() {
		return ro;
	}
	public void setRo(Long ro) {
		this.ro = ro;
	}
	public Long getTro() {
		return tro;
	}
	public void setTro(Long tro) {
		this.tro = tro;
	}
	public Long getIdDetail() {
		return idDetail;
	}
	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}
	public String getNamaRo() {
		return namaRo;
	}
	public void setNamaRo(String namaRo) {
		this.namaRo = namaRo;
	}
	public String getNamaTro() {
		return namaTro;
	}
	public void setNamaTro(String namaTro) {
		this.namaTro = namaTro;
	}
	
	
	
	
}
