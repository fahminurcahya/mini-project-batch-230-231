package com.xsis.batch23x.dto;

import java.time.LocalDate;
import java.util.Date;

public class RescheduleDto {

	private Long id;
	private String invitationCode;
	private LocalDate invitationDate;
	private Long scheduleTypeId;
	private Long ro;
	private String roName;
	private Long tro;
	private String  troName;
	private String jenisUnd;
	private String otherRoTro;
	private String location;
	private String time;
	private Long idDetail;
	private Long biodataId;
	private Long undanganId;
	private String notes;
	private Long idBio;
	private String fullname;
	private String email;
	
	public RescheduleDto(Long id, String invitationCode, LocalDate invitationDate, Long scheduleTypeId, Long ro,
			Long tro, String otherRoTro, String location, String time, Long idDetail, Long biodataId, Long undanganId,
			String notes, Long idBio, String fullname) {
		super();
		this.id = id;
		this.invitationCode = invitationCode;
		this.invitationDate = invitationDate;
		this.scheduleTypeId = scheduleTypeId;
		this.ro = ro;
		this.tro = tro;
		this.otherRoTro = otherRoTro;
		this.location = location;
		this.time = time;
		this.idDetail = idDetail;
		this.biodataId = biodataId;
		this.undanganId = undanganId;
		this.notes = notes;
		this.idBio = idBio;
		this.fullname = fullname;
	}
	
	
	public RescheduleDto(Long id, Long ro, Long tro) {
		super();
		this.id = id;
		this.ro = ro;
		this.tro = tro;
	}


	public RescheduleDto(Long idDetail) {
		super();
		this.idDetail = idDetail;
	}
	
	


	public RescheduleDto(String invitationCode, LocalDate invitationDate, String roName, String troName,
			String jenisUnd, String otherRoTro, String location, String time, String notes, String fullname, String email) {
		super();
		this.invitationCode = invitationCode;
		this.invitationDate = invitationDate;
		this.roName = roName;
		this.troName = troName;
		this.jenisUnd = jenisUnd;
		this.otherRoTro = otherRoTro;
		this.location = location;
		this.time = time;
		this.notes = notes;
		this.fullname = fullname;
		this.email = email;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInvitationCode() {
		return invitationCode;
	}
	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}
	public LocalDate getInvitationDate() {
		return invitationDate;
	}
	public void setInvitationDate(LocalDate invitationDate) {
		this.invitationDate = invitationDate;
	}
	public Long getScheduleTypeId() {
		return scheduleTypeId;
	}
	public void setScheduleTypeId(Long scheduleTypeId) {
		this.scheduleTypeId = scheduleTypeId;
	}
	public Long getRo() {
		return ro;
	}
	public void setRo(Long ro) {
		this.ro = ro;
	}
	public Long getTro() {
		return tro;
	}
	public void setTro(Long tro) {
		this.tro = tro;
	}
	public String getOtherRoTro() {
		return otherRoTro;
	}
	public void setOtherRoTro(String otherRoTro) {
		this.otherRoTro = otherRoTro;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Long getIdDetail() {
		return idDetail;
	}
	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public Long getUndanganId() {
		return undanganId;
	}
	public void setUndanganId(Long undanganId) {
		this.undanganId = undanganId;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Long getIdBio() {
		return idBio;
	}
	public void setIdBio(Long idBio) {
		this.idBio = idBio;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}


	public String getRoName() {
		return roName;
	}


	public void setRoName(String roName) {
		this.roName = roName;
	}


	public String getTroName() {
		return troName;
	}


	public void setTroName(String troName) {
		this.troName = troName;
	}


	public String getJenisUnd() {
		return jenisUnd;
	}


	public void setJenisUnd(String jenisUnd) {
		this.jenisUnd = jenisUnd;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
