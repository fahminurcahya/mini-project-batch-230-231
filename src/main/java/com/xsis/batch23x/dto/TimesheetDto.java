package com.xsis.batch23x.dto;

import java.util.Date;

public class TimesheetDto {
	private Long id;
	private String status;
	private Date timesheetDate;
	private boolean overtime;
	private String userApproval;
	private String eroStatus;
	private String activity;
	private String start;
	private String selesai;
	private String startOt;
	private String endOt;
	private Date submittedOn;
	private Date sentOn;
	private Date approvedOn;
	private Date collectedOn;
	private String name;
	private Long placementId;
	private Date createdOn;
	private Long createdBy;
	
	public TimesheetDto(Long id, String status, Date timesheetDate, boolean overtime, String userApproval,
			String eroStatus, String activity, String start, String selesai, String startOt, String endOt,
			Date submittedOn, Date sentOn, Date approvedOn, Date collectedOn, String name, Long placementId, Date createdOn, Long createdBy) {
		super();
		this.id = id;
		this.status = status;
		this.timesheetDate = timesheetDate;
		this.overtime = overtime;
		this.userApproval = userApproval;
		this.eroStatus = eroStatus;
		this.activity = activity;
		this.start = start;
		this.selesai = selesai;
		this.startOt = startOt;
		this.endOt = endOt;
		this.submittedOn = submittedOn;
		this.sentOn = sentOn;
		this.approvedOn = approvedOn;
		this.collectedOn = collectedOn;
		this.name = name;
		this.placementId = placementId;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
	}
	
	
	
	public TimesheetDto(Long id, String status, Date timesheetDate, boolean overtime, String userApproval,
			String eroStatus, String activity, String start, String selesai, String startOt, String endOt,
			Date submittedOn, Date sentOn, Date approvedOn, Date collectedOn, String name) {
		super();
		this.id = id;
		this.status = status;
		this.timesheetDate = timesheetDate;
		this.overtime = overtime;
		this.userApproval = userApproval;
		this.eroStatus = eroStatus;
		this.activity = activity;
		this.start = start;
		this.selesai = selesai;
		this.startOt = startOt;
		this.endOt = endOt;
		this.submittedOn = submittedOn;
		this.sentOn = sentOn;
		this.approvedOn = approvedOn;
		this.collectedOn = collectedOn;
		this.name = name;
	}
	
	


	public TimesheetDto(Long id) {
		super();
		this.id = id;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getTimesheetDate() {
		return timesheetDate;
	}
	public void setTimesheetDate(Date timesheetDate) {
		this.timesheetDate = timesheetDate;
	}
	public boolean isOvertime() {
		return overtime;
	}
	public void setOvertime(boolean overtime) {
		this.overtime = overtime;
	}
	public String getUserApproval() {
		return userApproval;
	}
	public void setUserApproval(String userApproval) {
		this.userApproval = userApproval;
	}
	public String getEroStatus() {
		return eroStatus;
	}
	public void setEroStatus(String eroStatus) {
		this.eroStatus = eroStatus;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getSelesai() {
		return selesai;
	}
	public void setSelesai(String selesai) {
		this.selesai = selesai;
	}
	public String getStartOt() {
		return startOt;
	}
	public void setStartOt(String startOt) {
		this.startOt = startOt;
	}
	public String getEndOt() {
		return endOt;
	}
	public void setEndOt(String endOt) {
		this.endOt = endOt;
	}
	public Date getSubmittedOn() {
		return submittedOn;
	}
	public void setSubmittedOn(Date submittedOn) {
		this.submittedOn = submittedOn;
	}
	public Date getSentOn() {
		return sentOn;
	}
	public void setSentOn(Date sentOn) {
		this.sentOn = sentOn;
	}
	public Date getApprovedOn() {
		return approvedOn;
	}
	public void setApprovedOn(Date approvedOn) {
		this.approvedOn = approvedOn;
	}
	public Date getCollectedOn() {
		return collectedOn;
	}
	public void setCollectedOn(Date collectedOn) {
		this.collectedOn = collectedOn;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getPlacementId() {
		return placementId;
	}
	public void setPlacementId(Long placementId) {
		this.placementId = placementId;
	}
	
	
	
	
	
	
	
	
}
