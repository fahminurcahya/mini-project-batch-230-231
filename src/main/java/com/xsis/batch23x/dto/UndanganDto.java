package com.xsis.batch23x.dto;

import java.time.LocalDate;
import java.util.Date;

public class UndanganDto {
	
	private Long cByUndangan;
	private Date cOnUndangan;
	private Long cByDetail;
	private Date cOnDetail;
	private Long id;
	private String invitationCode;
	private LocalDate invitationDate;
	private Long scheduleTypeId;
	private Long ro;
	private Long tro;
	private String otherRoTro;
	private String location;
	private String time;
	private Long idDetail;
	private Long biodataId;
	private Long undanganId;
	private String notes;
	private Long idBio;
	private String fullname;
	private Long idP;
	private Long idBioP;
	private String schoolName;
	private String major;
	private Long educationLevelId;
	private String namaRo;
	private String namaTro;
	private String schName;
	private String email;
		
	public UndanganDto(Long id,String invitationCode, LocalDate invitationDate, Long scheduleTypeId, Long ro, Long tro, String otherRoTro,
			String location, String time, Long idDetail, Long biodataId, Long undanganId, String notes, Long idBio,
			String fullname,Long cByUndangan, Date cOnUndangan, Long cByDetail, Date cOnDetail) {
		super();
		this.id = id;
		this.invitationCode = invitationCode;
		this.invitationDate = invitationDate;
		this.scheduleTypeId = scheduleTypeId;
		this.ro = ro;
		this.tro = tro;
		this.otherRoTro = otherRoTro;
		this.location = location;
		this.time = time;
		this.idDetail = idDetail;
		this.biodataId = biodataId;
		this.undanganId = undanganId;
		this.notes = notes;
		this.idBio = idBio;
		this.fullname = fullname;
		this.cByUndangan = cByUndangan;
		this.cOnUndangan = cOnUndangan;
		this.cByDetail = cByDetail;
		this.cOnDetail = cOnDetail;
	}
	public UndanganDto(Long id, LocalDate invitationDate, Long ro, Long tro, String location,String time, Long idDetail,String schName,String namaRo,
			String namaTro) {
		super();
		this.id = id;
		this.invitationDate = invitationDate;
		this.ro = ro;
		this.tro = tro;
		this.location = location;
		this.time = time;
		this.idDetail = idDetail;
		this.schName = schName;
		this.namaRo = namaRo;
		this.namaTro = namaTro;
	}
	public String getSchName() {
		return schName;
	}
	public void setSchName(String schName) {
		this.schName = schName;
	}
	public String getNamaRo() {
		return namaRo;
	}
	public void setNamaRo(String namaRo) {
		this.namaRo = namaRo;
	}
	public String getNamaTro() {
		return namaTro;
	}
	public void setNamaTro(String namaTro) {
		this.namaTro = namaTro;
	}
	public UndanganDto(LocalDate invitationDate, Long ro, Long tro, String time) {
		super();
		this.invitationDate = invitationDate;
		this.ro = ro;
		this.tro = tro;
		this.time = time;
	}
	public UndanganDto(Long id, String invitationCode, LocalDate invitationDate, Long scheduleTypeId, Long ro, Long tro,
			String otherRoTro, String location, String time, Long idDetail, Long biodataId, Long undanganId,
			String notes, Long idBio, String fullname,
			Long cByUndangan,Date cOnUndangan,Long cByDetail, Date cOnDetail, String email) {
		super();
		this.id = id;
		this.invitationCode = invitationCode;
		this.invitationDate = invitationDate;
		this.scheduleTypeId = scheduleTypeId;
		this.ro = ro;
		this.tro = tro;
		this.otherRoTro = otherRoTro;
		this.location = location;
		this.time = time;
		this.idDetail = idDetail;
		this.biodataId = biodataId;
		this.undanganId = undanganId;
		this.notes = notes;
		this.idBio = idBio;
		this.fullname = fullname;
		this.cByUndangan =  cByUndangan;
		this.cOnUndangan = cOnUndangan;
		this.cByDetail = cByDetail;
		this.cOnDetail = cOnDetail;
		this.email = email;
	}
	public UndanganDto(Long id, String invitationCode, LocalDate invitationDate, Long scheduleTypeId, Long ro, Long tro,
			String otherRoTro, String location, String time, Long idDetail, Long biodataId, Long undanganId,
			String notes, Long idBio, String fullname, Long idP, Long idBioP, String schoolName, String major,
			Long educationLevelId) {
		super();
		this.id = id;
		this.invitationCode = invitationCode;
		this.invitationDate = invitationDate;
		this.scheduleTypeId = scheduleTypeId;
		this.ro = ro;
		this.tro = tro;
		this.otherRoTro = otherRoTro;
		this.location = location;
		this.time = time;
		this.idDetail = idDetail;
		this.biodataId = biodataId;
		this.undanganId = undanganId;
		this.notes = notes;
		this.idBio = idBio;
		this.fullname = fullname;
		this.idP = idP;
		this.idBioP = idBioP;
		this.schoolName = schoolName;
		this.major = major;
		this.educationLevelId = educationLevelId;
	}
	
	
	public Long getcByUndangan() {
		return cByUndangan;
	}
	public void setcByUndangan(Long cByUndangan) {
		this.cByUndangan = cByUndangan;
	}
	public Date getcOnUndangan() {
		return cOnUndangan;
	}
	public void setcOnUndangan(Date cOnUndangan) {
		this.cOnUndangan = cOnUndangan;
	}
	public Long getcByDetail() {
		return cByDetail;
	}
	public void setcByDetail(Long cByDetail) {
		this.cByDetail = cByDetail;
	}
	public Date getcOnDetail() {
		return cOnDetail;
	}
	public void setcOnDetail(Date cOnDetail) {
		this.cOnDetail = cOnDetail;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInvitationCode() {
		return invitationCode;
	}
	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}
	public LocalDate getInvitationDate() {
		return invitationDate;
	}
	public void setInvitationDate(LocalDate invitationDate) {
		this.invitationDate = invitationDate;
	}
	public Long getScheduleTypeId() {
		return scheduleTypeId;
	}
	public void setScheduleTypeId(Long scheduleTypeId) {
		this.scheduleTypeId = scheduleTypeId;
	}
	public Long getRo() {
		return ro;
	}
	public void setRo(Long ro) {
		this.ro = ro;
	}
	public Long getTro() {
		return tro;
	}
	public void setTro(Long tro) {
		this.tro = tro;
	}
	public String getOtherRoTro() {
		return otherRoTro;
	}
	public void setOtherRoTro(String otherRoTro) {
		this.otherRoTro = otherRoTro;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Long getIdDetail() {
		return idDetail;
	}
	public void setIdDetail(Long idDetail) {
		this.idDetail = idDetail;
	}
	public Long getBiodataId() {
		return biodataId;
	}
	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	public Long getUndanganId() {
		return undanganId;
	}
	public void setUndanganId(Long undanganId) {
		this.undanganId = undanganId;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Long getIdBio() {
		return idBio;
	}
	public void setIdBio(Long idBio) {
		this.idBio = idBio;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public Long getIdP() {
		return idP;
	}
	public void setIdP(Long idP) {
		this.idP = idP;
	}
	public Long getIdBioP() {
		return idBioP;
	}
	public void setIdBioP(Long idBioP) {
		this.idBioP = idBioP;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public Long getEducationLevelId() {
		return educationLevelId;
	}
	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
	
	
	
}
