package com.xsis.batch23x.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_addrbook")
public class AddrbookModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "is_locked", nullable = false)
	private Boolean isLocked = false;
	
	@Column (name = "attempt", length = 1, nullable = false)
	private Integer attempt = 0;
	
	@Column (name = "email", length = 100, nullable = false)
	private String email;
	
	@Column (name = "abuid", length = 50, unique = true, nullable = false)
	private String abuid;
	
	@Column (name = "abpwd", length = 50, nullable = false)
	private String abpwd;
	
	@Column (name = "fp_token", length = 100)
	private String fpToken;
	
	@Column (name = "fp_expired_date")
	private LocalDateTime fpExpiredDate;
	
	@Column (name = "fp_counter", length = 3, nullable = false)
	private Integer fpCounter;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsLocked() {
		return isLocked;
	}

	public void setIsLocked(Boolean isLocked) {
		this.isLocked = isLocked;
	}

	public Integer getAttempt() {
		return attempt;
	}

	public void setAttempt(Integer attempt) {
		this.attempt = attempt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAbuid() {
		return abuid;
	}

	public void setAbuid(String abuid) {
		this.abuid = abuid;
	}

	public String getAbpwd() {
		return abpwd;
	}

	public void setAbpwd(String abpwd) {
		this.abpwd = abpwd;
	}

	public String getFpToken() {
		return fpToken;
	}

	public void setFpToken(String fpToken) {
		this.fpToken = fpToken;
	}

	public LocalDateTime getFpExpiredDate() {
		return fpExpiredDate;
	}

	public void setFpExpiredDate(LocalDateTime fpExpiredDate) {
		this.fpExpiredDate = fpExpiredDate;
	}

	public Integer getFpCounter() {
		return fpCounter;
	}

	public void setFpCounter(Integer fpCounter) {
		this.fpCounter = fpCounter;
	}
	
	

}
