package com.xsis.batch23x.models;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="x_biodata")
public class BiodataModel extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "fullname", length = 255, nullable = false)
	private String fullname;
	
	@Column (name = "nick_name", length = 100, nullable = false)
	private String nickName;
	
	@Column (name = "pob", length = 100, nullable = false)
	private String pob;
	
	@Column (name = "dob", nullable = false)
	private LocalDate dob;
	
	@Column (name = "gender", nullable = false)
	private Boolean gender = false;
	
	@Column (name = "religion_id", length = 11, nullable = false)
	private Long religionId;
	
	@ManyToOne
	@JoinColumn(name = "religion_id", insertable = false, updatable = false)
	private ReligionModel religionmodel;
	
	@Column (name = "high")
	private Integer high;
	
	@Column (name = "weight")
	private Integer weight;
	
	@Column (name = "nationality", length = 100)
	private String nationality;
	
	@Column (name = "ethnic", length = 50)
	private String ethnic;
	
	@Column (name = "hobby", length = 255)
	private String hobby;
	
	@Column (name = "identity_type_id", length = 11, nullable = false)
	private Long identityTypeId;
	
	@ManyToOne
	@JoinColumn(name = "identity_type_id", insertable = false, updatable = false)
	private IdentityTypeModel identityTypemodel;
	
	@Column (name = "identity_no", length = 50, nullable = false)
	private String identityNo;
	
	@Column (name = "email", length = 100, nullable = false)
	private String email;
	
	@Column (name = "phone_number1", length = 50, nullable = false)
	private String phoneNumber1;
	
	@Column (name = "phone_number2", length = 50)
	private String phoneNumber2;
	
	@Column (name = "parent_phone_number", length = 50, nullable = false)
	private String parentPhoneNumber;
	
	@Column (name = "child_sequence", length = 5)
	private String childSequence;
	
	@Column (name = "how_many_brothers", length = 5)
	private String howManyBrothers;
	
	@Column (name = "marital_status_id", length = 11, nullable = false)
	private Long maritalStatusId;
	
	@ManyToOne
	@JoinColumn(name = "marital_status_id", insertable = false, updatable = false)
	private MaritalStatusModel maritalStatusmodel;
	
	@Column (name = "addrbook_id", length = 11)
	private Long addrbookId;
	
	@OneToOne
	@JoinColumn(name="addrbook_id", insertable = false, updatable = false)
	private AddrbookModel addrBookModel;
	
	@Column (name = "token", length = 10)
	private String token;
	
	@Column (name = "expired_token")
	private Date expiredToken;
	
	@Column (name = "marriage_year", length = 10)
	private String marriageYear;
	
	@Column (name = "company_id", length = 11, nullable = false)
	private Long companyId;
	
	@Column (name = "is_process")
	private Boolean isProcess = false;
	
	@Column (name = "is_complete")
	private Boolean isComplete = false;
		

	  @OneToMany(mappedBy = "biodatamodel", cascade = CascadeType.ALL,fetch =
	  FetchType.LAZY)
	  @Column(nullable = true) 
	private Set<UndanganDetailModel> undanganDetailModel;
	 
	
	@OneToMany(mappedBy = "biodatamodel", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@Column(nullable = true)
	private Set<RiwayatPendidikanModel> riwayatpendidikanmodel;


	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPob() {
		return pob;
	}

	public void setPob(String pob) {
		this.pob = pob;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Long getReligionId() {
		return religionId;
	}

	public void setReligionId(Long religionId) {
		this.religionId = religionId;
	}

	public Integer getHigh() {
		return high;
	}

	public void setHigh(Integer high) {
		this.high = high;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getEthnic() {
		return ethnic;
	}

	public void setEthnic(String ethnic) {
		this.ethnic = ethnic;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public Long getIdentityTypeId() {
		return identityTypeId;
	}

	public void setIdentityTypeId(Long identityTypeId) {
		this.identityTypeId = identityTypeId;
	}

	public String getIdentityNo() {
		return identityNo;
	}

	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getParentPhoneNumber() {
		return parentPhoneNumber;
	}

	public void setParentPhoneNumber(String parentPhoneNumber) {
		this.parentPhoneNumber = parentPhoneNumber;
	}

	public String getChildSequence() {
		return childSequence;
	}

	public void setChildSequence(String childSequence) {
		this.childSequence = childSequence;
	}

	public String getHowManyBrothers() {
		return howManyBrothers;
	}

	public void setHowManyBrothers(String howManyBrothers) {
		this.howManyBrothers = howManyBrothers;
	}

	public Long getMaritalStatusId() {
		return maritalStatusId;
	}

	public void setMaritalStatusId(Long maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}

	public Long getAddrbookId() {
		return addrbookId;
	}

	public void setAddrbookId(Long addrbookId) {
		this.addrbookId = addrbookId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiredToken() {
		return expiredToken;
	}

	public void setExpiredToken(Date expiredToken) {
		this.expiredToken = expiredToken;
	}

	public String getMarriageYear() {
		return marriageYear;
	}

	public void setMarriageYear(String marriageYear) {
		this.marriageYear = marriageYear;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Boolean getIsProcess() {
		return isProcess;
	}

	public void setIsProcess(Boolean isProcess) {
		this.isProcess = isProcess;
	}

	public Boolean getIsComplete() {
		return isComplete;
	}

	public void setIsComplete(Boolean isComplete) {
		this.isComplete = isComplete;
	}


	public Set<RiwayatPendidikanModel> getRiwayatpendidikanmodel() {
		return riwayatpendidikanmodel;
	}

	public void setRiwayatpendidikanmodel(Set<RiwayatPendidikanModel> riwayatpendidikanmodel) {
		this.riwayatpendidikanmodel = riwayatpendidikanmodel;
	}

	public ReligionModel getReligionmodel() {
		return religionmodel;
	}

	public void setReligionmodel(ReligionModel religionmodel) {
		this.religionmodel = religionmodel;
	}

	public IdentityTypeModel getIdentityTypemodel() {
		return identityTypemodel;
	}

	public void setIdentityTypemodel(IdentityTypeModel identityTypemodel) {
		this.identityTypemodel = identityTypemodel;
	}

	public MaritalStatusModel getMaritalStatusmodel() {
		return maritalStatusmodel;
	}

	public void setMaritalStatusmodel(MaritalStatusModel maritalStatusmodel) {
		this.maritalStatusmodel = maritalStatusmodel;
	}



	
	
	
	


	

}
