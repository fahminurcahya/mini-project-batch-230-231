package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_client")
public class ClientModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "name", length = 100, nullable = false)
	private String name;
	
	@Column (name = "user_client_name", length = 100, nullable = false)
	private String userClientName;
	
	@Column (name = "ero", length = 11, nullable = false)
	private Long ero;
	
	@Column (name = "user_email", length = 100, nullable = false)
	private String userEmail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserClientName() {
		return userClientName;
	}

	public void setUserClientName(String userClientName) {
		this.userClientName = userClientName;
	}

	public Long getEro() {
		return ero;
	}

	public void setEro(Long ero) {
		this.ero = ero;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	

}
