package com.xsis.batch23x.models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class CommonEntity {
	
		@Column(name = "created_by", length=11, nullable = false)
		protected Long createdBy;
	
		@Column(name = "created_on", nullable = false)
	    protected Date createdOn;
	 
		@Column(name = "modified_by", length=11, nullable = true)
		protected Long modifiedBy;

	    @Column(name = "modified_on",nullable = true)
	    protected Date modifiedOn;
	    
	    @Column(name = "deleted_by", length=11, nullable = true)
	    protected Long deletedBy;

	    @Column(name = "deleted_on", nullable = true)
	    protected Date deletedOn;
	    
	    @Column(name = "is_delete", nullable = false)
	    protected Boolean isDelete = false;

		public Long getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(Long createdBy) {
			this.createdBy = createdBy;
		}

		public Date getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		public Long getModifiedBy() {
			return modifiedBy;
		}

		public void setModifiedBy(Long modifiedBy) {
			this.modifiedBy = modifiedBy;
		}

		public Date getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(Date modifiedOn) {
			this.modifiedOn = modifiedOn;
		}

		public Long getDeletedBy() {
			return deletedBy;
		}

		public void setDeletedBy(Long deletedBy) {
			this.deletedBy = deletedBy;
		}

		public Date getDeletedOn() {
			return deletedOn;
		}

		public void setDeletedOn(Date deletedOn) {
			this.deletedOn = deletedOn;
		}

		public Boolean getIsDelete() {
			return isDelete;
		}

		public void setIsDelete(Boolean isDelete) {
			this.isDelete = isDelete;
		}

		
	    
	    
}
