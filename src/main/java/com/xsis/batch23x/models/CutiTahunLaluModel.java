package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CutiTahunLalu")
public class CutiTahunLaluModel extends CommonEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "leaveId", length = 11)
	private Long leaveId;
	
	@Column (name = "cuti")
	private Integer cuti;
	
	@Column (name = "tahun")
	private String tahun;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(Long leaveId) {
		this.leaveId = leaveId;
	}

	public Integer getCuti() {
		return cuti;
	}

	public void setCuti(Integer cuti) {
		this.cuti = cuti;
	}

	public String getTahun() {
		return tahun;
	}

	public void setTahun(String tahun) {
		this.tahun = tahun;
	}

	public CutiTahunLaluModel(Long id, Long leaveId, Integer cuti, String tahun) {
		super();
		this.id = id;
		this.leaveId = leaveId;
		this.cuti = cuti;
		this.tahun = tahun;
	}

	public CutiTahunLaluModel(Long leaveId, Integer cuti, String tahun) {
		super();
		this.leaveId = leaveId;
		this.cuti = cuti;
		this.tahun = tahun;
	}

	public CutiTahunLaluModel() {
		super();
	}

	

	
	
}
