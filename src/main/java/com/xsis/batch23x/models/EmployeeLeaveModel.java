package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "x_employee_leave")
public class EmployeeLeaveModel extends CommonEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "employee_id", length = 11, nullable = false)
	private Long employeeId;
	
	@Column (name = "regular_quota", nullable = false)
	private Integer regularQuota;
	
	@Column (name = "annual_collective_leave", nullable = false)
	private Integer annualCollectiveLeave;
	
	@Column (name = "leave_already_taken", nullable = false)
	private Integer leaveAlreadyTaken;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}


	public Integer getRegularQuota() {
		return regularQuota;
	}


	public void setRegularQuota(Integer regularQuota) {
		this.regularQuota = regularQuota;
	}


	public Integer getAnnualCollectiveLeave() {
		return annualCollectiveLeave;
	}


	public void setAnnualCollectiveLeave(Integer annualCollectiveLeave) {
		this.annualCollectiveLeave = annualCollectiveLeave;
	}


	public Integer getLeaveAlreadyTaken() {
		return leaveAlreadyTaken;
	}


	public void setLeaveAlreadyTaken(Integer leaveAlreadyTaken) {
		this.leaveAlreadyTaken = leaveAlreadyTaken;
	}


	public EmployeeLeaveModel() {
		
	}


	public EmployeeLeaveModel(Long id, Long employeeId, Integer regularQuota, Integer annualCollectiveLeave,
			Integer leaveAlreadyTaken) {
		super();
		this.id = id;
		this.employeeId = employeeId;
		this.regularQuota = regularQuota;
		this.annualCollectiveLeave = annualCollectiveLeave;
		this.leaveAlreadyTaken = leaveAlreadyTaken;
	}


	public EmployeeLeaveModel(Integer regularQuota, Integer annualCollectiveLeave, Integer leaveAlreadyTaken) {
		super();
		this.regularQuota = regularQuota;
		this.annualCollectiveLeave = annualCollectiveLeave;
		this.leaveAlreadyTaken = leaveAlreadyTaken;
	}


	public EmployeeLeaveModel(Integer regularQuota) {
		super();
		this.regularQuota = regularQuota;
	}

	



}
