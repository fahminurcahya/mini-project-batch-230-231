package com.xsis.batch23x.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "x_employee")
public class EmployeeModel  extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "biodata_id", referencedColumnName = "id",
    		insertable = false, updatable = false, nullable = false)
    private BiodataModel biodataModel;
	
	@Column (name = "is_idle")
	private Boolean isIdle = false;
	
	@Column (name = "is_ero")
	private Boolean isEro = false;
	
	@Column (name = "is_user_client")
	private Boolean isUserClient = false;
	
	@Column (name = "ero_email", length = 100)
	private String eroEmail;
	
	
	
	
	public BiodataModel getBiodataModel() {
		return biodataModel;
	}

	public void setBiodataModel(BiodataModel biodataModel) {
		this.biodataModel = biodataModel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public Boolean getIsIdle() {
		return isIdle;
	}

	public void setIsIdle(Boolean isIdle) {
		this.isIdle = isIdle;
	}

	public Boolean getIsEro() {
		return isEro;
	}

	public void setIsEro(Boolean isEro) {
		this.isEro = isEro;
	}

	public Boolean getIsUserClient() {
		return isUserClient;
	}

	public void setIsUserClient(Boolean isUserClient) {
		this.isUserClient = isUserClient;
	}

	public String getEroEmail() {
		return eroEmail;
	}

	public void setEroEmail(String eroEmail) {
		this.eroEmail = eroEmail;
		
	}

	

	
}
