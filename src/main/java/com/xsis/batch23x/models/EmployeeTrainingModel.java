package com.xsis.batch23x.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_employee_training")
public class EmployeeTrainingModel extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "employee_id", length = 11, nullable = false)
	private Long employeeId;
	
	@Column (name = "training_id", length = 11, nullable = false)
	private Long trainingId;
	
	@Column (name = "training_organizer_id", length = 11)
	private Long trainingOrganizerId;
	
	@Column (name = "training_date")
	private LocalDate trainingDate;
	
	@Column (name = "training_type_id", length = 11)
	private Long trainingTypeId;
	
	@Column (name = "certification_type_id", length = 11)
	private Long certificationTypeId;
	
	@Column (name = "status", length = 15, nullable = false)
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getTrainingId() {
		return trainingId;
	}

	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}

	public Long getTrainingOrganizerId() {
		return trainingOrganizerId;
	}

	public void setTrainingOrganizerId(Long trainingOrganizerId) {
		this.trainingOrganizerId = trainingOrganizerId;
	}

	public LocalDate getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(LocalDate trainingDate) {
		this.trainingDate = trainingDate;
	}

	public Long getTrainingTypeId() {
		return trainingTypeId;
	}

	public void setTrainingTypeId(Long trainingTypeId) {
		this.trainingTypeId = trainingTypeId;
	}

	public Long getCertificationTypeId() {
		return certificationTypeId;
	}

	public void setCertificationTypeId(Long certificationTypeId) {
		this.certificationTypeId = certificationTypeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
