package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//import com.fasterxml.jackson.annotation.JsonBackReference;

//import com.fasterxml.jackson.annotation.JsonBackReference;

import com.xsis.batch23x.models.SkillLevelModel;

@Entity
@Table(name="x_keahlian")
public class KeahlianModel extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", length = 11, nullable = true)
	private Long id;
	
	@Column(name="biodata_id", length= 11, nullable = false)
	private Long biodataId;
	
	@Column(name="skill_name", length= 100, nullable = true)
	private String skillName;
		
	@Column(name="notes", length= 1000, nullable = true)
	private String notes;
	
	@ManyToOne
	@JoinColumn (name = "skill_level_id", referencedColumnName = "id")
	private SkillLevelModel skillLevelId;
	
	public SkillLevelModel getskillLevelId() {
		return skillLevelId;
	}

	public void setSkillLevelId(SkillLevelModel skillLevelId) {
		this.skillLevelId = skillLevelId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	

}
