package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_keterangan_tambahan")
public class KeteranganTambahanModel extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@Column (name = "emergency_contact_name", length = 100)
	private String emergencyContactName;
	
	@Column (name = "emergency_contact_phone", length = 50)
	private String emergencyContactPhone;
	
	@Column (name = "expected_salary", length = 20)
	private String expectedSalary;
	
	@Column (name = "is_negotiable")
	private Boolean isNegotiable = false;
	
	@Column (name = "start_working", length = 100)
	private String startWorking;
	
	@Column (name = "is_ready_to_outoftown")
	private Boolean isReadyToOutOfTown = false;
	
	@Column (name = "is_apply_other_place")
	private Boolean isApplyOtherPlace = false;
	
	@Column (name = "apply_place", length = 100)
	private String applyPlace;
	
	@Column (name = "selection_phase", length = 100)
	private String selectionPhase;
	
	@Column (name = "is_ever_badly_sick")
	private Boolean isEverBadlySick = false;
	
	@Column (name = "disease_name", length = 100)
	private String diseaseName;
	
	@Column (name = "disease_time", length = 100)
	private String diseaseTime;
	
	@Column (name = "is_ever_psychotest")
	private Boolean isEverPsychotest = false;
	
	@Column (name = "psychotest_need", length = 100)
	private String psychotestNeed;
	
	@Column (name = "psychotest_time", length = 100)
	private String psychotestTime;
	
	@Column (name = "requirements_required", length = 500)
	private String requirementsRequired;
	
	@Column (name = "other_notes", length = 1000)
	private String otherNotes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getEmergencyContactName() {
		return emergencyContactName;
	}

	public void setEmergencyContactName(String emergencyContactName) {
		this.emergencyContactName = emergencyContactName;
	}

	public String getEmergencyContactPhone() {
		return emergencyContactPhone;
	}

	public void setEmergencyContactPhone(String emergencyContactPhone) {
		this.emergencyContactPhone = emergencyContactPhone;
	}

	public String getExpectedSalary() {
		return expectedSalary;
	}

	public void setExpectedSalary(String expectedSalary) {
		this.expectedSalary = expectedSalary;
	}

	public Boolean getIsNegotiable() {
		return isNegotiable;
	}

	public void setIsNegotiable(Boolean isNegotiable) {
		this.isNegotiable = isNegotiable;
	}

	public String getStartWorking() {
		return startWorking;
	}

	public void setStartWorking(String startWorking) {
		this.startWorking = startWorking;
	}

	public Boolean getIsReadyToOutOfTown() {
		return isReadyToOutOfTown;
	}

	public void setIsReadyToOutOfTown(Boolean isReadyToOutOfTown) {
		this.isReadyToOutOfTown = isReadyToOutOfTown;
	}

	public Boolean getIsApplyOtherPlace() {
		return isApplyOtherPlace;
	}

	public void setIsApplyOtherPlace(Boolean isApplyOtherPlace) {
		this.isApplyOtherPlace = isApplyOtherPlace;
	}

	public String getApplyPlace() {
		return applyPlace;
	}

	public void setApplyPlace(String applyPlace) {
		this.applyPlace = applyPlace;
	}

	public String getSelectionPhase() {
		return selectionPhase;
	}

	public void setSelectionPhase(String selectionPhase) {
		this.selectionPhase = selectionPhase;
	}

	public Boolean getIsEverBadlySick() {
		return isEverBadlySick;
	}

	public void setIsEverBadlySick(Boolean isEverBadlySick) {
		this.isEverBadlySick = isEverBadlySick;
	}

	public String getDiseaseName() {
		return diseaseName;
	}

	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}

	public String getDiseaseTime() {
		return diseaseTime;
	}

	public void setDiseaseTime(String diseaseTime) {
		this.diseaseTime = diseaseTime;
	}

	public Boolean getIsEverPsychotest() {
		return isEverPsychotest;
	}

	public void setIsEverPsychotest(Boolean isEverPsychotest) {
		this.isEverPsychotest = isEverPsychotest;
	}

	public String getPsychotestNeed() {
		return psychotestNeed;
	}

	public void setPsychotestNeed(String psychotestNeed) {
		this.psychotestNeed = psychotestNeed;
	}

	public String getPsychotestTime() {
		return psychotestTime;
	}

	public void setPsychotestTime(String psychotestTime) {
		this.psychotestTime = psychotestTime;
	}

	public String getRequirementsRequired() {
		return requirementsRequired;
	}

	public void setRequirementsRequired(String requirementsRequired) {
		this.requirementsRequired = requirementsRequired;
	}

	public String getOtherNotes() {
		return otherNotes;
	}

	public void setOtherNotes(String otherNotes) {
		this.otherNotes = otherNotes;
	}
	
	

}
