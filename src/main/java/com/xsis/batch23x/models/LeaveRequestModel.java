package com.xsis.batch23x.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "x_leave_request")
public class LeaveRequestModel extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@ManyToOne
	@JoinColumn (name = "leave_name_id", referencedColumnName = "id")
	private LeaveNameModel leaveNameId;
	
	public LeaveNameModel getleaveNameId() {
		return leaveNameId;
	}

	public void setleaveNameId(LeaveNameModel leaveNameId) {
		this.leaveNameId = leaveNameId;
	}
	
	@Column (name = "start", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date start;
	
	@Column (name = "end_", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date end;
	
	@Column (name = "reason", length = 255, nullable = true)
	private String reason;
	
	@Column (name = "leave_contact", length = 50, nullable = true)
	private String leaveContact;
	
	@Column (name = "leave_address", length = 255, nullable = true)
	private String leaveAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLeaveContact() {
		return leaveContact;
	}

	public void setLeaveContact(String leaveContact) {
		this.leaveContact = leaveContact;
	}

	public String getLeaveAddress() {
		return leaveAddress;
	}

	public void setLeaveAddress(String leaveAddress) {
		this.leaveAddress = leaveAddress;
	}

	
}
