package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_online_test_detail")
public class OnlineTestDetailModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "online_test_id", length = 11)
	private Long onlineTestId;
	
	@Column (name = "test_type_id", length = 11)
	private Long testTypeId;
	
	@Column (name = "test_order")
	private Integer testOrder;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOnlineTestId() {
		return onlineTestId;
	}

	public void setOnlineTestId(Long onlineTestId) {
		this.onlineTestId = onlineTestId;
	}

	public Long getTestTypeId() {
		return testTypeId;
	}

	public void setTestTypeId(Long testTypeId) {
		this.testTypeId = testTypeId;
	}

	public Integer getTestOrder() {
		return testOrder;
	}

	public void setTestOrder(Integer testOrder) {
		this.testOrder = testOrder;
	}
	
	

}
