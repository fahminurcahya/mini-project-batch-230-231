package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_organisasi")
public class OrganisasiModel extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@Column (name = "name", length = 100)
	private String name;
	
	@Column (name = "position", length = 100)
	private String position;
	
	@Column (name = "entry_year", length = 10)
	private String entryYear;
	
	@Column (name = "exit_year", length = 10)
	private String exitYear;
	
	@Column (name = "responsibility", length = 100)
	private String responsibility;
	
	@Column (name = "notes", length = 1000)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getEntryYear() {
		return entryYear;
	}

	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}

	public String getExitYear() {
		return exitYear;
	}

	public void setExitYear(String exitYear) {
		this.exitYear = exitYear;
	}

	public String getResponsibility() {
		return responsibility;
	}

	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public OrganisasiModel(Long id, Long biodataId, String name, String position, String entryYear, String exitYear,
			String responsibility, String notes) {
		super();
		this.id = id;
		this.biodataId = biodataId;
		this.name = name;
		this.position = position;
		this.entryYear = entryYear;
		this.exitYear = exitYear;
		this.responsibility = responsibility;
		this.notes = notes;
	}

	public OrganisasiModel(Long biodataId, String name, String position, String entryYear, String exitYear,
			String responsibility, String notes) {
		super();
		this.biodataId = biodataId;
		this.name = name;
		this.position = position;
		this.entryYear = entryYear;
		this.exitYear = exitYear;
		this.responsibility = responsibility;
		this.notes = notes;
	}

	public OrganisasiModel() {
		super();
	}

	
	
	
	
	

}
