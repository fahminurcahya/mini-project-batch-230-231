package com.xsis.batch23x.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "x_permission")
public class PermissionModel extends CommonEntity{
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11, nullable = false)
	private Long id;
		
	@Column (name = "employee_name", length = 255, nullable = false)
	private String employeeName;
		
	@Column (name = "department", length = 100, nullable = true)
	private String department;
	
	@Column (name = "position", length = 100, nullable = true)
	private String position;
	
	@Column (name = "permission_time", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date permissionTime;
	
	@Column (name = "notes", length = 255, nullable = false)
	private String notes;
	
	@Column(name = "is_absent", nullable = true)
    private Boolean isAbsent = true;
	
	@Column(name = "is_sick", nullable = true)
    private Boolean isSick = true;
	
	@Column(name = "is_coming_late", nullable = true)
    private Boolean isComingLate = true;
	
	@Column(name = "is_early_leave", nullable = true)
    private Boolean isEarlyLeave = true;
	
	@Column(name = "others", nullable = true)
    private Boolean others = true;
	
	@Column (name = "status", length = 20, nullable = false)
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getPermissionTime() {
		return permissionTime;
	}

	public void setPermissionTime(Date permissionTime) {
		this.permissionTime = permissionTime;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsAbsent() {
		return isAbsent;
	}

	public void setIsAbsent(Boolean isAbsent) {
		this.isAbsent = isAbsent;
	}

	public Boolean getIsSick() {
		return isSick;
	}

	public void setIsSick(Boolean isSick) {
		this.isSick = isSick;
	}

	public Boolean getIsComingLate() {
		return isComingLate;
	}

	public void setIsComingLate(Boolean isComingLate) {
		this.isComingLate = isComingLate;
	}

	public Boolean getIsEarlyLeave() {
		return isEarlyLeave;
	}

	public void setIsEarlyLeave(Boolean isEarlyLeave) {
		this.isEarlyLeave = isEarlyLeave;
	}

	public Boolean getOthers() {
		return others;
	}

	public void setOthers(Boolean others) {
		this.others = others;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
