package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "x_placement")
public class PlacementModel extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "client_id", length = 11, nullable = false)
	private Long clientId;
	
	@ManyToOne
		@JsonBackReference
		@JoinColumn(name="client_id", insertable = false, updatable = false)
		private ClientModel clientModel;
	
	@Column (name = "employee_id", length = 11, nullable = false)
	private Long employeeId;
	
	@ManyToOne
	@JoinColumn(name="employee_id", insertable = false, updatable = false)
	private EmployeeModel employeeModel;
	
	@Column (name = "is_placement_active", nullable = false)
	private Boolean isPlacementActive = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Boolean getIsPlacementActive() {
		return isPlacementActive;
	}

	public void setIsPlacementActive(Boolean isPlacementActive) {
		this.isPlacementActive = isPlacementActive;
	}
	
	

}
