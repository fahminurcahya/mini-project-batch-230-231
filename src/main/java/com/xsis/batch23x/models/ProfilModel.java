package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="viewprofil")
public class ProfilModel {
	@Id
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@Column (name = "fullname", length = 255, nullable = false)
	private String fullname;
	
	@Column (name = "school_name", length = 100, nullable = false)
	private String schoolName;
	
	@Column (name = "entry_year", length = 10)
	private String entryYear;
	
	@Column (name = "graduation_year", length = 10)
	private String graduationYear;
	
	@Column (name = "country", length = 50)
	private String country;
	
	@Column (name = "major", length = 100)
	private String major;
	
	@Column (name = "gpa")
	private Double gpa;
	
	@Column(name="skill_name", length= 100, nullable = true)
	private String skillName;
	
	@Column (name = "last_position", length = 100)
	private String lastPosition;
	
	@Column (name = "company_name", length = 100)
	private String companyName;
	
	@Column (name = "join_year", length = 10)
	private String joinYear;
	
	@Column (name = "resign_year", length = 10)
	private String resignYear;
	
	@Column (name = "training_name", length = 100 )
	private String trainingName;
	
	@Column (name = "training_year", length = 10)
	private String trainingYear;
	
	@Column(name = "certificate_name", nullable=true, length=200)
	private String certificateName;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
	@Column(name = "valid_start_year", nullable=true )
	private Integer validStartYear;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
	@Column(name = "until_year", nullable=true)
	private Integer untilYear;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getEntryYear() {
		return entryYear;
	}

	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}

	public String getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Double getGpa() {
		return gpa;
	}

	public void setGpa(Double gpa) {
		this.gpa = gpa;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public String getLastPosition() {
		return lastPosition;
	}

	public void setLastPosition(String lastPosition) {
		this.lastPosition = lastPosition;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getJoinYear() {
		return joinYear;
	}

	public void setJoinYear(String joinYear) {
		this.joinYear = joinYear;
	}

	public String getResignYear() {
		return resignYear;
	}

	public void setResignYear(String resignYear) {
		this.resignYear = resignYear;
	}

	public String getTrainingName() {
		return trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public String getTrainingYear() {
		return trainingYear;
	}

	public void setTrainingYear(String trainingYear) {
		this.trainingYear = trainingYear;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public Integer getValidStartYear() {
		return validStartYear;
	}

	public void setValidStartYear(Integer validStartYear) {
		this.validStartYear = validStartYear;
	}

	public Integer getUntilYear() {
		return untilYear;
	}

	public void setUntilYear(Integer untilYear) {
		this.untilYear = untilYear;
	}

	
}
