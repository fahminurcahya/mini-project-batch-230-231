package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_rencana_jadwal_detail")
public class RencanaJadwalDetailModel extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "rencana_jadwal_id", length = 11, nullable = false)
	private Long rencanaJadwalId;
	
	@Column (name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRencanaJadwalId() {
		return rencanaJadwalId;
	}

	public void setRencanaJadwalId(Long rencanaJadwalId) {
		this.rencanaJadwalId = rencanaJadwalId;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}
	
	

}
