package com.xsis.batch23x.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "x_rencana_jadwal")
public class RencanaJadwalModel extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "schedule_code", length = 20)
	private String scheduleCode;
	
	@Column (name = "schedule_date")
	private LocalDate scheduleDate;
	
	@Column (name = "time", length = 10)
	private String time;
	
	@Column (name = "ro", length = 11)
	private Long ro;
	
	@Column (name = "tro", length = 11)
	private Long tro;
	
	@Column (name = "schedule_type_id", length = 11)
	private Long scheduleTypeId;
	
	@Column (name = "location", length = 100)
	private String location;
	
	@Column (name = "other_ro_tro", length = 100)
	private String otherRoTro;
	
	@Column (name = "notes", length = 1000)
	private String notes;
	
	@Column (name = "is_automatic_mail")
	private Boolean isAutomaticMail = false;
	
	@Column (name = "sent_date")
	private LocalDate sentDate;
	
	@Column (name = "status", length = 50)
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getScheduleCode() {
		return scheduleCode;
	}

	public void setScheduleCode(String scheduleCode) {
		this.scheduleCode = scheduleCode;
	}

	public LocalDate getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(LocalDate scheduleDate) {
		this.scheduleDate = scheduleDate;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getRo() {
		return ro;
	}

	public void setRo(Long ro) {
		this.ro = ro;
	}

	public Long getTro() {
		return tro;
	}

	public void setTro(Long tro) {
		this.tro = tro;
	}

	public Long getScheduleTypeId() {
		return scheduleTypeId;
	}

	public void setScheduleTypeId(Long scheduleTypeId) {
		this.scheduleTypeId = scheduleTypeId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOtherRoTro() {
		return otherRoTro;
	}

	public void setOtherRoTro(String otherRoTro) {
		this.otherRoTro = otherRoTro;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsAutomaticMail() {
		return isAutomaticMail;
	}

	public void setIsAutomaticMail(Boolean isAutomaticMail) {
		this.isAutomaticMail = isAutomaticMail;
	}

	public LocalDate getSentDate() {
		return sentDate;
	}

	public void setSentDate(LocalDate sentDate) {
		this.sentDate = sentDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
