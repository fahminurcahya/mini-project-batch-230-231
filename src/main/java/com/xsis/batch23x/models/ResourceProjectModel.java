package com.xsis.batch23x.models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name= "x_resource_project")
public class ResourceProjectModel extends CommonEntity {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column (name = "id", length = 11)
private Long id;


@ManyToOne
@JoinColumn (name = "client_id")
private ClientModel clientId;

@Column (name = "location", length = 100 )
private String location;

@Column (name = "departement", length = 100 )
private String departement;

@Column (name = "pic_name", length = 100 )
private String picName;

@Column (name = "project_name", length = 100, nullable = false )
private String projectName;

@Column (name = "start_project", nullable = false )
@Temporal(TemporalType.DATE)
private Date startProject;


@Column (name = "end_project", nullable = false )
@Temporal(TemporalType.DATE)
private Date endProject;

@Column (name = "project_role", length = 255, nullable = false )
private String projectRole;

@Column (name = "project_phase", length = 255, nullable = false )
private String projectPhase;

@Column (name = "project_description", length = 255, nullable = false )
private String projectDescription;

@Column (name = "project_technology", length = 255, nullable = false )
private String projectTechnology;

@Column (name = "main_task", length = 255, nullable = false )
private String mainTask;

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}


public ClientModel getClientId() {
	return clientId;
}

 /*public String getClientName() {
	 return clientId.getName();
} */



public void setClientId(ClientModel clientId) {
	this.clientId = clientId;
}

public String getLocation() {
	return location;
}

public void setLocation(String location) {
	this.location = location;
}

public String getDepartement() {
	return departement;
}

public void setDepartement(String departement) {
	this.departement = departement;
}

public String getPicName() {
	return picName;
}

public void setPicName(String picName) {
	this.picName = picName;
}

public String getProjectName() {
	return projectName;
}

public void setProjectName(String projectName) {
	this.projectName = projectName;
}

public Date getStartProject() {
	return startProject;
}

public void setStartProject(Date startProject) {
	this.startProject = startProject;
}

public Date getEndProject() {
	return endProject;
}

public void setEndProject(Date endProject) {
	this.endProject = endProject;
}

public String getProjectRole() {
	return projectRole;
}

public void setProjectRole(String projectRole) {
	this.projectRole = projectRole;
}

public String getProjectPhase() {
	return projectPhase;
}

public void setProjectPhase(String projectPhase) {
	this.projectPhase = projectPhase;
}

public String getProjectDescription() {
	return projectDescription;
}

public void setProjectDescription(String projectDescription) {
	this.projectDescription = projectDescription;
}

public String getProjectTechnology() {
	return projectTechnology;
}

public void setProjectTechnology(String projectTechnology) {
	this.projectTechnology = projectTechnology;
}

public String getMainTask() {
	return mainTask;
}

public void setMainTask(String mainTask) {
	this.mainTask = mainTask;
}


}