package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="x_riwayat_pendidikan")
public class RiwayatPendidikanModel extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "biodata_id", length = 11, nullable = false)
	private Long biodataId;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="biodata_id", insertable = false, updatable = false, nullable = false )	
	private BiodataModel biodatamodel;

	
	@Column (name = "school_name", length = 100)
	private String schoolName;
	
	@Column (name = "city", length = 50)
	private String city;
	
	@Column (name = "country", length = 50)
	private String country;
	
	@Column (name = "education_level_id", length = 11)
	private Long educationLevelId;
	
	@ManyToOne
	@JoinColumn(name="education_level_id", insertable = false, updatable = false)
	private EducationLevelModel educationLevelModel;
	
	@Column (name = "entry_year", length = 10)
	private String entryYear;
	
	@Column (name = "graduation_year", length = 10)
	private String graduationYear;
	
	@Column (name = "major", length = 100)
	private String major;
	
	@Column (name = "gpa")
	private Double gpa;
	
	@Column (name = "notes", length = 1000)
	private String notes;
	
	@Column (name = "order_")
	private Integer order;
	
	@Column (name = "judul_ta", length = 255)
	private String judulTa;
	
	@Column (name = "deskripsi_ta", length = 5000)
	private String deskripsiTa;

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}


	public String getEntryYear() {
		return entryYear;
	}

	public void setEntryYear(String entryYear) {
		this.entryYear = entryYear;
	}

	public String getGraduationYear() {
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear) {
		this.graduationYear = graduationYear;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Double getGpa() {
		return gpa;
	}

	public void setGpa(Double gpa) {
		this.gpa = gpa;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getJudulTa() {
		return judulTa;
	}

	public void setJudulTa(String judulTa) {
		this.judulTa = judulTa;
	}

	public String getDeskripsiTa() {
		return deskripsiTa;
	}

	public void setDeskripsiTa(String deskripsiTa) {
		this.deskripsiTa = deskripsiTa;
	}



}
