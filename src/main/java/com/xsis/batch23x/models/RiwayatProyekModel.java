package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "x_riwayat_proyek")
public class RiwayatProyekModel  extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "riwayat_pekerjaan_id", length = 11, nullable = false)
	private Long riwayatPekerjaanId;
	
	@Column (name = "start_year", length = 10)
	private String startYear;
	
	@Column (name = "start_month", length = 10)
	private String startMonth;
	
	@Column (name = "project_name", length = 50)
	private String projectName;
	
	@Column (name = "project_duration")
	private Integer projectDuration;
	
	

	@ManyToOne(optional =true)
	@JoinColumn (name = "time_period_id")
	private TimePeriodModel timePeriodId;
	
	
	@Column (name = "client", length = 100)
	private String client;
	
	@Column (name = "project_position", length = 100)
	private String projectPosition;
	
	@Column (name = "description", length = 1000)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public TimePeriodModel getTimePeriodId() {
		return timePeriodId;
	}

	public void setTimePeriodId(TimePeriodModel timePeriodId) {
		this.timePeriodId = timePeriodId;
	}

	public Long getRiwayatPekerjaanId() {
		return riwayatPekerjaanId;
	}

	public void setRiwayatPekerjaanId(Long riwayatPekerjaanId) {
		this.riwayatPekerjaanId = riwayatPekerjaanId;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getProjectDuration() {
		return projectDuration;
	}

	public void setProjectDuration(Integer projectDuration) {
		this.projectDuration = projectDuration;
	}

	

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getProjectPosition() {
		return projectPosition;
	}

	public void setProjectPosition(String projectPosition) {
		this.projectPosition = projectPosition;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
