package com.xsis.batch23x.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name ="x_sertifikasi")
public class SertifikasiModel extends CommonEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", length=11)
	private Long id;
	
	@Column(name = "biodata_id" ,nullable=false, length=11)
	private Long biodataId;
	
	@Column(name = "certificate_name", nullable=true, length=200)
	private String certificateName;
	
	@Column(name = "publisher", nullable=true, length=200)
	private String publisher;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
	@Column(name = "valid_start_year", nullable=true )
	private Integer validStartYear;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM")
	@Column(name = "valid_start_month", nullable=true)
	private String valiStartMonth;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
	@Column(name = "until_year", nullable=true)
	private Integer untilYear;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM")
	@Column(name = "until_month", nullable=true)
	private String untilMonth;
	
	@Column(name = "notes", nullable=true, length=1000)
	private String notes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Integer getValidStartYear() {
		return validStartYear;
	}

	public void setValidStartYear(Integer validStartYear) {
		this.validStartYear = validStartYear;
	}

	public String getValiStartMonth() {
		return valiStartMonth;
	}

	public void setValiStartMonth(String valiStartMonth) {
		this.valiStartMonth = valiStartMonth;
	}

	public Integer getUntilYear() {
		return untilYear;
	}

	public void setUntilYear(Integer untilYear) {
		this.untilYear = untilYear;
	}

	public String getUntilMonth() {
		return untilMonth;
	}

	public void setUntilMonth(String untilMonth) {
		this.untilMonth = untilMonth;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	
	
}
