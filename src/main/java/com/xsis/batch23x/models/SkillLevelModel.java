package com.xsis.batch23x.models;


//import java.util.Set;

//import javax.persistence.CascadeType;

//import java.util.Set;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.OneToMany;
//import javax.persistence.OneToMany;
import javax.persistence.Table;

//import com.fasterxml.jackson.annotation.JsonManagedReference;

//import com.fasterxml.jackson.annotation.JsonManagedReference;

//import com.xsis.batch23x.models.KeahlianModel;

@Entity
@Table(name = "x_skill_level")
public class SkillLevelModel extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "name", length = 50, nullable = false)
	private String name;
	
	@Column (name = "description", length = 100)
	private String description;
	
	/*
	 * @OneToMany(mappedBy="skilllevelmodel", cascade=CascadeType.ALL,
	 * fetch=FetchType.LAZY)
	 * 
	 * @JsonManagedReference
	 * 
	 * @Column(nullable=true) private Set<KeahlianModel> skilllevelmodel;
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
