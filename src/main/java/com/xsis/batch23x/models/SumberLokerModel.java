package com.xsis.batch23x.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "x_sumber_loker")
public class SumberLokerModel extends CommonEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "biodata_id", length = 11)
	private Long biodataId;
	
	@Column (name = "vacancy_source", length = 20)
	private String vacancySource;
	
	@Column (name = "candidate_type", length = 10)
	private String candidateType;
	
	@Column (name = "event_name", length = 100)
	private String eventName;
	
	@Column (name = "career_center_name", length = 100)
	private String careerCenterName;
	
	@Column (name = "referrer_name", length = 100)
	private String referrerName;
	
	@Column (name = "referrer_phone", length = 20)
	private String referrerPhone;
	
	@Column (name = "referrer_email", length = 100)
	private String referrerEmail;
	
	@Column (name = "other_source", length = 100)
	private String otherSource;
	
	@Column (name = "last_income", length = 20)
	private String lastIncome;
	
	@Column (name = "apply_date")
	@Temporal(TemporalType.DATE)
	private Date applyDate;
	
	@OneToOne
	@JsonBackReference
	@JoinColumn(name = "biodata_id", insertable = false, updatable = false)
	private BiodataModel bm;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodataId() {
		return biodataId;
	}

	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}

	public String getVacancySource() {
		return vacancySource;
	}

	public void setVacancySource(String vacancySource) {
		this.vacancySource = vacancySource;
	}

	public String getCandidateType() {
		return candidateType;
	}

	public void setCandidateType(String candidateType) {
		this.candidateType = candidateType;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getCareerCenterName() {
		return careerCenterName;
	}

	public void setCareerCenterName(String careerCenterName) {
		this.careerCenterName = careerCenterName;
	}

	public String getReferrerName() {
		return referrerName;
	}

	public void setReferrerName(String referrerName) {
		this.referrerName = referrerName;
	}

	public String getReferrerPhone() {
		return referrerPhone;
	}

	public void setReferrerPhone(String referrerPhone) {
		this.referrerPhone = referrerPhone;
	}

	public String getReferrerEmail() {
		return referrerEmail;
	}

	public void setReferrerEmail(String referrerEmail) {
		this.referrerEmail = referrerEmail;
	}

	public String getOtherSource() {
		return otherSource;
	}

	public void setOtherSource(String otherSource) {
		this.otherSource = otherSource;
	}

	public String getLastIncome() {
		return lastIncome;
	}

	public void setLastIncome(String lastIncome) {
		this.lastIncome = lastIncome;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	
	

}
