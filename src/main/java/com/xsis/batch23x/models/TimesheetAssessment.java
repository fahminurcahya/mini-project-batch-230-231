package com.xsis.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "x_timesheet_assessment")
public class TimesheetAssessment extends CommonEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "year", nullable = false)
	private int year;
	
	@Column (name = "month", nullable = false)
	private int month;
	
    @Column(name = "placement_id", nullable = false, length = 11)
    private Long placementId;
	
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name="placement_id", insertable = false, updatable = false)
	private PlacementModel placementModel;
	
	@Column (name = "target_result", nullable = false)
	private int targetResult;
	
	@Column (name = "competency", nullable = false)
	private int competencyCompetency;
	
	@Column (name = "discipline", nullable = false)
	private int discipline;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public Long getPlacementId() {
		return placementId;
	}

	public void setPlacementId(Long placementId) {
		this.placementId = placementId;
	}
	
	public PlacementModel getPlacementModel() {
		return placementModel;
	}

	public void setPlacementModel(PlacementModel placementModel) {
		this.placementModel = placementModel;
	}

	public int getTargetResult() {
		return targetResult;
	}

	public void setTargetResult(int targetResult) {
		this.targetResult = targetResult;
	}

	public int getCompetencyCompetency() {
		return competencyCompetency;
	}

	public void setCompetencyCompetency(int competencyCompetency) {
		this.competencyCompetency = competencyCompetency;
	}

	public int getDiscipline() {
		return discipline;
	}

	public void setDiscipline(int discipline) {
		this.discipline = discipline;
	}
	
}
