package com.xsis.batch23x.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "x_timesheet")
public class TimesheetModel extends CommonEntity {
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column (name = "id", length = 11)
		private Long id;
		
		@Column(name = "status", nullable = false, length = 15)
	    private String status;

	    @Column(name = "placement_id", nullable = false, length = 11)
	    private Long placementId;
	    
	    @ManyToOne
		@JsonBackReference
		@JoinColumn(name="placement_id", insertable = false, updatable = false)
		private PlacementModel placementModel;

	    @Column(name = "timesheet_date", nullable = false)
	    @Temporal(TemporalType.DATE)
	    private Date timesheetDate;

	    @Column(name = "start", nullable = false,length = 5)
	    private String start;

	    @Column(name = "selesai", nullable = false, length = 5)
	    private String selesai;

	    @Column(name = "overtime")
	    private boolean overtime = false;

	    @Column(name = "start_ot",length = 5)
	    private String startOt;

	    @Column(name = "end_ot", length = 5)
	    private String endOt;

	    @Column(name = "activity", nullable = false, length = 255)
	    private String activity;

	    @Column(name = "user_approval", length = 50)
	    private String userApproval;

	    @Column(name = "submitted_on", nullable = false)
	    private Date submittedOn;

	    @Column(name = "approved_on")
	    private Date approvedOn;

	    @Column(name = "ero_status", length = 50)
	    private String eroStatus;

	    @Column(name = "sent_on")
	    @Temporal(TemporalType.DATE)
	    private Date sentOn;

	    @Column(name = "collected_on")
	    @Temporal(TemporalType.DATE)
	    private Date collectedOn;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Long getPlacementId() {
			return placementId;
		}

		public void setPlacementId(Long placementId) {
			this.placementId = placementId;
		}

		public PlacementModel getPlacementModel() {
			return placementModel;
		}

		public void setPlacementModel(PlacementModel placementModel) {
			this.placementModel = placementModel;
		}

		public Date getTimesheetDate() {
			return timesheetDate;
		}

		public void setTimesheetDate(Date timesheetDate) {
			this.timesheetDate = timesheetDate;
		}

		public String getStart() {
			return start;
		}

		public void setStart(String start) {
			this.start = start;
		}

		public String getSelesai() {
			return selesai;
		}

		public void setSelesai(String selesai) {
			this.selesai = selesai;
		}

		public boolean isOvertime() {
			return overtime;
		}

		public void setOvertime(boolean overtime) {
			this.overtime = overtime;
		}

		public String getStartOt() {
			return startOt;
		}

		public void setStartOt(String startOt) {
			this.startOt = startOt;
		}

		public String getEndOt() {
			return endOt;
		}

		public void setEndOt(String endOt) {
			this.endOt = endOt;
		}

		public String getActivity() {
			return activity;
		}

		public void setActivity(String activity) {
			this.activity = activity;
		}

		public String getUserApproval() {
			return userApproval;
		}

		public void setUserApproval(String userApproval) {
			this.userApproval = userApproval;
		}

		public Date getSubmittedOn() {
			return submittedOn;
		}

		public void setSubmittedOn(Date submittedOn) {
			this.submittedOn = submittedOn;
		}

		public Date getApprovedOn() {
			return approvedOn;
		}

		public void setApprovedOn(Date approvedOn) {
			this.approvedOn = approvedOn;
		}

		public String getEroStatus() {
			return eroStatus;
		}

		public void setEroStatus(String eroStatus) {
			this.eroStatus = eroStatus;
		}

		public Date getSentOn() {
			return sentOn;
		}

		public void setSentOn(Date sentOn) {
			this.sentOn = sentOn;
		}

		public Date getCollectedOn() {
			return collectedOn;
		}

		public void setCollectedOn(Date collectedOn) {
			this.collectedOn = collectedOn;
		}
	    
	    
}
