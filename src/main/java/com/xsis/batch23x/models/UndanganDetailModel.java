package com.xsis.batch23x.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "x_undangan_detail")
public class UndanganDetailModel extends CommonEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", length = 11)
	private Long id;

	@Column(name = "undangan_id", length = 11)
	private Long undanganId;

	@Column(name = "biodata_id", length = 11)
	private Long biodataId;

	@Column(name = "notes", length = 1000)
	private String notes;

	
	  @ManyToOne(optional = false) 
	  @JoinColumn(name="biodata_id", insertable = false, updatable = false,
	  nullable = false ) private BiodataModel biodatamodel;
	 

	@ManyToOne(optional = false)
	@JoinColumn(name = "undangan_id", insertable = false, updatable = false, nullable = false)
	private UndanganModel undanganModel;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getUndanganId() {
		return undanganId;
	}


	public void setUndanganId(Long undanganId) {
		this.undanganId = undanganId;
	}


	public Long getBiodataId() {
		return biodataId;
	}


	public void setBiodataId(Long biodataId) {
		this.biodataId = biodataId;
	}


	public String getNotes() {
		return notes;
	}


	public void setNotes(String notes) {
		this.notes = notes;
	}


	public BiodataModel getBiodatamodel() {
		return biodatamodel;
	}


	public void setBiodatamodel(BiodataModel biodatamodel) {
		this.biodatamodel = biodatamodel;
	}


	
	 

}
