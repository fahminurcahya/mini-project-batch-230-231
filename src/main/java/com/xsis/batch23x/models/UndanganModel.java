package com.xsis.batch23x.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "x_undangan")
public class UndanganModel extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "id", length = 11)
	private Long id;
	
	@Column (name = "schedule_type_id", length = 11)
	private Long scheduleTypeId;
	
	@Column (name = "invitation_date")
	private LocalDate invitationDate;
	
	@Column (name = "invitation_code", length = 20)
	private String invitationCode;
	
	@Column (name = "time", length = 10)
	private String time;
	
	@Column (name = "ro", length = 11)
	private Long ro;
	
	@Column (name = "tro", length = 11)
	private Long tro;
	
	@Column (name = "other_ro_tro", length = 100)
	private String otherRoTro;
	
	@Column (name = "location", length = 100)
	private String location;
	
	@Column (name = "status", length = 50)
	private String status;
	
	@OneToMany(mappedBy = "undanganModel", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@Column(nullable = true)
	private Set<UndanganDetailModel> undangandetailModel;
	
	public Set<UndanganDetailModel> getUndangandetailModel() {
		return undangandetailModel;
	}

	public void setUndangandetailModel(Set<UndanganDetailModel> undangandetailModel) {
		this.undangandetailModel = undangandetailModel;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getScheduleTypeId() {
		return scheduleTypeId;
	}

	public void setScheduleTypeId(Long scheduleTypeId) {
		this.scheduleTypeId = scheduleTypeId;
	}

	public LocalDate getInvitationDate() {
		return invitationDate;
	}

	public void setInvitationDate(LocalDate invitationDate) {
		this.invitationDate = invitationDate;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getRo() {
		return ro;
	}

	public void setRo(Long ro) {
		this.ro = ro;
	}

	public Long getTro() {
		return tro;
	}

	public void setTro(Long tro) {
		this.tro = tro;
	}

	public String getOtherRoTro() {
		return otherRoTro;
	}

	public void setOtherRoTro(String otherRoTro) {
		this.otherRoTro = otherRoTro;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
	
	

}
