package com.xsis.batch23x.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.batch23x.models.AddressModel;

public interface AddressRepo extends JpaRepository<AddressModel, Long>{

}
