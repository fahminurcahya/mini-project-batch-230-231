package com.xsis.batch23x.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.Biodata2Dto;
import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.models.RoleModel;

@Repository
public interface BiodataRepo extends JpaRepository<BiodataModel, Long> {
	
	  
	  @Query("select new com.xsis.batch23x.dto.BiodataDto(b.id as id,b.fullname as fullname,"
			  + " b.nickName as nickName,b.email as email,b.phoneNumber1 as phoneNumber1,"
			  + " p.major as major,p.schoolName as schoolName, p.educationLevelId as idPendidikan) "
			  + " from BiodataModel b left join RiwayatPendidikanModel p "
			  + " on p.biodataId= b.id "
			  + " where b.isDelete=false and b.isProcess = true and  (p.educationLevelId=?1 OR p.educationLevelId=null)  "
			  + "and (b.fullname LIKE %?2% OR b.nickName LIKE %?3%)") 
	  Page<BiodataDto> findByNamePag(Pageable pageable,Long idPendidikan, String fullname, String nickName);
	 
	  @Query("select new com.xsis.batch23x.dto.BiodataDto(b.id as id,b.fullname as fullname,"
			  + " b.nickName as nickName,b.email as email,b.phoneNumber1 as phoneNumber1,"
			  + " p.major as major,p.schoolName as schoolName, p.educationLevelId as idPendidikan,"
			  + " b.isProcess as isProcess, b.isComplete as isComplete,"
			  + " b.createdBy as createdBy, b.createdOn as createdOn, b.companyId as companyId, b.token as token,"
			  + " b.maritalStatusId as maritalStatusId, b.parentPhoneNumber as parentPhoneNumber, b.identityNo as identityNo,"
			  + " b.identityTypeId as identityTypeId, b.religionId as religionId, b.gender as gender,"
			  + " b.dob as dob, b.pob as pob) "
			  + " from BiodataModel b left join RiwayatPendidikanModel p "
			  + " on p.biodataId= b.id "
			  + " where b.isDelete=false and  (p.educationLevelId=?1 OR p.educationLevelId=null) "
			  + " and b.isComplete= false") 
	  Page<BiodataDto> viewProsesP(Pageable pageable,Long idPendidikan);
	  
	  @Query("select new com.xsis.batch23x.dto.BiodataDto(b.id as id,b.fullname as fullname,"
			  + " b.nickName as nickName,b.email as email,b.phoneNumber1 as phoneNumber1,"
			  + " p.major as major,p.schoolName as schoolName, p.educationLevelId as idPendidikan,"
			  + " b.isProcess as isProcess, b.isComplete as isComplete,"
			  + " b.createdBy as createdBy, b.createdOn as createdOn, b.companyId as companyId, b.token as token,"
			  + " b.maritalStatusId as maritalStatusId, b.parentPhoneNumber as parentPhoneNumber, b.identityNo as identityNo,"
			  + " b.identityTypeId as identityTypeId, b.religionId as religionId, b.gender as gender,"
			  + " b.dob as dob, b.pob as pob) "
			  + " from BiodataModel b left join RiwayatPendidikanModel p "
			  + " on p.biodataId= b.id "
			  + " where b.isDelete=false and  (p.educationLevelId=?1 OR p.educationLevelId=null) "
			  + " and b.isComplete= false") 
	  List<BiodataDto> showAllProsesP(Long idPendidikan);
	  
	  @Query("select b"
				+ " from BiodataModel b WHERE b.isDelete = false")
	  List<BiodataModel> showAllBiodataForCariPelamar();
	  
	  @Query("select b"
				+ " from BiodataModel b WHERE b.isDelete = false"
				+ " and isProcess=true")
		List<BiodataModel> showAllBio();
	  
	  @Query("select b"
				+ " from BiodataModel b WHERE b.isDelete = false"
				+ " and isProcess=true"
				+ " and b.id=?1")
		List<BiodataModel> showBioById(Long idBio);
	  
	  @Query("select b"
				+ " from BiodataModel b WHERE b.isDelete = false"
				+ " and b.id=?1")
		List<BiodataModel> showBioProfil(Long idBio);
	  
	  @Query("select b"
				+ " from BiodataModel b WHERE b.isDelete = false"
				+ " and b.id=?1 and b.isComplete= false")
		List<BiodataModel> viewById(Long id);

	  @Query(value = ("select new com.xsis.batch23x.dto.Biodata2Dto"
				+ "("
				+ " b.id as id,"
				+ " b.fullname as fullname,"
				+ " b.nickName as nickName,"
				+ " b.email as email,"
				+ " b.pob as pob,"
				+ " b.dob as dob,"
				+ " b.gender as gender,"
				+ " b.high as high,"
				+ " b.weight as weight,"
				+ " r.name as agama,"
				+ " b.nationality as nationality,"
				+ " b.ethnic as ethnic,"
				+ " b.hobby as hobby,"
				+ " i.name as jenisId,"
				+ " b.identityNo as identityNo,"
				+ " m.name as martial,"
				+ " b.marriageYear as marriageYear,"
				+ " b.maritalStatusId as maritalId,"
				+ " b.religionId as religionId,"
				+ " b.identityTypeId as identityTypeId,"
				+ " b.phoneNumber1 as phoneNumber1,"
				+ " b.phoneNumber2 as phoneNumber2,"
				+ " b.parentPhoneNumber as parentPhoneNumber,"
				+ " b.childSequence as childSequence,"
				+ " b.howManyBrothers as howManyBrothers,"
				
				
				+ " a.id as id2,"
				+ " a.address1 as address1,"
				+ " a.postalCode1 as postalCode1,"
				+ " a.rt1 as rt1,"
				+ " a.rw1 as rw1,"
				+ " a.kelurahan1 as kelurahan1,"
				+ " a.kecamatan1 as kecamatan1,"
				+ " a.region1 as region1,"
				
				+ " a.address2 as address2,"
				+ " a.postalCode2 as postalCode2,"
				+ " a.rt1 as rt2,"
				+ " a.rw2 as rw2,"
				+ " a.kelurahan2 as kelurahan2,"
				+ " a.kecamatan2 as kecamatan2,"
				+ " a.region2 as region2,"
				+ " b.createdOn as createdOn,"
				+ " b.createdBy as createdBy"
				
				+ " )"
				+ " from BiodataModel b join ReligionModel r "
				+ " ON b.religionId = r.id"
				+ " JOIN IdentityTypeModel i "
				+ " ON i.id = b.identityTypeId "
				+ " JOIN MaritalStatusModel m "
				+ " ON m.id = b.maritalStatusId "
				+ " LEFT JOIN AddressModel a "
				+ " ON b.id = a.biodataId "
				+ " WHERE b.isDelete = false AND b.id=?1"))
			List<Biodata2Dto> showBiodata(Long id);

	  @Query(value = ("select new com.xsis.batch23x.dto.Biodata2Dto"
			  + "("
			  + " b.id as id"
			  
		+ " )"
		+ " from BiodataModel b join ReligionModel r "
		+ " ON b.religionId = r.id"
		+ " JOIN IdentityTypeModel i "
		+ " ON i.id = b.identityTypeId "
		+ " JOIN MaritalStatusModel m "
		+ " ON m.id = b.maritalStatusId "
		+ " LEFT JOIN AddressModel a "
		+ " ON b.id = a.biodataId "
		+ " WHERE b.isDelete = false AND (b.identityTypeId=?1 AND b.identityNo=?2) AND b.id!=?3"))
	  List<Biodata2Dto> validasiIdentitas(Long id, String noin, Long idbio);
	  
	  
	  @Query(value = ("select new com.xsis.batch23x.dto.Biodata2Dto"
			  + "("
			  + " b.id as id"
			  
		+ " )"
		+ " from BiodataModel b join ReligionModel r "
		+ " ON b.religionId = r.id"
		+ " JOIN IdentityTypeModel i "
		+ " ON i.id = b.identityTypeId "
		+ " JOIN MaritalStatusModel m "
		+ " ON m.id = b.maritalStatusId "
		+ " LEFT JOIN AddressModel a "
		+ " ON b.id = a.biodataId "
		+ " WHERE b.isDelete = false AND b.email=?1 AND b.id!=?2"))
	  List<Biodata2Dto> validasiEmail(String email, Long id);
	  
	  


}
