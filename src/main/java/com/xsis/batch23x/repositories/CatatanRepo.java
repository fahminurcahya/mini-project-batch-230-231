package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.CatatanDto;
import com.xsis.batch23x.models.CatatanModel;

@Repository
public interface CatatanRepo extends JpaRepository<CatatanModel, Long> {
	
	/*
	 * @Query("select c from CatatanModel c where c.biodataId=?1 and isDelete=false"
	 * ) List<CatatanModel> findCatatanById(Long id);
	 */
	
	
	  @Query(value="select * from x_catatan catatan left join x_biodata bio  \r\n"
	  + "on catatan.biodata_id = bio.id  \r\n" +
	  "where  bio.id=?1 AND catatan.is_delete = false ",nativeQuery = true )
	  List<CatatanModel> findCatatanByIdBio(Long biodataId);
	 
	 @Query(value="select new com.xsis.batch23x.dto.CatatanDto (c.id as id,"
	 		+ "c.title as title, c.notes as notes, c.biodataId as biodataId, c.noteTypeId as noteTypeId, "
	 		+ " n.name as nameTypeNote, b.nickName as nickName, c.createdBy as createdBy, c.createdOn as createdOn)"
	 		+ " from CatatanModel c left join NoteTypeModel n "
			+ " on c.noteTypeId = n.id "
			+ " left join BiodataModel b on c.biodataId = b.id"
			+ " where c.isDelete=false and c.biodataId=?1") 
	  List<CatatanDto> findCatatanByBio(Long biodataId);	 
	 
	/*
	 * @Query(value="select * from x_catatan catatan \r\n" +
	 * "left join x_biodata bio on catatan.biodata_id = bio.id \r\n" +
	 * "left join x_note_type n on catatan.note_type_id = n.id \r\\n" +
	 * "where bio.id=?1 AND catatan.is_delete = false",nativeQuery = true )
	 * List<CatatanModel> findCatatanByIdBio(Long biodataId);
	 */
	
}
