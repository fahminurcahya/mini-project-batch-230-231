package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.ClientModel;


@Repository
public interface ClientRepo extends JpaRepository<ClientModel,Long>{

	@Query("select DISTINCT c  from  ClientModel c" + 
			"					JOIN PlacementModel p" + 
			"				     ON p.clientId = c.id" + 
			"					JOIN EmployeeModel e" + 
			"					ON p.employeeId = e.id " + 
			"					 JOIN BiodataModel b " + 
			"					ON e.biodataId = b.id " + 
			" 			    	JOIN AddrbookModel a" + 
			"				    ON b.addrbookId = a.id WHERE   a.id=?1")
	List<ClientModel> showClientbyIdAddr(Long idAddr);
	
}
