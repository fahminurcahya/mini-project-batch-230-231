package com.xsis.batch23x.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.CutiComboDto;
import com.xsis.batch23x.models.CutiTahunLaluModel;

@Repository
public interface CutiRepo  extends JpaRepository<CutiTahunLaluModel, Long>{

	@Query("select u"
			+ " from CutiTahunLaluModel u"
			+ " WHERE u.leaveId = ?1 ")
	Optional<CutiTahunLaluModel> showCutiByIdEmployee(Long leaveId);

	
	
}
