package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeLeaveModel;

@Repository
public interface ELRepo extends JpaRepository<EmployeeLeaveModel, Long>{

	@Query("select new com.xsis.batch23x.dto.CutiDto"
			+ "("
			+ " c.id as id,"
			+ " c.modifiedOn as modifiedOn,"
			+ " b.fullname as karyawan,"
			+ " c.employeeId as employeeId,"
			+ " c.regularQuota as regularQuota,"
			+ " c.annualCollectiveLeave as annualCollectiveLeave,"
			+ " c.leaveAlreadyTaken as leaveAlreadyTaken,"
			+ " c.createdBy as createdBy,"
			+ " c.createdOn as createdOn"
			+ " )"
			+ " from  EmployeeModel e left join BiodataModel b "
			+ " on e.biodataId = b.id"
			+ " left join EmployeeLeaveModel c "
			+ " on c.employeeId = e.id"
			+ " WHERE e.isDelete = false AND c.createdBy = ?1 AND"
			+ " (extract(year from c.createdOn) =?2 )")
	List<CutiDto> findPYLQByIdEmployee(Long id, Integer tahun);
	
	@Query("select new com.xsis.batch23x.dto.CutiDto"
			+ "("
			+ " c.id as id,"
			+ " c.modifiedOn as modifiedOn,"
			+ " b.fullname as karyawan,"
			+ " c.employeeId as employeeId,"
			+ " c.regularQuota as regularQuota,"
			+ " c.annualCollectiveLeave as annualCollectiveLeave,"
			+ " c.leaveAlreadyTaken as leaveAlreadyTaken,"
			+ " c.createdBy as createdBy,"
			+ " c.createdOn as createdOn"
			+ " )"
			+ " from  EmployeeModel e left join BiodataModel b "
			+ " on e.biodataId = b.id"
			+ " left join EmployeeLeaveModel c "
			+ " on c.employeeId = e.id"
			+ " WHERE e.isDelete = false AND c.createdBy = ?1 AND"
			+ " (extract(year from c.createdOn) =?2 )")
	List<CutiDto> findByIdEmployee(Long id, Integer tahun);
}
