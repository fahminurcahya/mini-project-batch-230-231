package com.xsis.batch23x.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeLeaveModel;
import com.xsis.batch23x.models.UndanganModel;

@Repository
public interface EmployeeLeaveRepo extends JpaRepository<EmployeeLeaveModel, Long> {

	@Query("select new com.xsis.batch23x.dto.CutiDto"
			+ "("
			+ " c.id as id,"
			+ " c.modifiedOn as modifiedOn,"
			+ " b.fullname as karyawan,"
			+ " c.employeeId as employeeId, "
			+ " c.regularQuota  as regularQuota,"
			+ " c.annualCollectiveLeave as annualCollectiveLeave,"
			+ " c.leaveAlreadyTaken as leaveAlreadyTaken,"
			+ " z.cuti as tahunLalu,"
			+ " c.createdBy as createdBy,"
			+ " c.createdOn as createdOn) "
			+ " from  EmployeeModel e left join BiodataModel b "
			+ " on e.biodataId=b.id"
			+ " left join EmployeeLeaveModel c "
			+ " on c.employeeId=e.id"
			+ " left join CutiTahunLaluModel z"
			+ " on z.leaveId=c.id"
			+ " WHERE  c.isDelete=false and e.isDelete=false  and b.fullname LIKE %?1%"
			+ " and (c.createdOn BETWEEN "
			+ " ?2" + " AND " + "?3 ) and z.tahun LIKE %?4%"
			+ " and z.isDelete=false")
	Page<CutiDto>findByNameKaryawan(
			Pageable pageable,String fullname,Date date1, Date date2, String tahun);
	
	
	
	

	@Query("select new com.xsis.batch23x.dto.CutiDto"
			+ "("
			+ " c.id as id,"
			+ " c.modifiedOn as modifiedOn,"
			+ " b.fullname as karyawan,"
			+ " c.employeeId as employeeId, "
			+ " c.regularQuota  as regularQuota,"
			+ " c.annualCollectiveLeave as annualCollectiveLeave,"
			+ " c.leaveAlreadyTaken as leaveAlreadyTaken,"
			+ " c.createdBy as createdBy,"
			+ " c.createdOn as createdOn"
			+ " )"
			+ " from  EmployeeModel e left join BiodataModel b "
			+ " on e.biodataId=b.id"
			+ " left join EmployeeLeaveModel c "
			+ " on c.employeeId=e.id"
			+ " WHERE e.isDelete=false and c.isDelete=false "
			+ " and (c.createdOn BETWEEN "
			+ " ?1" + " AND " + "?2 )"  )
	List<CutiDto>findAllCuti(Date date1, Date date2);
	
	@Query("select new com.xsis.batch23x.dto.CutiDto"
			+ "("
			+ " c.id as id,"
			+ " c.modifiedOn as modifiedOn,"
			+ " b.fullname as karyawan,"
			+ " c.employeeId as employeeId, "
			+ " c.regularQuota  as regularQuota,"
			+ " c.annualCollectiveLeave as annualCollectiveLeave,"
			+ " c.  leaveAlreadyTaken as leaveAlreadyTaken"
			+ " )"
			+ " from  EmployeeModel e left join BiodataModel b "
			+ " on e.biodataId=b.id"
			+ " left join EmployeeLeaveModel c "
			+ " on c.employeeId=e.id"
			+ " WHERE e.isDelete=false and c.isDelete=false "
			+ " and c.id= ?1 and(c.createdOn BETWEEN "
			+ " ?2" + " AND " + "?3 ) "  )
	List<CutiDto>findCutiById(Long idLeave,Date date1, Date date2);
	
	@Query("select u "
			+ "from EmployeeLeaveModel u"
			+ " WHERE u.isDelete = false")
	List<EmployeeLeaveModel> viewForLastid();
	
	
	 
	
}
