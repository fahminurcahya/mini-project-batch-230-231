package com.xsis.batch23x.repositories;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.CutiComboDto;
import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeModel;
import com.xsis.batch23x.models.UndanganModel;

@Repository
public interface EmployeeRepo extends JpaRepository<EmployeeModel, Long> {
	
	

	@Query("select new com.xsis.batch23x.dto.CutiComboDto"
			+ "("
			+ " e.id as id,"
			+ " b.fullname as karyawan)"
			+ " from  EmployeeModel e "
			+ " left join BiodataModel b "
			+ " on e.biodataId=b.id"
			+ " WHERE e.isDelete=false" )
	 List<CutiComboDto> findKaryawanCombo();
	
//tahun sekarang	
	@Query("select new com.xsis.batch23x.dto.CutiDto"
			+ "("
			+ " e.id as id,"
			+ " c.modifiedOn as modifiedOn,"
			+ " b.fullname as karyawan,"
			+ " c.employeeId as employeeId, "
			+ " c.regularQuota  as regularQuota,"
			+ " c.annualCollectiveLeave as annualCollectiveLeave,"
			+ " c.leaveAlreadyTaken as leaveAlreadyTaken,"
			+ " c.createdBy as createdBy,"
			+ " c.createdOn as createdOn"
			+ " )"
			+ " from  EmployeeModel e left join BiodataModel b "
			+ " on e.biodataId=b.id"
			+ " left join EmployeeLeaveModel c "
			+ " on c.employeeId=e.id"
			+ " WHERE e.isDelete=false and c.isDelete=false and e.id=?1"
			+ " and (c.createdOn BETWEEN "
			+ " ?2" + " AND " + "?3 )"  )
	 List<CutiDto> findKaryawanComboByIdTahunSekarang(Long id,Date date1, Date date2);
	
	@Query("select e"
			+ " from EmployeeModel e"
			+ " WHERE e.isDelete = false and e.isEro = true"
			+ " and e.id = ?1")
	List<EmployeeModel> showEmployeById(Long idEmp);
	
	@Query("select e"
			+ " from EmployeeModel e"
			+ " WHERE e.isDelete = false and e.isEro = true")
	List<EmployeeModel> showEmployeAllIsEro();
	
	
	
	
}
