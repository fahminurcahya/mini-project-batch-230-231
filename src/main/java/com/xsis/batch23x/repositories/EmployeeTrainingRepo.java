package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.EmployeeTrainingDto;
import com.xsis.batch23x.models.EmployeeTrainingModel;
import com.xsis.batch23x.models.RiwayatPelatihanModel;

@Repository
public interface EmployeeTrainingRepo extends JpaRepository<EmployeeTrainingModel, Long> {
	
	
	
	
//	@Query("select new com.xsis.batch23x.dto.EmployeeTrainingDto(et.id as id, et.certificationTypeId as certificationTypeId, s.id as idTipeSertifikasi, "
//			+ "s.name as certificationName, et.employeeId as employeeId, e.id as idKaryawan, e.biodataId as biodataId, b.id as idBiodata, e.name as employeeName, "
//			+ "et.status as status, et.trainingId as trainingId, t.id as idPelatihan, t.name as trainingName, et.trainingOrganizerId as trainingOrganizerId, "
//			+ "o.id as idPenyelenggara, o.name as organizerName, et.trainingTypeId as trainingTypeId, tp.id as idTipePelatihan, tp.name as trainingTypename, "
//			+ "et.trainingDate as trainingDate) "
//			+ "from EmployeeTrainingModel et "
//			+ "left join CertificationTypeModel s on et.certificationTypeId = s.id "
//			+ "left join EmployeeModel e on et.employeeId = e.id "
//			+ "left join BiodataModel b on e.idBiodata = b.id "
//			+ "left join TrainingModel t on et.trainingId = t.id "
//			+ "left join TrainingOrganizerModel o on et.trainingOrganizerId = o.id "
//			+ "left join TrainingTypeModel tp on et.trainingTypeId = tp.id ")
//	List<EmployeeTrainingDto> findEmployeeTraining(Long biodataId);

}
