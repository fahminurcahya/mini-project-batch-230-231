package com.xsis.batch23x.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.batch23x.models.FamilyRelationModel;

public interface FamilyRelationRepo extends JpaRepository<FamilyRelationModel, Long> {

}
