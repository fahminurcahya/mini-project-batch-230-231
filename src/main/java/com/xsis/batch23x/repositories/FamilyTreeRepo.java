package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.FamilyTreeTypeModel;

public interface FamilyTreeRepo extends JpaRepository<FamilyTreeTypeModel, Long> {

	@Query("select f from FamilyTreeTypeModel f where f.isDelete = false")
	Page<FamilyTreeTypeModel> findAllFamilyTree(Pageable pageable);
	
	@Query("select f from FamilyTreeTypeModel f where f.isDelete = false")
	List<FamilyTreeTypeModel> showAll();
	
	@Query("select f from FamilyTreeTypeModel f where f.isDelete = false And (lower(f.name) Like %?1% Or lower(f.description) like %?2%)")
	Iterable<FamilyTreeTypeModel> findByNameOrDesc(String name, String description);
}
