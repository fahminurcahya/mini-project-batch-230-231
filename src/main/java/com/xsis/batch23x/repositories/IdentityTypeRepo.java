package com.xsis.batch23x.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.IdentityTypeModel;

@Repository
public interface IdentityTypeRepo extends JpaRepository<IdentityTypeModel, Long> {

}
