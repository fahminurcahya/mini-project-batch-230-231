package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.KeahlianModel;

@Repository
public interface KeahlianRepo extends JpaRepository<KeahlianModel, Long> {
	
	@Query(value="select * from x_keahlian keahlian left join x_biodata bio  \r\n" + 
			"on keahlian.biodata_id = bio.id  \r\n" + 
			"where  bio.id=?1 AND keahlian.is_delete = false ",nativeQuery = true )
	List<KeahlianModel> findKeahlianByIdBio(Long biodataId);
	
	@Query("select r"
			+ " from KeahlianModel r WHERE r.isDelete = false")
	List<KeahlianModel> findKeahlian();
	
}
