package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.KeluargaDto;
import com.xsis.batch23x.dto.PendidikanDto;
import com.xsis.batch23x.models.KeluargaModel;

@Repository
public interface KeluargaRepo extends JpaRepository<KeluargaModel, Long>{
	
	@Query(value="select * from x_keluarga f left join x_biodata bio  \r\n" + 
			"on f.biodata_id = bio.id  \r\n" + 
			"where  bio.id=?1 AND f.is_delete=false",nativeQuery = true)
	List<KeluargaModel> findKeluargaByIdBio(Long biodataId);
	
	@Query("select new com.xsis.batch23x.dto.KeluargaDto(k.id as id, k.biodataId as biodataId, k.dob as dob, k.educationLevelId as educationLevelId, "
			+ "k.familyRelationId as familyRelationId, k.familyTreeTypeId as familyTreeTypeId, k.gender as gender, k.job as job, k.name as name, b.id as idBiodata, "
			+ "el.id as idEducationLevel, fr.id as idFamilyRelation, ft.id as idFamilyTreeType, ft.name as familyTreeTypeName, fr.name as familyRelationName, el.name as educationLevelName) "
			  + " from KeluargaModel k "
			  + "left join BiodataModel b on k.biodataId = b.id "
			  + "left join EducationLevelModel el on k.educationLevelId = el.id "
			  + "left join FamilyRelationModel fr on k.familyRelationId = fr.id "
			  + "left join FamilyTreeTypeModel ft on k.familyTreeTypeId = ft.id "
			  + " where k.isDelete=false and k.biodataId=?1") 
	  List<KeluargaDto> findKeluargaByBio(Long biodataId);

}
