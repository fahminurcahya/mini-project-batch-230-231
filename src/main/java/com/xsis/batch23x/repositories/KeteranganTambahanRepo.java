package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.KeteranganTambahanModel;

@Repository
public interface KeteranganTambahanRepo extends JpaRepository<KeteranganTambahanModel,Long> {

	

	@Query(value="select keterangan.* from x_keterangan_tambahan keterangan left join x_biodata bio \r\n" + 
			"on keterangan.biodata_id = bio.id \r\n" + 
			"where  bio.id=?1 limit 1",nativeQuery = true)
	List<KeteranganTambahanModel> findKeteranganByBioId(Long BioId);
}
