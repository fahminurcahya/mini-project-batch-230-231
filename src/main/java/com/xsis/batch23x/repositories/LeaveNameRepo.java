package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.LeaveNameModel;

@Repository
public interface LeaveNameRepo extends JpaRepository<LeaveNameModel, Long> {

	List<LeaveNameModel> findLeaveNameById(Long id);

}
