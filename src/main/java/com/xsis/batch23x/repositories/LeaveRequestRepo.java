package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.LeaveRequestModel;

public interface LeaveRequestRepo extends JpaRepository<LeaveRequestModel, Long>{
	@Query("select l from LeaveRequestModel l where l.isDelete = false and l.createdBy = ?1")
	Page<LeaveRequestModel> findAllLeaveRequest(Pageable pageable,Long createdBy);
	
	@Query("select l from LeaveRequestModel l where l.isDelete = false and l.createdBy = ?1")
	List<LeaveRequestModel> showAll(Long createdBy);
	
	@Query("select l from LeaveRequestModel l where l.isDelete = false and l.leaveNameId = 1 and l.createdBy = ?1")
	Iterable<LeaveRequestModel> findBy(Long createdBy);
	
	@Query("select l from LeaveRequestModel l where l.isDelete = false and l.leaveNameId != 1 and l.createdBy = ?1")
	Iterable<LeaveRequestModel> findByKhusus(Long createdBy);
}
