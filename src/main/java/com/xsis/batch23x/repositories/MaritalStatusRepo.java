package com.xsis.batch23x.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.MaritalStatusModel;

@Repository
public interface MaritalStatusRepo extends JpaRepository<MaritalStatusModel, Long> {

}
