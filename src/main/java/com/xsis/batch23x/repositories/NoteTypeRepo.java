package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.NoteTypeModel;

@Repository
public interface NoteTypeRepo extends JpaRepository<NoteTypeModel, Long> {

	List<NoteTypeModel> findNoteTypeById(Long id);

}
