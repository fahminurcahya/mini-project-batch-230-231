package com.xsis.batch23x.repositories;


import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.OrganisasiModel;

@Repository
public interface OrganisasiRepo  extends JpaRepository<OrganisasiModel, Long> {
	
	@Query("select o from OrganisasiModel o where o.biodataId=?1 and isDelete=false")
	List<OrganisasiModel> findOrganisasiByIdBio(Long biodataId);
	
	
	
}
