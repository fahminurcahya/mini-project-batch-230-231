package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.PeReferensiModel;

public interface PeReferensiRepo extends JpaRepository<PeReferensiModel,Long>{


	@Query(value="select ref.* from x_pe_referensi ref left join x_biodata bio \r\n" + 
			"on ref.biodata_id = bio.id \r\n" + 
			"where  bio.id=?1 and ref.is_delete=false ",nativeQuery = true)
	List<PeReferensiModel> findPeReferensiByBioId(Long BioId);
}
