package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.RiwayatPelatihanModel;


@Repository
public interface PelatihanRepo extends JpaRepository<RiwayatPelatihanModel,Long> {
	
	@Query(value="select * from x_riwayat_pelatihan riwayat left join x_biodata bio  \r\n" + 
			"on riwayat.biodata_id = bio.id  \r\n" + 
			"where  bio.id=?1 and riwayat.is_delete=false ",nativeQuery = true)
	List<RiwayatPelatihanModel> findRiwayatPelatihanByBioId(Long BioId);
	
	
	
	
	
}
