package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.dto.PendidikanDto;
import com.xsis.batch23x.models.KeahlianModel;
import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.models.RiwayatPendidikanModel;


@Repository
public interface PendidikanRepo extends JpaRepository<RiwayatPendidikanModel, Long> {
	
	@Query(value="select * from x_riwayat_pendidikan rp left join x_biodata bio  \r\n" + 
			"on rp.biodata_id = bio.id  \r\n" + 
			"where  bio.id=?1 AND rp.is_delete=false",nativeQuery = true)
	List<RiwayatPendidikanModel> findPendidikanByIdBio(Long biodataId);
	
	@Query("select new com.xsis.batch23x.dto.PendidikanDto(p.id as id,p.biodataId as biodataId, p.gpa as gpa,"
			  + " p.schoolName as schoolName, p.country as country, p.entryYear as entryYear, p.graduationYear as graduationYear,"
			  + " p.major as major, p.educationLevelId as educationLevelId, e.name as NameEducation,"
			  + " p.createdBy as createdBy, p.createdOn as createdOn) "
			  + " from RiwayatPendidikanModel p left join EducationLevelModel e "
			  + " on p.educationLevelId = e.id "
			  + " where p.isDelete=false and p.biodataId=?1") 
	  List<PendidikanDto> findPendidikanByBio(Long biodataId);
	 

}
