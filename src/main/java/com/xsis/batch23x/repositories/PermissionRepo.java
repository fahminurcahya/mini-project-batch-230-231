package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.PermissionModel;

@Repository
public interface PermissionRepo extends JpaRepository<PermissionModel, Long> {

	@Query("select p from PermissionModel p where p.isDelete = false")
	Page<PermissionModel> findAllPermission(Pageable pageable);
	
	@Query("select p from PermissionModel p where p.isDelete = false")
	List<PermissionModel> showAll();

}
