package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.dto.PlacementDto;
import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.models.PlacementModel;

public interface PlacementRepo extends JpaRepository<PlacementModel,Long> {
	

	@Query(value = ("SELECT new com.xsis.batch23x.dto.PlacementDto "
			+ "("
			+ "p.id as id,"
			+ "c.name as name,"
			+ "c.userEmail as userEmail,"
			+ "p.isPlacementActive as isPlacementActive,"
			+ "p.employeeId as employeeId,"
			+ "b.fullname as fullname"
			+ ")"
			+ " FROM PlacementModel p "
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId "
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE a.id =?1 "
			+ " AND p.isPlacementActive = true "))
	List<PlacementDto> findClientById(Long id);
	
	
}
