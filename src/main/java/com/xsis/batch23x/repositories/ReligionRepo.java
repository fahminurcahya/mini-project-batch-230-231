package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.ReligionModel;

@Repository
public interface ReligionRepo extends JpaRepository<ReligionModel, Long>{
	@Query("select r from ReligionModel r where r.isDelete = false")
	Page<ReligionModel> findAllReligion(Pageable pageable);
	
	@Query("select r from ReligionModel r where r.isDelete = false")
	List<ReligionModel> showAll();
	
	@Query("select r from ReligionModel r where r.isDelete = false And (lower(r.name) Like %?1% Or lower(r.description) like %?2%)")
	Iterable<ReligionModel> findByNameOrDesc(String name, String description);
}
