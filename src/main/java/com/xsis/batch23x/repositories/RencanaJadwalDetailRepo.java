package com.xsis.batch23x.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.RencanaJadwalDetailModel;
import com.xsis.batch23x.models.UndanganDetailModel;

public interface RencanaJadwalDetailRepo  extends JpaRepository<RencanaJadwalDetailModel, Long>{

	@Query("select u"
			+ " from RencanaJadwalDetailModel u"
			+ " WHERE u.isDelete = false "
			+ " and u.rencanaJadwalId = ?1")
	Optional<RencanaJadwalDetailModel> showjadwalDetailById(Long rencanaJadwalId);
	
}
