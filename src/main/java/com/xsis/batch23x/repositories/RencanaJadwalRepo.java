package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.RencanaJadwalDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.RencanaJadwalModel;

@Repository
public interface RencanaJadwalRepo extends JpaRepository<RencanaJadwalModel, Long> {
	
	@Query("select new com.xsis.batch23x.dto.RencanaJadwalDto("
			+ "u.id as id, "
			+ "u.scheduleDate as scheduleDate,"
			+ "u.time as time,"
			+ "sd.name as schName,"
			+ "u.location as location,"
			+ "u.ro as ro, "
			+ "u.tro as tro,"
			+ "d.id as idDetail,"
			+ "u.scheduleCode as scheduleCode,"
			+ "(select b.fullname from EmployeeModel e left join BiodataModel b on e.biodataId=b.id WHERE e.id=u.ro ) as namaRo,"
			+ "(select b.fullname  from EmployeeModel e left join BiodataModel b on e.biodataId=b.id WHERE e.id=u.tro )as namaTro)"
			+ " from RencanaJadwalModel u left join RencanaJadwalDetailModel d on u.id=d.rencanaJadwalId "
			+ " left join ScheduleTypeModel sd on sd.id=u.scheduleTypeId"
			+ " WHERE u.isDelete=false and d.biodataId=?1")
	List<RencanaJadwalDto> showDetailJadwal(Long idP);
	
}
