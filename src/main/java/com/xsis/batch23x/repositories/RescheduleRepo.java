package com.xsis.batch23x.repositories;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.dto.RescheduleDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.UndanganModel;

public interface RescheduleRepo extends JpaRepository<UndanganModel, Long> {
	
	
	@Query("select new com.xsis.batch23x.dto.RescheduleDto"
			+ "("
			+ "u.id as id, u.invitationCode as invitationCode,"
			+ "u.invitationDate as invitationDate,"
			+ "u.scheduleTypeId as scheduleTypeId,"
			+ "u.ro as ro,"
			+ "u.tro as tro,"
			+ "u.otherRoTro as otherRoTro,"
			+ "u.location as location,"
			+ "u.time as time, "
			+ "d.id as idDetail, "
			+ "d.biodataId as biodataId,"
			+ "d.undanganId as undanganId,"
			+ "d.notes as notes, "
			+ "b.id as idBio, "
			+ "b.fullname as fullname "
			+ ")"
			+ " from UndanganModel u join UndanganDetailModel d on u.id=d.undanganId"
			+ " join BiodataModel b on d.biodataId=b.id"
			+ " WHERE u.isDelete = false and d.isDelete = false and b.fullname LIKE %?1% and u.status = 'sent' ")
	List<RescheduleDto> showAllRsch(String fullname);
	
	
	
	//view tabel
	@Query("select new com.xsis.batch23x.dto.RescheduleDto"
			+ "("
			+ "u.id as id, u.invitationCode as invitationCode,"
			+ "u.invitationDate as invitationDate,"
			+ "u.scheduleTypeId as scheduleTypeId,"
			+ "u.ro as ro,"
			+ "u.tro as tro,"
			+ "u.otherRoTro as otherRoTro,"
			+ "u.location as location,"
			+ "u.time as time, "
			+ "d.id as idDetail, "
			+ "d.biodataId as biodataId,"
			+ "d.undanganId as undanganId,"
			+ "d.notes as notes, "
			+ "b.id as idBio, "
			+ "b.fullname as fullname "
			+ ")"
			+ " from UndanganModel u join UndanganDetailModel d on u.id=d.undanganId"
			+ " join BiodataModel b on d.biodataId=b.id"
			+ " WHERE u.isDelete = false and d.isDelete = false and b.fullname LIKE %?1% and u.status = 'sent' ")
	Page<RescheduleDto> showReschedule(Pageable pageable ,String fullname);
	
	//resch
	@Query("select new com.xsis.batch23x.dto.UndanganDto(u.id as id, u.invitationCode as invitationCode,"
			+ "u.invitationDate as invitationDate,u.scheduleTypeId as scheduleTypeId,"
			+ "u.ro as ro, u.tro as tro,u.otherRoTro as otherRoTro,u.location as location,"
			+ "u.time as time, d.id as idDetail, d.biodataId as biodataId,"
			+ "d.undanganId as undanganId,d.notes as notes, b.id as idBio, b.fullname as fullname,"
			+ "u.createdBy as cByUndangan, u.createdOn as cOnUndangan,"
			+ "d.createdBy as cByDetail, d.createdOn as cOnDetail, b.email as email)"
			+ " from UndanganModel u left join UndanganDetailModel d on u.id=d.undanganId"
			+ " left join BiodataModel b on d.biodataId=b.id"
			+ " WHERE u.isDelete = false "
			+ " and d.id = ?1")
	List<UndanganDto> showUndanganById(Long idUD);
	
	//cek undangan detail yang punya idu yg sama, apakah semuanya false atau masih ada
	@Query("select d.id as id"
			+ " from  UndanganDetailModel d join UndanganModel u "
			+ " ON d.undanganId = u.id"
			+ " WHERE d.isDelete = false AND d.undanganId=?1 AND u.isDelete = false")
	List<?> cekUndangan(Long id);
	
	
	//untuk validasi ro tro 
	@Query("select new com.xsis.batch23x.dto.RescheduleDto"
			+ "("
			+ "u.id as id,"
			+ "u.ro as ro,"
			+ "u.tro as tro"
			+ ")"
			+ " from  UndanganModel u" 
			+ " WHERE u.isDelete = false AND u.invitationDate =?1 AND u.time=?2")
	List<RescheduleDto> cekRoTro(LocalDate tgl, String jam);
	
	
	//nama ro
	@Query("select b.fullname"
			+ " from  UndanganDetailModel d join UndanganModel u "
			+ " ON d.undanganId = u.id"
			+ " join EmployeeModel e "
			+ " ON u.ro = e.id "
			+ " JOIN BiodataModel b "
			+ " ON b.id = e.biodataId "
			+ " WHERE d.isDelete = false AND d.id=?1")
	List<?> nameRo(Long idUD);
	
	//nama tro
	@Query("select b.fullname"
			+ " from  UndanganDetailModel d join UndanganModel u "
			+ " ON d.undanganId = u.id"
			+ " join EmployeeModel e "
			+ " ON u.tro = e.id "
			+ " JOIN BiodataModel b "
			+ " ON b.id = e.biodataId "
			+ " WHERE d.isDelete = false AND d.id=?1")
	List<?> nameTro(Long idUD);
	
	//nama type sch
	@Query("select t.name"
			+ " from  UndanganDetailModel d join UndanganModel u "
			+ " ON d.undanganId = u.id"
			+ " join ScheduleTypeModel t "
			+ " ON u.scheduleTypeId = t.id "
			+ " WHERE d.isDelete = false AND d.id=?1")
	List<?> namesType(Long idUD);
	
//	@Query("select u.id"
//			+ " from  UndanganModel u"
//			+ " WHERE u.isDelete = false AND u.invitationDate=?1 AND u.time=?2 AND u.ro = ?3")
//	Long cekRo(LocalDate tgl, String jam, Long ro);
//	
//	@Query("select u.id"
//			+ " from  UndanganModel u"
//			+ " WHERE u.isDelete = false AND u.invitationDate=?1 AND u.time=?2 AND u.tro = ?3")
//	Long cekTro(LocalDate tgl, String jam, Long tro);
	
	
	
}
