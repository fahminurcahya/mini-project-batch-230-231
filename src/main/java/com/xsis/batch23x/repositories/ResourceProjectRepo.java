package com.xsis.batch23x.repositories;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.ResourceProjectModel;

@Repository
public interface ResourceProjectRepo extends  JpaRepository<ResourceProjectModel,Long>{
	

	@Query("select DISTINCT r from ResourceProjectModel r  JOIN ClientModel c" + 
			"					 ON c.id = r.clientId" + 
			"					JOIN PlacementModel p" + 
			"				     ON p.clientId = c.id" + 
			"					JOIN EmployeeModel e" + 
			"					ON p.employeeId = e.id " + 
			"					 JOIN BiodataModel b " + 
			"					ON e.biodataId = b.id " + 
			" 			    	JOIN AddrbookModel a" + 
			"				    ON b.addrbookId = a.id WHERE r.isDelete = false AND  a.id=?1 ")
	List<ResourceProjectModel> showAll(Long idAddr);
	
	
	@Query(value=" SELECT r.* from x_resource_Project r  JOIN  x_client c ON c.id = r.client_id \r\n" + 
			"				JOIN (SELECT distinct p.employee_id,p.client_id FROM x_placement p) as p \r\n" + 
			"				  ON  c.id = p.client_id \r\n" + 
			"				JOIN x_Employee e \r\n" + 
			"				  ON p.employee_id = e.id \r\n" + 
			"				JOIN x_Biodata b \r\n" + 
			"					ON e.biodata_id = b.id \r\n" + 
			"				JOIN x_Addrbook a \r\n" + 
			"				 ON b.addrbook_id = a.id  WHERE  r.is_Delete = false  AND a.id=?1 ",nativeQuery=true)
	Page<ResourceProjectModel> findAllResource(Long id, Pageable pageable);
	

	@Query("SELECT DISTINCT r FROM ResourceProjectModel r JOIN ClientModel  c" + 
			"					 ON c.id = r.clientId " + 
			"				join PlacementModel p" + 
			"					  ON p.clientId = c.id " + 
			"					JOIN EmployeeModel e "+ 
			"					ON p.employeeId = e.id " + 
			"					 JOIN BiodataModel b " + 
			"					ON e.biodataId = b.id " + 
			"			  		JOIN AddrbookModel a" + 
			"					 ON b.addrbookId = a.id   WHERE r.isDelete = false AND a.id=?4 AND ( lower(r.clientId.name) LIKE %?1%  AND (r.startProject BETWEEN ?2 AND ?3 AND "
			+ " (r.endProject BETWEEN ?2 AND ?3)))")
	Iterable<ResourceProjectModel> findByNameOrDate(String name,Date Date1,Date Date2,Long idAddr);
	
	

	@Query("SELECT DISTINCT r FROM ResourceProjectModel r JOIN ClientModel  c" + 
			"					 ON c.id = r.clientId " + 
			"				join PlacementModel p" + 
			"					  ON p.clientId = c.id " + 
			"					JOIN EmployeeModel e "+ 
			"					ON p.employeeId = e.id " + 
			"					 JOIN BiodataModel b " + 
			"					ON e.biodataId = b.id " + 
			"			  		JOIN AddrbookModel a" + 
			"					 ON b.addrbookId = a.id   WHERE r.isDelete = false AND a.id=?2 AND ( lower(r.clientId.name) LIKE %?1% )")
	Iterable<ResourceProjectModel> findByNameOnly(String name,Long idAddr);

}
