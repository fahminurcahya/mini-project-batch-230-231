package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.dto.PendidikanDto;
import com.xsis.batch23x.models.KeahlianModel;
import com.xsis.batch23x.models.RiwayatPekerjaanModel;
import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.models.RiwayatPendidikanModel;
import com.xsis.batch23x.models.SertifikasiModel;


@Repository
public interface RiwayatPekerjaanRepo extends JpaRepository<RiwayatPekerjaanModel, Long> {
	
	
	@Query("select p"
			+ " from RiwayatPekerjaanModel p")
	Page<RiwayatPekerjaanModel> findAllPekerjaan(Pageable pageable);
	
	@Query("select m"
			+ " from RiwayatPekerjaanModel m"
			+ " WHERE m.biodataId =?1 and isDelete = false")
	List<RiwayatPekerjaanModel> findPekerjaanByIdBio(Long biodataId);
}