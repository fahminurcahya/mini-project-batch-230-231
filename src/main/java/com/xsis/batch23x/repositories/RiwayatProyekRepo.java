package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.RiwayatProyekModel;


public interface RiwayatProyekRepo extends JpaRepository<RiwayatProyekModel, Long>{

	@Query("select p"
			+ " from RiwayatProyekModel p"
			+ " WHERE p.isDelete = false and p.riwayatPekerjaanId = ?1")
	List<RiwayatProyekModel> findProyekByIdPekerjaan(Long pekerjaanId);
	
}
