package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.RoleModel;


public interface RoleRepo extends JpaRepository<RoleModel,Long> {
	
	@Query("select r"
			+ " from RoleModel r WHERE r.isDelete = false")
	List<RoleModel> showAll();
	
	@Query("select r"
			+ " from RoleModel r WHERE r.isDelete = false")
	Page<RoleModel> findAllRole(Pageable pageable);
	
	 @Query("SELECT r FROM RoleModel r WHERE r.isDelete = false AND (lower(r.code) LIKE %?1% OR lower(r.name) LIKE %?2%)")
	Iterable<RoleModel> findByCodeOrName(String code, String name);

}
