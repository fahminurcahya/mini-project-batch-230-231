package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.ScheduleTypeModel;
import com.xsis.batch23x.models.UndanganModel;

public interface ScheduleTypeRepo extends JpaRepository<ScheduleTypeModel, Long>{
	
	@Query("select s"
			+ " from ScheduleTypeModel s"
			+ " WHERE s.isDelete = false "
			+ " and s.id = ?1")
	List<ScheduleTypeModel> showStById(Long idSt);
	
	@Query("select s"
			+ " from ScheduleTypeModel s"
			+ " WHERE s.isDelete = false ")
	List<ScheduleTypeModel> showAllSt();
	
	
	

}
