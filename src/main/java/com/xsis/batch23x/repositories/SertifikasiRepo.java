package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.SertifikasiModel;

@Repository
public interface SertifikasiRepo extends JpaRepository<SertifikasiModel, Long>{

	@Query("select p"
			+ " from SertifikasiModel p")
	Page<SertifikasiModel> findAllSertifikasi(Pageable pageable);
	
	@Query("select o from SertifikasiModel o where o.biodataId=?1 and isDelete=false")
	List<SertifikasiModel> findSertifikasiByIdBio(Long biodataId);
}
