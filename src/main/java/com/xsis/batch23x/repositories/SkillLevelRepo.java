package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.SkillLevelModel;

@Repository
public interface SkillLevelRepo extends JpaRepository<SkillLevelModel, Long> {

	List<SkillLevelModel> findSkillLevelById(Long id);

}
