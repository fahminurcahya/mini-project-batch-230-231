package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.SumberLokerModel;

@Repository
public interface SumberLokerRepo extends JpaRepository<SumberLokerModel, Long> {

	@Query("select sl from SumberLokerModel sl where sl.biodataId=?1 and isDelete=false")
	List<SumberLokerModel> findSumberLokerByBio(Long biodataId);
}
