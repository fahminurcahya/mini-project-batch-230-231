package com.xsis.batch23x.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.batch23x.models.TimePeriodModel;

public interface TimePeriodRepo   extends JpaRepository<TimePeriodModel, Long>{

}
