package com.xsis.batch23x.repositories;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.dto.TimesheetApprovalDto;
import com.xsis.batch23x.models.TimesheetModel;

public interface TimesheetApprovalRepo extends JpaRepository<TimesheetModel, Long>{

	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetApprovalDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overtime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId "
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND"
			+ " t.userApproval = 'submitted' AND t.eroStatus = null AND"
			+ " (extract(year from t.timesheetDate) =?2 AND"
			+ "  extract(month from t.timesheetDate) =?3)"  
			+ " ORDER BY t.timesheetDate"))	
	List<TimesheetApprovalDto> showTimesheet(Long id, Integer tahun, Integer bulan);
	
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetApprovalDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overtime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name,"
			+ " t.placementId as placementId,"
			+ "	t.createdOn as createdOn,"
			+ " t.createdBy as createdBy"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId"
			+ " WHERE t.isDelete = false AND t.id=?1"))
		Optional<TimesheetApprovalDto> findTimesheetApprovalById(Long id);
	
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetApprovalDto"
			+ "("
			+ " t.id as id"
			+ ")"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId"
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND t.timesheetDate=?2"))
		List<TimesheetApprovalDto> validasi(Long id, Date tanggal);
	
	@Query("select t.id as id"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId"
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND t.timesheetDate=?2")
		Long validasi2(Long id, Date tanggal);
}
