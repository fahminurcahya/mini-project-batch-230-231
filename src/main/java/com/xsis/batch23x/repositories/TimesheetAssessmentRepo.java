package com.xsis.batch23x.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.batch23x.models.TimesheetAssessment;

public interface TimesheetAssessmentRepo extends JpaRepository<TimesheetAssessment, Long> {

}
