package com.xsis.batch23x.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.models.TimesheetModel;

public interface TimesheetCollectionRepo extends JpaRepository<TimesheetModel,Long>{
	
	
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overStime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId "
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND"
			+ " (extract(month from t.timesheetDate) =?2 AND"
			+ "  extract(year from t.timesheetDate) =?3)"  
			+ " ORDER BY t.timesheetDate"))	
	List<TimesheetDto> showAllForCollection(Long id,String Client,String pegawai, Integer bulan, Integer tahun);

	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overtime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name,"
			+ " t.placementId as placementId,"
			+ "	t.createdOn as createdOn,"
			+ " t.createdBy as createdBy"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId"
			+ " WHERE t.isDelete = false AND t.id=?1"))
		Optional<TimesheetDto> findTimesheetById(Long id);


}
