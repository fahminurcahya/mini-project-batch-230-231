package com.xsis.batch23x.repositories;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.models.TimesheetModel;

public interface TimesheetRepo extends JpaRepository<TimesheetModel,Long>{
//	@Query(value = "SELECT t.status, t.timesheet_date, t.overtime, \r\n" + 
//			"	t.user_approval, t.ero_status, t.activity,c.name, t.start, \r\n" + 
//			"	t.start_ot, t.end_ot, t.submitted_on, t.sent_on,\r\n" + 
//			"	t.approved_on, t.collected_on \r\n" + 
//			"	FROM public.x_timesheet t JOIN\r\n" + 
//			"	x_placement p ON t.placement_id = p.id\r\n" + 
//			"	JOIN x_client c ON\r\n" + 
//			"	p.client_id = c.id",nativeQuery = true)
//	List<TimesheetModel> showTimesheet();
	
	//Buat menghitung jumlah data aja
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overtime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId "
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND "
			+ " (t.timesheetDate BETWEEN ?2 AND ?3) "  
			+ " ORDER BY t.timesheetDate"))
		List<TimesheetDto> showAll(Long id, Date date1, Date date2);
	
	
	//Buat ditampilin ke tabel
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overtime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId "
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND "
			+ " (t.timesheetDate BETWEEN ?2 AND ?3) "))
		Page<TimesheetDto> showTimesheet(Long id, Date date1, Date date2, Pageable pageable);
	
	
	//edit data
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overtime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name,"
			+ " t.placementId as placementId,"
			+ "	t.createdOn as createdOn,"
			+ " t.createdBy as createdBy"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId"
			+ " WHERE t.isDelete = false AND t.id=?1"))
		Optional<TimesheetDto> findTimesheetById(Long id);
	
	
	//tanggal yang sama di form add
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetDto"
			+ "("
			+ " t.id as id"
			+ ")"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId"
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND t.timesheetDate=?2"))
		List<TimesheetDto> validasi(Long id, Date tanggal);

//	 @Query(value = "SELECT id from x_timesheet where timesheet_date=?1 ", nativeQuery = true)
//	    Long validate(String tanggal);

//	tanggal yang sama di form edit
	@Query("select t.id as id"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId"
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND t.timesheetDate=?2")
	Long validasi2(Long id, Date tanggal);
	
	
}
