package com.xsis.batch23x.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.dto.TimesheetSubmissionDto;
import com.xsis.batch23x.models.TimesheetModel;

public interface TimesheetSubmissionRepo extends JpaRepository<TimesheetModel,Long>{
	
	@Query(value = ("select new com.xsis.batch23x.dto.TimesheetSubmissionDto"
			+ "("
			+ " t.id as id,"
			+ " t.status as status,"
			+ " t.timesheetDate as timesheetDate,"
			+ " t.overtime as overtime,"
			+ " t.userApproval as userApproval,"
			+ " t.eroStatus as eroStatus,"
			+ " t.activity as activity,"
			+ " t.start as start,"
			+ " t.selesai as selesai,"
			+ " t.startOt as startOt,"
			+ " t.endOt as endOt,"
			+ " t.submittedOn as submittedOn,"
			+ " t.sentOn as sentOn,"
			+ " t.approvedOn as approvedOn,"
			+ " t.collectedOn as collectedOn,"
			+ " c.name as name,"
			+ " c.userEmail as userEmail,"
			+ " c.userClientName as userClientName"
			+ " )"
			+ " from  TimesheetModel t join PlacementModel p "
			+ " ON t.placementId = p.id"
			+ " JOIN ClientModel c "
			+ " ON c.id = p.clientId "
			+ " JOIN EmployeeModel e "
			+ " ON p.employeeId = e.id "
			+ " JOIN BiodataModel b "
			+ " ON e.biodataId = b.id "
			+ " JOIN AddrbookModel a "
			+ " ON b.addrbookId = a.id "
			+ " WHERE t.isDelete = false AND a.id=?1 AND"
			+ " (extract(month from t.timesheetDate) =?2 AND"
			+ "  extract(year from t.timesheetDate) =?3) AND t.userApproval = null AND t.eroStatus = null "  
			+ " ORDER BY t.timesheetDate"))	
	List<TimesheetSubmissionDto> showTimesheet(Long id, Integer bulan, Integer tahun);
	
	
	@Modifying
	@Query(value = "UPDATE x_timesheet t "
			+ " SET modified_by=1, "
			+ " modified_on=now(), "
			+ " user_approval='Submitted', "
			+ " submitted_on=now()	"
			+ " FROM x_placement p "	
					+ " JOIN x_client c "
					+ " ON c.id = p.client_id "
					+ " JOIN x_employee e"
					+ " ON e.id = p.employee_id"
					+ " JOIN x_biodata b "
					+ " ON e.biodata_id = b.id "
					+ " JOIN x_addrbook a "
					+ " ON a.id = b.addrbook_id "
			+ " WHERE p.id = t.placement_id AND t.is_delete = false AND a.id=?1"
			+ " AND (extract(month FROM t.timesheet_date) = ?2 "
			+ " AND extract(year FROM t.timesheet_date) = ?3) AND t.user_approval IS NULL AND t.ero_status IS NULL ", 
		    nativeQuery = true)
	int submit(Long id, Integer bulan, Integer tahun);
	
	
}
