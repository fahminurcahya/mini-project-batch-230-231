package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.models.TrainingModel;

@Repository
public interface TrainingRepo extends JpaRepository<TrainingModel, Long> {
	
	@Query(value="select * from x_training",nativeQuery = true)
	List<TrainingModel> findTraining(Long biodataId);

}
