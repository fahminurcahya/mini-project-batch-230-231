package com.xsis.batch23x.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.models.UndanganModel;

public interface UndanganDetailRepo  extends JpaRepository<UndanganDetailModel, Long>{

	@Query("select u"
			+ " from UndanganDetailModel u"
			+ " WHERE u.isDelete = false "
			+ " and u.id = ?1")
	Optional<UndanganDetailModel> showUndanganDetailById(Long id);

	
}
