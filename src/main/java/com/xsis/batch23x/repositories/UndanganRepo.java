package com.xsis.batch23x.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.models.UndanganModel;

@Repository
public interface UndanganRepo extends JpaRepository<UndanganModel, Long>{
	
	@Query("select new com.xsis.batch23x.dto.UndanganDto("
			+ "u.id as id, "
			+ "u.invitationDate as invitationDate,"
			+ "u.ro as ro, "
			+ "u.tro as tro,"
			+ "u.location as location,"
			+ "u.time as time,"
			+ "d.id as idDetail,"
			+ "sd.name as schName,"
			+ "(select b.fullname from EmployeeModel e left join BiodataModel b on e.biodataId=b.id WHERE e.id=u.ro ) as namaRo,"
			+ "(select b.fullname  from EmployeeModel e left join BiodataModel b on e.biodataId=b.id WHERE e.id=u.tro )as namaTro)"
			+ " from UndanganModel u left join UndanganDetailModel d on u.id=d.undanganId "
			+ " left join ScheduleTypeModel sd on sd.id=u.scheduleTypeId"
			+ " WHERE u.isDelete=false and d.isDelete=false and d.biodataId=?1")
	List<UndanganDto> showDetailUndangan(Long idP);
	
	
	@Query("select new com.xsis.batch23x.dto.UndanganDto(u.id as id, u.invitationCode as invitationCode,"
			+ "u.invitationDate as invitationDate,u.scheduleTypeId as scheduleTypeId,"
			+ "u.ro as ro, u.tro as tro,u.otherRoTro as otherRoTro,u.location as location,"
			+ "u.time as time, d.id as idDetail, d.biodataId as biodataId,"
			+ "d.undanganId as undanganId,d.notes as notes, b.id as idBio, b.fullname as fullname, p.id as idP,"
			+ "p.biodataId as idBioP, p.schoolName as schoolName, p.major as major, p.educationLevelId as educationLevelId)"
			+ " from UndanganModel u left join UndanganDetailModel d on u.id=d.undanganId"
			+ " left join BiodataModel b on d.biodataId=b.id"
			+ " left join RiwayatPendidikanModel p on b.id=p.biodataId"
			+ " WHERE u.isDelete = false and d.isDelete=false"
			+ "  and (p.educationLevelId=?1 OR p.educationLevelId=null) and b.fullname LIKE %?2% ")
	Page<UndanganDto> showNameUndangan(Pageable pageable,Long educationLevelId,String fullname);
	
	
	@Query("select new com.xsis.batch23x.dto.UndanganDto("
			+ "u.id as id, "
			+ "u.invitationCode as invitationCode,"
			+ "u.invitationDate as invitationDate,"
			+ "u.scheduleTypeId as scheduleTypeId,"
			+ "u.ro as ro, "
			+ "u.tro as tro,"
			+ "u.otherRoTro as otherRoTro,"
			+ "u.location as location,"
			+ "u.time as time, "
			+ "d.id as idDetail, "
			+ "d.biodataId as biodataId,"
			+ "d.undanganId as undanganId,"
			+ "d.notes as notes, "
			+ "b.id as idBio, "
			+ "b.fullname as fullname,"
			+ "u.createdBy as cByUndangan, "
			+ "u.createdOn as cOnUndangan,"
			+ "d.createdBy as cByDetail, "
			+ "d.createdOn as cOnDetail)"
			+ " from UndanganModel u left join UndanganDetailModel d on u.id=d.undanganId"
			+ " left join BiodataModel b on d.biodataId=b.id"
			+ " WHERE u.isDelete = false and d.isDelete=false and b.fullname LIKE %?1% "
			)
	List<UndanganDto> showAllUndangan(String fullname);
	
	@Query("select u "
			+ "from UndanganModel u"
			+ " WHERE u.isDelete = false")
	List<UndanganModel> viewForLastid();
	
	@Query("select new com.xsis.batch23x.dto.UndanganDto(u.id as id, u.invitationCode as invitationCode,"
			+ "u.invitationDate as invitationDate,u.scheduleTypeId as scheduleTypeId,"
			+ "u.ro as ro, u.tro as tro,u.otherRoTro as otherRoTro,u.location as location,"
			+ "u.time as time, d.id as idDetail, d.biodataId as biodataId,"
			+ "d.undanganId as undanganId,d.notes as notes, b.id as idBio, b.fullname as fullname,"
			+ "u.createdBy as cByUndangan, u.createdOn as cOnUndangan,"
			+ "d.createdBy as cByDetail, d.createdOn as cOnDetail, b.email as email)"
			+ " from UndanganModel u left join UndanganDetailModel d on u.id=d.undanganId"
			+ " left join BiodataModel b on d.biodataId=b.id"
			+ " WHERE u.isDelete = false and d.isDelete=false"
			+ " and u.id = ?1")
	List<UndanganDto> showUndanganById(Long idUndangan);
	
	@Query("select new com.xsis.batch23x.dto.UndanganDto"
			+ "(u.invitationDate as invitationDate, u.ro as ro, u.tro as tro, u.time as time)"
			+ " from UndanganModel u "
			+ " left join UndanganDetailModel d on u.id=d.undanganId"
			+ " WHERE u.isDelete = false and d.isDelete = false")
	List<UndanganDto> CekJadwalRoTro();
	
	

}
