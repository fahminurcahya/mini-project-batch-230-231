package com.xsis.batch23x.restcontroller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.dto.Biodata1Dto;
import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.models.AddressModel;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.repositories.BiodataRepo;
import com.xsis.batch23x.service.AddressService;
import com.xsis.batch23x.service.BiodataService;
import com.xsis.batch23x.service.impl.MailService;

@RestController
@RequestMapping(path="/api/biodata", produces="application/json")
@CrossOrigin(origins = "*")
public class BiodataRestController {
	
	@Autowired
	private BiodataService biodataservice;
	
	@Autowired
	private ModelMapper modelmapper;
	
	@Autowired
	private AddressService addressService ;
	
	
	
	@Autowired
	private MailService notificationService;
	
	@GetMapping("/")
	public ResponseEntity<?>findAll() {
		return new ResponseEntity<>(biodataservice.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/showAllBio")
	public ResponseEntity<?>showAllBio() {
		return new ResponseEntity<>(biodataservice.showAllBio(), HttpStatus.OK);
	}
	
	
	

	@GetMapping("/findByNamePag")
	public ResponseEntity<?> findByNamePag(
			@RequestParam Long idPendidikan,
			@RequestParam String fullname, 
			@RequestParam String nickName, @RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,
			@RequestParam String sortType) {
		
		return new ResponseEntity<>(biodataservice.findByNamePag(idPendidikan,fullname,nickName,pageNo, pageSize, sortBy,sortType), HttpStatus.OK);
	}
	
	@GetMapping("/showBio/{id}")
	public ResponseEntity<?> findOrganisasiByIdBio(@PathVariable("id") Long id){
		return new ResponseEntity<>(biodataservice.showBiodata(id), HttpStatus.OK);		
	}
	
	/// kirim token
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		
		return new ResponseEntity<>(biodataservice.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("send/{id}/{exp}") 
	public ResponseEntity<?>  sendEmail(@PathVariable("id") Long id, @PathVariable("exp") Date expDate) throws ParseException {
		BiodataModel biodataModel = biodataservice.findById(id).get();
		
		String name ="qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM1234567890";
		char[] nama = new char[name.length()];
		for(int i=0; i<name.length(); i++) {
			nama[i]=name.charAt(i);
		}
		String token = RandomStringUtils.random(10,nama);
//		String expToken = strDate;
//		Date dateExp = new SimpleDateFormat("yyyy-MM-dd").parse(expToken);
		System.out.println(expDate);
		biodataModel.setExpiredToken(expDate);

		biodataModel.setToken(token);
		biodataModel.setModifiedBy(1L);
		biodataModel.setModifiedOn(new Date());
		
		try {
			notificationService.sendEmail(biodataModel);
		} catch (MailException mailException) {
			System.out.println(mailException);
		}
        return new ResponseEntity<>(biodataservice.save(biodataModel), HttpStatus.OK);
	}
	
	@PutMapping("/editBio")
	public ResponseEntity<?> putBio(@RequestBody BiodataModel biodataModel) {
		
		biodataModel.setModifiedBy(1L);
		biodataModel.setModifiedOn(new Date());
		biodataModel.setIsDelete(false);
		biodataModel.setCompanyId(1L);
		biodataModel.setIsProcess(true);
		return new ResponseEntity<>(biodataservice.save(biodataModel), HttpStatus.OK);
	}
	@PutMapping("/editAddr")
	public ResponseEntity<?> putAddr(@RequestBody AddressModel addressMod) {
		
		addressMod.setModifiedBy(1L);
		addressMod.setModifiedOn(new Date());
		addressMod.setIsDelete(false);
		return new ResponseEntity<>(addressService.save(addressMod), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> save(@RequestBody BiodataModel roleModel) {

		return new ResponseEntity<>(biodataservice.save(roleModel), HttpStatus.OK);
	}
	
	@GetMapping("/showBioById")
	public ResponseEntity<?> showBioById(
			@RequestParam Long idBio) {
		
		return new ResponseEntity<>(biodataservice.showBioById(idBio), HttpStatus.OK);
	}
	@GetMapping("/showAllProsesP")
	public ResponseEntity<?> showAllProsesP(
			@RequestParam Long idPendidikan) {
		
		return new ResponseEntity<>(biodataservice.showAllProsesP(idPendidikan), HttpStatus.OK);
	}
	
	@GetMapping("/showBioProfil")
	public ResponseEntity<?> showBioProfil(
			@RequestParam Long idBio) {
		
		return new ResponseEntity<>(biodataservice.showBioProfil(idBio), HttpStatus.OK);
	}
	
	@GetMapping("/viewById")
	public ResponseEntity<?> viewById(
			@RequestParam Long id) {
		
		return new ResponseEntity<>(biodataservice.viewById(id), HttpStatus.OK);
	}
	
	//view proses pelamar
	@GetMapping("/viewProsesP")
	public ResponseEntity<?> viewProsesP(
			@RequestParam Long idEducation,
			@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, 
			@RequestParam String sortBy,
			@RequestParam String sortType) {
		
		return new ResponseEntity<>(biodataservice.viewProsesP(idEducation,pageNo, pageSize, sortBy,sortType), HttpStatus.OK);
	}
	

	//proses pelamar
	@PutMapping("/proses")
	public ResponseEntity<?> proseses(@RequestBody BiodataModel biomodel){
		biomodel.setIsProcess(true);
		biomodel.setCreatedBy(biomodel.getCreatedBy());
		biomodel.setCreatedOn(biomodel.getCreatedOn());
		biomodel.setIsDelete(false);
		return new ResponseEntity<>(biodataservice.save(biomodel), HttpStatus.OK);
	}
	

	@PostMapping(value="/insert",consumes = {"application/json"})
	public ResponseEntity<?> saveBiodata(@RequestBody Biodata1Dto biodata1dto) {
		BiodataModel biodata = modelmapper.map(biodata1dto, BiodataModel.class);
		AddressModel address = modelmapper.map(biodata1dto, AddressModel.class);
		
		biodata.setIdentityTypeId(1L);
		biodata.setCompanyId(1L);
		biodata.setCreatedBy(1L);
		biodata.setCreatedOn(new Date());
		biodata.setIsDelete(false);
		biodata.setMaritalStatusId(1L);
		biodata.setReligionId(1L);
		biodata.setToken("adad");
		biodata.setGender(true);
		
		biodata.setChildSequence(biodata1dto.getChildSequence());
		biodata.setDob(biodata1dto.getDob());
		biodata.setPob(biodata1dto.getPob());
		biodata.setFullname(biodata1dto.getFullname());
		biodata.setNickName(biodata1dto.getNickName());
		biodata.setHigh(biodata1dto.getHigh());
		biodata.setWeight(biodata1dto.getWeight());
		biodata.setNationality(biodata1dto.getNationality());
		biodata.setEthnic(biodata1dto.getEthnic());
		biodata.setHobby(biodata1dto.getHobby());
		biodata.setIdentityNo(biodata1dto.getIdentityNo());
		biodata.setEmail(biodata1dto.getEmail());
		biodata.setPhoneNumber1(biodata1dto.getPhoneNumber1());
		biodata.setPhoneNumber2(biodata1dto.getPhoneNumber2());
		biodata.setParentPhoneNumber(biodata1dto.getParentPhoneNumber());
		biodata.setChildSequence(biodata1dto.getChildSequence());
		biodata.setHowManyBrothers(biodata1dto.getHowManyBrothers());
		biodata.setMarriageYear(biodata1dto.getMarriageYear());
		
		biodata.setReligionmodel(biodata1dto.getReligion());
		biodata.setIdentityTypemodel(biodata1dto.getIdentity());
		
	
		address.setAddress1(biodata1dto.getAddress1());
		address.setPostalCode1(biodata1dto.getPostalCode1());
		address.setRt1(biodata1dto.getRt1());
		address.setRw1(biodata1dto.getRw1());
		address.setKelurahan1(biodata1dto.getKelurahan1());
		address.setKecamatan1(biodata1dto.getKecamatan1());
		address.setRegion1(biodata1dto.getRegion1());
		
		address.setAddress2(biodata1dto.getAddress2());
		address.setPostalCode2(biodata1dto.getPostalCode2());
		address.setRt2(biodata1dto.getRt2());
		address.setRw2(biodata1dto.getRw2());
		address.setKelurahan2(biodata1dto.getKelurahan2());
		address.setKecamatan2(biodata1dto.getKecamatan2());
		address.setRegion2(biodata1dto.getRegion2());
		
		address.setBiodataId(biodata1dto.getId());
		
		biodataservice.save(biodata);
		addressService.save(address);
		
		return  new ResponseEntity<>(biodata1dto, HttpStatus.OK);
	}
	
	@PostMapping("/insertBio")
	public ResponseEntity<?> addBio(@RequestBody BiodataModel biodataModel) {
		
		biodataModel.setCreatedBy(1L);
		biodataModel.setCreatedOn(new Date());
		biodataModel.setIsDelete(false);
		biodataModel.setCompanyId(1L);
		biodataModel.setToken("adad");
		return new ResponseEntity<>(biodataservice.save(biodataModel), HttpStatus.OK);
	}
	@PostMapping("/insertAddr")
	public ResponseEntity<?> addAddr(@RequestBody AddressModel addressMod) {
		
		addressMod.setCreatedBy(1L);
		addressMod.setCreatedOn(new Date());
		addressMod.setIsDelete(false);
		return new ResponseEntity<>(addressService.save(addressMod), HttpStatus.OK);
	}

	
	@GetMapping("validasiIdentitas/{id}/{noin}/{idBio}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> valid(@PathVariable("id") Long id,
			@PathVariable("noin") String noin,
			@PathVariable("idBio") Long idBio) {
		
		return new ResponseEntity<>(biodataservice.validasiIdentitas(id,noin, idBio), HttpStatus.OK);
	}
	
	@GetMapping("validasiEmail/{email}/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> valem(@PathVariable("email") String email,
			@PathVariable("id") Long id) {
		
		return new ResponseEntity<>(biodataservice.validasiEmail(email, id), HttpStatus.OK);
	}

}
