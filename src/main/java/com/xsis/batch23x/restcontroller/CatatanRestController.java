package com.xsis.batch23x.restcontroller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.CatatanService;
import com.xsis.batch23x.models.CatatanModel;

@RestController
@RequestMapping(path="/api/catatan",produces="application/json")
@CrossOrigin(origins="*")
public class CatatanRestController {
	
	@Autowired
	private CatatanService catatanService;
	
	// find data Catatan by id biodata
	@GetMapping("/findCatatanByIdBio/{biodataId}")
	public ResponseEntity<?> findCatatanByIdBio(@PathVariable("biodataId") Long biodataId){
		return new ResponseEntity<>(catatanService.findCatatanByIdBio(biodataId), HttpStatus.OK);		
	}
	
	@GetMapping("/")
	public ResponseEntity<?> findAllCatatan(){
		return new ResponseEntity<>(catatanService.findAllCatatan(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?>saveCatatan(@RequestBody CatatanModel catatan){
		catatan.setCreatedBy(1L);
		catatan.setCreatedOn(new Date());
		catatan.setIsDelete(false);
		return new ResponseEntity<>(catatanService.save(catatan), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?>putCatatan(@RequestBody CatatanModel catatan){
		catatan.setCreatedBy(catatan.getCreatedBy());
		catatan.setCreatedOn(catatan.getCreatedOn());
		catatan.setIsDelete(false);
		catatan.setModifiedBy(1L);
		catatan.setModifiedOn(new Date());
		System.out.println(catatan.getTitle());
		return new ResponseEntity<>(catatanService.save(catatan), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public  ResponseEntity<?>  delete(@PathVariable("id")Long id) {
		CatatanModel catatan = catatanService.findById(id).get();
		catatan.setIsDelete(true);
		catatan.setDeletedBy(1L);
		catatan.setDeletedOn(new Date());
		return new ResponseEntity<>(catatanService.save(catatan), HttpStatus.OK);
		}

	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(catatanService.findById(id), HttpStatus.OK);
	}
	
	@GetMapping("/findCatatanByBio")
	public ResponseEntity<?> findCatatanByBio(
			@RequestParam Long biodataId){
		return new ResponseEntity<>(catatanService.findCatatanByBio(biodataId), HttpStatus.OK);
	}
}

