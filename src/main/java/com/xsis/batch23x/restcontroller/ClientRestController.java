package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.ClientService;


@RestController
@RequestMapping(path="/api/client",produces="application/json")
@CrossOrigin(origins="*")
public class ClientRestController {
	
@Autowired
private ClientService cs;
	
	
	@GetMapping("/all")
	public ResponseEntity<?> showAllClient(Long idAddr){
		return new ResponseEntity<>(cs.showClientbyIdAddr(idAddr),HttpStatus.OK);
	}

}
