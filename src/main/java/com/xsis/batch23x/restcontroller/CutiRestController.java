package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.CutiTahunLaluModel;
import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.service.CutiService;

@RestController
@RequestMapping(path="/api/cuti", produces="application/json")
@CrossOrigin(origins = "*")
public class CutiRestController {
	
	@Autowired CutiService a;
	
	//delete
		@DeleteMapping("/delete/{leaveId}")
	    public ResponseEntity<?>  deleteCuti(@PathVariable(value = "leaveId") Long leaveId) {
			CutiTahunLaluModel cuti = a.showCutiByIdEmployee(leaveId).get();
			cuti.setIsDelete(true);
			cuti.setDeletedBy(1L);
			cuti.setDeletedOn(new Date());
	        return new ResponseEntity<>(a.save(cuti), HttpStatus.OK);
	    }
	//save
	@PostMapping("/add")
	public ResponseEntity<?> saveCuti(@RequestBody CutiTahunLaluModel cuti){
		cuti.setCreatedBy(1L);
		cuti.setCreatedOn(new Date());
		cuti.setIsDelete(false);
		
		return new ResponseEntity<>(a.save(cuti), HttpStatus.OK);
	}
	//put
	@PutMapping("/put")
	public ResponseEntity<?> putCuti(@RequestBody CutiTahunLaluModel cuti){
		return new ResponseEntity<>(a.save(cuti), HttpStatus.OK);
	}
	
	
	

}
