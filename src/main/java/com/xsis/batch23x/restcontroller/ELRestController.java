package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.EmployeeLeaveModel;
import com.xsis.batch23x.service.ELService;

@RestController
@RequestMapping(path = "/api/empleave", produces = "application/json")
@CrossOrigin(origins = "*")
public class ELRestController {

	@Autowired
	private ELService ELS;
	
	@GetMapping("/pylq/{id}/{tahun}")
	public ResponseEntity<?> pylq(@PathVariable("id") Long id, @PathVariable("tahun") Integer tahun){
		return new ResponseEntity<>(ELS.findPYLQByIdEmployee(id, tahun), HttpStatus.OK);
	}
	
	@GetMapping("/user/{id}/{tahun}")
	public ResponseEntity<?> user(@PathVariable("id") Long id, @PathVariable("tahun") Integer tahun){
		return new ResponseEntity<>(ELS.findByIdEmployee(id, tahun), HttpStatus.OK);
	}
	
	@PutMapping("/save")
	public ResponseEntity<?> save(@RequestBody EmployeeLeaveModel ELM){
		ELM.setIsDelete(false);
		return new ResponseEntity<>(ELS.save(ELM), HttpStatus.OK);
	}
}
