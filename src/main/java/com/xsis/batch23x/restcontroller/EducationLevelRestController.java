package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.EducationLevelService;


@RestController
@RequestMapping(path="/api/educationLevel",produces="application/json")
@CrossOrigin(origins="*")
public class EducationLevelRestController {
	
@Autowired
private EducationLevelService educationLevelService;

@GetMapping("/")
public ResponseEntity<?> findAllEducationLevel(){
	return new ResponseEntity<>(educationLevelService.findAllEducationLevel(), HttpStatus.OK);
}
		
}
