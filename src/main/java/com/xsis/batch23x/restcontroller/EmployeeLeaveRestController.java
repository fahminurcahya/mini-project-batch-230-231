package com.xsis.batch23x.restcontroller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.EmployeeLeaveModel;
import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.service.EmployeeLeaveService;

@RestController
@RequestMapping(path="/api/employeeL",produces="application/json")
@CrossOrigin(origins="*")
public class EmployeeLeaveRestController {
	
	@Autowired
	private EmployeeLeaveService employeeLeaveService;
	
	@GetMapping("/findAllCuti")
	public ResponseEntity<?> findAllCuti(
			@RequestParam String date1,
			@RequestParam String date2) throws ParseException{
		
		Date dat1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
		
		Date dat2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
		
		return new ResponseEntity<>(employeeLeaveService.findAllCuti(dat1, dat2), HttpStatus.OK);
	}
	
	@GetMapping("/findByNameKaryawan")
	public ResponseEntity<?> findByNameKaryawan(
			@RequestParam String fullname,
			@RequestParam String date1,
			@RequestParam String date2,
			@RequestParam String tahun,
			@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, 
			@RequestParam String sortBy,
			@RequestParam String sortType) throws ParseException {
		
		
		Date dat1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
	
		Date dat2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
		
		
		return new ResponseEntity<>(employeeLeaveService.findByNameKaryawan(fullname,dat1,dat2,tahun,pageNo, pageSize, sortBy,sortType), HttpStatus.OK);
	}
	@PutMapping("/put")
	public ResponseEntity<?> putCuti(@RequestBody EmployeeLeaveModel empl){
		empl.setCreatedBy(empl.getCreatedBy());
		empl.setCreatedOn(empl.getCreatedOn());
		empl.setIsDelete(false);
		empl.setModifiedBy(1L);
		empl.setModifiedOn(new Date());
		return new ResponseEntity<>(employeeLeaveService.save(empl), HttpStatus.OK);
	}
	// save
	@PostMapping("/add")
	public ResponseEntity<?> saveEmployeeLeave(@RequestBody EmployeeLeaveModel empl){
		
		empl.setCreatedBy(1L);
		empl.setCreatedOn(new Date());
		empl.setIsDelete(false);
		
		return new ResponseEntity<>(employeeLeaveService.save(empl), HttpStatus.OK);
	}
	//delete
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?>  deleteEL(@PathVariable(value = "id") Long id) {
		EmployeeLeaveModel empl = employeeLeaveService.findById(id).get();
		empl.setIsDelete(true);
		empl.setDeletedBy(1L);
		empl.setDeletedOn(new Date());
        return new ResponseEntity<>(employeeLeaveService.save(empl), HttpStatus.OK);
    }
	
	
	@GetMapping("/viewForLastid")
	public ResponseEntity<?>viewForLastid() {
		return new ResponseEntity<>(employeeLeaveService.viewForLastid(), HttpStatus.OK);
	}
	
	@GetMapping("/findCutiById")
	public ResponseEntity<?> findCutiById(
			@RequestParam Long idLeave,
			@RequestParam String date1,
			@RequestParam String date2) throws ParseException {
		
		Date dat1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
		
		Date dat2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
		
		return new ResponseEntity<>(employeeLeaveService.findCutiById(idLeave,dat1,dat2), HttpStatus.OK);
	}
	
	
	

}
