package com.xsis.batch23x.restcontroller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.EmployeeService;

@RestController
@RequestMapping(path="/api/employee",produces="application/json")
@CrossOrigin(origins="*")
public class EmployeeRestController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllEmp(){
		return new ResponseEntity<>(employeeService.findAllEmp(), HttpStatus.OK);
	}
	

	@GetMapping("/findKaryawanCombo")
	public ResponseEntity<?> findKaryawanCombo(){
		return new ResponseEntity<>(employeeService.findKaryawanCombo(), HttpStatus.OK);
	}

	@GetMapping("/findKaryawanComboByIdTahunSekarang")
	public ResponseEntity<?> findKaryawanComboById(
			@RequestParam Long id,
			@RequestParam String date1,
			@RequestParam String date2) throws ParseException {
		
		Date dat1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
		System.out.println(dat1);
		Date dat2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
		System.out.println(dat2);
		return new ResponseEntity<>(employeeService.findKaryawanComboByIdTahunSekarang(id,dat1,dat2), HttpStatus.OK);
	}
	
	@GetMapping("/showEmployeById")
	public ResponseEntity<?> showEmployeById(@RequestParam Long idEmp){
		return new ResponseEntity<>(employeeService.showEmployeById(idEmp), HttpStatus.OK);
	}
	
	@GetMapping("/showEmployeAllIsEro")
	public ResponseEntity<?> showEmployeAllIsEro(){
		return new ResponseEntity<>(employeeService.showEmployeAllIsEro(), HttpStatus.OK);
	}
}
