package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.EmployeeTrainingService;

@RestController
@RequestMapping(path="/api/employeeTraining", produces="application/json")
@CrossOrigin(origins="*")
public class EmployeeTrainingRestController {
	
	@Autowired
	private EmployeeTrainingService employeeTrainingService;
	
//	@GetMapping("/findEmployeeTraining")
//	public ResponseEntity<?> findEmployeeTraining(
//			@RequestParam Long biodataId){
//		return new ResponseEntity<>(employeeTrainingService.findEmployeeTraining(biodataId), HttpStatus.OK);
//	}

}
