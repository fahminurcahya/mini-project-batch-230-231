package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.FamilyRelationService;

@RestController
@RequestMapping(path="/api/familyRelation",produces="application/json")
@CrossOrigin(origins="*")
public class FamilyRelationRestController {
	
	@Autowired
	private FamilyRelationService familyRelationService;

	@GetMapping("/")
	public ResponseEntity<?> findAllFamilyRelation(){
		return new ResponseEntity<>(familyRelationService.findAllFamilyRelation(), HttpStatus.OK);
	}

}
