package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.FamilyTreeTypeModel;
import com.xsis.batch23x.service.FamilyTreeService;

@RestController
@RequestMapping(path="/api/familyTree",produces="application/json")
@CrossOrigin(origins="*")
public class FamilyTreeRestController {
	
	@Autowired
	private FamilyTreeService familyTreeService;

	@GetMapping("/")
	public ResponseEntity<?>showAll() {
		return new ResponseEntity<>(familyTreeService.showAll(), HttpStatus.OK);
	}
	
	@GetMapping("/page")
	public ResponseEntity<?> findAllFamilyTree(@RequestParam Integer pageNo, @RequestParam Integer pageSize,
			@RequestParam String sortBy,@RequestParam String sortType){
		return new ResponseEntity<>(familyTreeService.findAllFamilyTree(pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	
	@PostMapping("/add")
	public ResponseEntity<?>saveFamilyTree(@RequestBody FamilyTreeTypeModel familytree){
		familytree.setCreatedBy(1L);
		familytree.setCreatedOn(new Date());
		familytree.setIsDelete(false);
		return new ResponseEntity<>(familyTreeService.save(familytree), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?>putFamilyTree(@RequestBody FamilyTreeTypeModel familytree){
		familytree.setModifiedBy(2L);
		familytree.setModifiedOn(new Date());
		return new ResponseEntity<>(familyTreeService.save(familytree), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> deleteFamilyTree(@PathVariable("id") Long id) {
		FamilyTreeTypeModel familytree = familyTreeService.findById(id).get();
		familytree.setIsDelete(true);
		familytree.setDeletedBy(3L);
		familytree.setDeletedOn(new Date());
		return new ResponseEntity<>(familyTreeService.save(familytree), HttpStatus.OK);
	}
	
	@GetMapping("/findFamilyTreeById/{id}")
	public ResponseEntity<?> findFamilyTreeById(@PathVariable ("id") Long id){
		return new ResponseEntity<>(familyTreeService.findFamilyTreeById(id), HttpStatus.OK);
	}
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(familyTreeService.findById(id), HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<Iterable<FamilyTreeTypeModel>> searchFamilyTree(@RequestParam String name, @RequestParam String description){
		Iterable<FamilyTreeTypeModel> list = familyTreeService.searchFamilyTree(name, description);
		return new ResponseEntity<Iterable<FamilyTreeTypeModel>>(list, HttpStatus.OK);
	}
}