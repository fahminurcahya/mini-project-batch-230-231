package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.IdentityTypeService;

@RestController
@RequestMapping(path="/api/identity", produces="application/json")
@CrossOrigin(origins="*")
public class IdentityTypeRestController {

	@Autowired
	private IdentityTypeService identityservice;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllIdentity(){
		return new ResponseEntity<>(identityservice.findAllIdentity(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(identityservice.findById(id), HttpStatus.OK);
	}
}
