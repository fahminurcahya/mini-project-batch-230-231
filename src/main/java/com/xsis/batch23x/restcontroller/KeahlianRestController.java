package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.KeahlianModel;
import com.xsis.batch23x.service.KeahlianService;

@RestController
@RequestMapping(path="/api/keahlian",produces="application/json")
@CrossOrigin(origins="*")
public class KeahlianRestController {
	
	@Autowired
	private KeahlianService keahlianService;
	
	// find data keahlian by id biodata
	@GetMapping("/findKeahlianByIdBio/{biodataId}")
	public ResponseEntity<?> findKeahlianByIdBio(@PathVariable("biodataId") Long biodataId){
		return new ResponseEntity<>(keahlianService.findKeahlianByIdBio(biodataId), HttpStatus.OK);		
	}
	
	@GetMapping("/")
	public ResponseEntity<?> findAllKeahlian(){
		return new ResponseEntity<>(keahlianService.findKeahlian(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?>saveKeahlian(@RequestBody KeahlianModel keahlian){
		keahlian.setCreatedBy(1L);
		keahlian.setCreatedOn(new Date());
		keahlian.setIsDelete(false);
		return new ResponseEntity<>(keahlianService.save(keahlian), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?>putKeahlian(@RequestBody KeahlianModel keahlian){
		keahlian.setCreatedBy(keahlian.getCreatedBy());
		keahlian.setCreatedOn(keahlian.getCreatedOn());
		keahlian.setIsDelete(false);
		keahlian.setModifiedBy(1L);
		keahlian.setModifiedOn(new Date());
		System.out.println(keahlian.getSkillName());
		return new ResponseEntity<>(keahlianService.save(keahlian), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public  ResponseEntity<?>  delete(@PathVariable("id")Long id) {
		KeahlianModel keahlian = keahlianService.findById(id).get();
		keahlian.setIsDelete(true);
		keahlian.setDeletedBy(1L);
		keahlian.setDeletedOn(new Date());
		return new ResponseEntity<>(keahlianService.save(keahlian), HttpStatus.OK);
		}

	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(keahlianService.findById(id), HttpStatus.OK);
	}
	
}

