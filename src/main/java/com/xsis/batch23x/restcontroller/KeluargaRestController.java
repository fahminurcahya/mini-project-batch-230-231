package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.KeluargaModel;
import com.xsis.batch23x.models.RiwayatPendidikanModel;
import com.xsis.batch23x.service.KeluargaService;
import com.xsis.batch23x.service.PendidikanService;

@RestController
@RequestMapping(path="/api/keluarga", produces="application/json")
@CrossOrigin(origins="*")
public class KeluargaRestController {
	
	@Autowired
	private KeluargaService keluargaService;
	
	@GetMapping("/findKeluargaByIdBio/{biodataId}")
	public ResponseEntity<?> findKeluargaByIdBio(@PathVariable("biodataId") Long biodataId){
		return new ResponseEntity<>(keluargaService.findKeluargaByIdBio(biodataId), HttpStatus.OK);		
	}
	
	@GetMapping("/findKeluargaByBio")
	public ResponseEntity<?> findKeluargaByBio(
			@RequestParam Long biodataId){
		return new ResponseEntity<>(keluargaService.findKeluargaByBio(biodataId), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	  public ResponseEntity<?> saveKeluarga(@RequestBody KeluargaModel keluarga){
		keluarga.setCreatedBy(1L);
		keluarga.setCreatedOn(new Date());
		keluarga.setIsDelete(false);
		return new ResponseEntity<>(keluargaService.save(keluarga), HttpStatus.OK);
	  }
	
	@PutMapping("/put")
	 public ResponseEntity<?> putKeluarga(@RequestBody KeluargaModel keluarga){
		keluarga.setCreatedBy(keluarga.getCreatedBy());
		keluarga.setCreatedOn(keluarga.getCreatedOn());
		keluarga.setIsDelete(false);
		keluarga.setModifiedBy(1L);
		keluarga.setModifiedOn(new Date());
		return new ResponseEntity<>(keluargaService.save(keluarga), HttpStatus.OK);
	  }
	
	@GetMapping("/") public ResponseEntity<?> findAllKeluarga(){ 
		  return new ResponseEntity<>(keluargaService.findAllKeluarga(), HttpStatus.OK); 
	}
	
	@GetMapping("/{id}") public ResponseEntity<?> findById(@PathVariable("id") Long id){ 
		  return new ResponseEntity<>(keluargaService.findById(id), HttpStatus.OK); 
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?>  deleteKeluarga(@PathVariable(value = "id") Long id) {
        KeluargaModel keluargaModel  = keluargaService.findById(id).get();
        keluargaModel.setIsDelete(true);
        keluargaModel.setDeletedBy(1L);
        keluargaModel.setDeletedOn(new Date());
        return new ResponseEntity<>(keluargaService.save(keluargaModel), HttpStatus.OK);
    }

}
