package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.KeteranganTambahanModel;
import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.service.KeteranganTambahanService;


@RestController
@RequestMapping(path="/api/keterangan",produces="application/json")
@CrossOrigin(origins="*")
public class KeteranganTambahanRestController {

	@Autowired
	private KeteranganTambahanService kts;
	@PostMapping("/add")
	public ResponseEntity<?> saveketerangan(@RequestBody KeteranganTambahanModel ktm){
		ktm.setCreatedBy(1L);
		ktm.setCreatedOn(new Date());
		ktm.setIsDelete(false);
		return new ResponseEntity<>(kts.save(ktm),HttpStatus.OK);
	}

	@GetMapping("/{bioId}")
	public ResponseEntity<?> findKeteranganByBioId(@PathVariable("bioId") Long bioId) {
		return new ResponseEntity<>(kts.findKeteranganByBioId(bioId), HttpStatus.OK);
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<?> findKeterangan(@PathVariable("id") Long Id) {
		return new ResponseEntity<>(kts.findById(Id), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?> put(@RequestBody KeteranganTambahanModel keterangan){
		keterangan.setModifiedOn(new Date());
		keterangan.setModifiedBy(1L); 
		keterangan.setIsDelete(false);
		return new ResponseEntity<>(kts.save(keterangan),HttpStatus.OK);
	}
	
}
