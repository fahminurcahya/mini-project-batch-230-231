package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.LeaveNameModel;
import com.xsis.batch23x.service.LeaveNameService;

@RestController
@RequestMapping(path="/api/leavename",produces="application/json")
@CrossOrigin(origins="*")
public class LeaveNameRestController {
	
	@Autowired
	private LeaveNameService leavenameService;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllLeaveName(){
		return new ResponseEntity<>(leavenameService.findAllLeaveName(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?>saveLeaveName(@RequestBody LeaveNameModel leavename){
		return new ResponseEntity<>(leavenameService.save(leavename), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?>putLeaveName(@RequestBody LeaveNameModel leavename){
		return new ResponseEntity<>(leavenameService.save(leavename), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void delete(@PathVariable("id")Long id) {
			try {
				leavenameService.delete(id);
			}catch (EmptyResultDataAccessException e) {		
				// TODO: handle exception
			}
		}
	
	@GetMapping("/findLeaveNameById/{id}")
	public ResponseEntity<?> findLeaveNameById(@PathVariable ("id") Long id){
		return new ResponseEntity<>(leavenameService.findLeaveNameById(id), HttpStatus.OK);
	}
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(leavenameService.findById(id), HttpStatus.OK);
	}
}

