package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.LeaveRequestModel;
import com.xsis.batch23x.service.LeaveRequestService;

@RestController
@RequestMapping(path="/api/leavereq", produces="application/json")
@CrossOrigin(origins="*")
public class LeaveRequestRestController {

	@Autowired
	private LeaveRequestService lrs;
	
	@GetMapping("/")
	public ResponseEntity<?>showAll(Long createdBy) {
		return new ResponseEntity<>(lrs.showAll(createdBy), HttpStatus.OK);
	}
	
	@GetMapping("/page")
	public ResponseEntity<?>findAllLeaveRequest(@RequestParam Long createdBy, @RequestParam Integer pageNo, @RequestParam Integer pageSize,
			@RequestParam String sortBy,@RequestParam String sortType){
		return new ResponseEntity<>(lrs.findAllLeaveRequest(createdBy, pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveLeaveRequest(@RequestBody LeaveRequestModel lrm){
		
		lrm.setCreatedOn(new Date());
		lrm.setIsDelete(false);
		return new ResponseEntity<>(lrs.save(lrm), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id){
		return new ResponseEntity<>(lrs.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> editLeaveRequest(@RequestBody LeaveRequestModel lrm){
		
		lrm.setModifiedOn(new Date());
		return new ResponseEntity<>(lrs.save(lrm), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}/{deleteBy}")
	public ResponseEntity<?> deleteLeaveRequest(@PathVariable("id") Long id, @PathVariable("deleteBy") Long deleteBy) {
		LeaveRequestModel lrm = lrs.findById(id).get();
		lrm.setIsDelete(true);
		lrm.setDeletedBy(deleteBy);
		lrm.setDeletedOn(new Date());
		return new ResponseEntity<>(lrs.save(lrm), HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<Iterable<LeaveRequestModel>> searchLeaveRequest(@RequestParam Long createdBy){
		Iterable<LeaveRequestModel> list = lrs.searchLeaveRequest(createdBy);
		return new ResponseEntity<Iterable<LeaveRequestModel>>(list, HttpStatus.OK);
	}
	
	@GetMapping("/searchkhusus")
	public ResponseEntity<Iterable<LeaveRequestModel>> searchLeaveRequestforKhusus(@RequestParam Long createdBy){
		Iterable<LeaveRequestModel> list = lrs.searchLeaveRequestforKhusus(createdBy);
		return new ResponseEntity<Iterable<LeaveRequestModel>>(list, HttpStatus.OK);
	}
}
