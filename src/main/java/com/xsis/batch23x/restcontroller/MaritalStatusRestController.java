package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.MaritalStatusService;

@RestController
@RequestMapping(path="/api/marital", produces="application/json")
@CrossOrigin(origins="*")
public class MaritalStatusRestController {

	@Autowired
	private MaritalStatusService maritalservice;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllMarital(){
		return new ResponseEntity<>(maritalservice.findAllMarital(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(maritalservice.findById(id), HttpStatus.OK);
	}
}
