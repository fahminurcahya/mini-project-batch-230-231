package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.NoteTypeModel;
import com.xsis.batch23x.service.NoteTypeService;

@RestController
@RequestMapping(path="/api/notetype",produces="application/json")
@CrossOrigin(origins="*")
public class NoteTypeRestController {
	
	@Autowired
	private NoteTypeService notetypeService;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllNoteType(){
		return new ResponseEntity<>(notetypeService.findAllNoteType(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?>saveNoteType(@RequestBody NoteTypeModel notetype){
		return new ResponseEntity<>(notetypeService.save(notetype), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?>putNoteType(@RequestBody NoteTypeModel notetype){
		return new ResponseEntity<>(notetypeService.save(notetype), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void delete(@PathVariable("id")Long id) {
			try {
				notetypeService.delete(id);
			}catch (EmptyResultDataAccessException e) {		
				// TODO: handle exception
			}
		}
	
	@GetMapping("/findNoteTypeById/{id}")
	public ResponseEntity<?> findNoteTypeById(@PathVariable ("id") Long id){
		return new ResponseEntity<>(notetypeService.findNoteTypeById(id), HttpStatus.OK);
	}
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(notetypeService.findById(id), HttpStatus.OK);
	}
}

