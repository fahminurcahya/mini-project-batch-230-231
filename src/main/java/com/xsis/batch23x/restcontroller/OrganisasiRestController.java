package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.service.OrganisasiService;

@RestController
@RequestMapping(path="/api/organisasi", produces="application/json")
@CrossOrigin(origins = "*")
public class OrganisasiRestController {
	
	@Autowired
	private OrganisasiService organisasiservice ;
	// find data organisasi by id biodata
	@GetMapping("/findOrganisasiByIdBio/{biodataId}")
	public ResponseEntity<?> findOrganisasiByIdBio(@PathVariable("biodataId") Long biodataId){
		return new ResponseEntity<>(organisasiservice.findOrganisasiByIdBio(biodataId), HttpStatus.OK);		
	}
	// save
	@PostMapping("/add")
	public ResponseEntity<?> saveOrganisasi(@RequestBody OrganisasiModel organisasi){
		
		organisasi.setCreatedBy(1L);
		organisasi.setCreatedOn(new Date());
		organisasi.setIsDelete(false);
		
		return new ResponseEntity<>(organisasiservice.save(organisasi), HttpStatus.OK);
	}
	//edit
	@PutMapping("/put")
	public ResponseEntity<?> putOrganisasi(@RequestBody OrganisasiModel organisasi){
		organisasi.setCreatedBy(organisasi.getCreatedBy());
		organisasi.setCreatedOn(organisasi.getCreatedOn());
		organisasi.setIsDelete(false);
		organisasi.setModifiedBy(1L);
		organisasi.setModifiedOn(new Date());
		return new ResponseEntity<>(organisasiservice.save(organisasi), HttpStatus.OK);
	}
	//delete
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?>  deleteRole(@PathVariable(value = "id") Long id) {
		OrganisasiModel organisasiModel = organisasiservice.findById(id).get();
		organisasiModel.setIsDelete(true);
		organisasiModel.setDeletedBy(1L);
		organisasiModel.setDeletedOn(new Date());
        return new ResponseEntity<>(organisasiservice.save(organisasiModel), HttpStatus.OK);
    }
	// find data organisasi by id 
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(organisasiservice.findById(id), HttpStatus.OK);
	}
	

	
}
