package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.PeReferensiModel;
import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.service.PeReferensiService;

@RestController
@RequestMapping(path="/api/referensi",produces="application/json")
@CrossOrigin(origins="*")
public class PeReferensiRestController {

	@Autowired
	private PeReferensiService prs;
	

	@GetMapping("/{bioId}")
	public ResponseEntity<?> findPelatihan(@PathVariable("bioId") Long bioId) {
		return new ResponseEntity<>(prs.findPeReferensiByBioId(bioId), HttpStatus.OK);
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<?> findpelatihan(@PathVariable("id") Long Id) {
		return new ResponseEntity<>(prs.findById(Id), HttpStatus.OK);
	}
	@PostMapping("/add")
	public ResponseEntity<?> savevariant(@RequestBody PeReferensiModel pereferensi){
		pereferensi.setCreatedBy(1L);
		pereferensi.setCreatedOn(new Date());
		pereferensi.setIsDelete(false);
		return new ResponseEntity<>(prs.save(pereferensi),HttpStatus.OK);
	}
	@PutMapping("/put")
	public ResponseEntity<?> putCategory(@RequestBody PeReferensiModel pereferensi){
		pereferensi.setModifiedOn(new Date());
		pereferensi.setModifiedBy(1L); 
		pereferensi.setIsDelete(false);
		return new ResponseEntity<>(prs.save(pereferensi),HttpStatus.OK);
	}
	
//	@DeleteMapping("/{id}")
//	@ResponseStatus(HttpStatus.NO_CONTENT)
//	public void deletePeReferensi(@PathVariable("id")Long Id){
//		try {
//			prs.delete(Id);
//		} catch(EmptyResultDataAccessException e) {
//			
//		}
//	}
//delete permanent 
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePeReferensi(@PathVariable("id")Long id){
		PeReferensiModel prm = prs.findById(id).get();
		prm.setIsDelete(true);
		prm.setDeletedBy(1L);
		prm.setDeletedOn(new Date());
		return new ResponseEntity<>(prs.save(prm), HttpStatus.OK);
	}
	
	
	
}
