package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.RiwayatPendidikanModel;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.service.PendidikanService;


@RestController
@RequestMapping(path="/api/pendidikan", produces="application/json")
@CrossOrigin(origins="*")
public class PendidikanRestController {
	
	@Autowired
	private PendidikanService pendidikanService;
	
	@GetMapping("/findPendidikanByIdBio/{biodataId}")
	public ResponseEntity<?> findPendidikanByIdBio(@PathVariable("biodataId") Long biodataId){
		return new ResponseEntity<>(pendidikanService.findPendidikanByIdBio(biodataId), HttpStatus.OK);		
	}
	
	@GetMapping("/") public ResponseEntity<?> findAllPendidikan(){ 
		  return new ResponseEntity<>(pendidikanService.findAllPendidikan(), HttpStatus.OK); 
		  }
	
	@PostMapping("/add")
	  public ResponseEntity<?> savePendidikan(@RequestBody RiwayatPendidikanModel pendidikan){
		pendidikan.setCreatedBy(1L);
		pendidikan.setCreatedOn(new Date());
		pendidikan.setIsDelete(false);
		return new ResponseEntity<>(pendidikanService.save(pendidikan), HttpStatus.OK);
	  }
	
	@PutMapping("/put")
	 public ResponseEntity<?> putPendidikan(@RequestBody RiwayatPendidikanModel pendidikan){
		pendidikan.setCreatedBy(pendidikan.getCreatedBy());
		pendidikan.setCreatedOn(pendidikan.getCreatedOn());
		pendidikan.setIsDelete(false);
		pendidikan.setModifiedBy(1L);
		pendidikan.setModifiedOn(new Date());
		return new ResponseEntity<>(pendidikanService.save(pendidikan), HttpStatus.OK);
	  }
	
	@GetMapping("/{id}") public ResponseEntity<?> findById(@PathVariable("id") Long id){ 
		  return new ResponseEntity<>(pendidikanService.findById(id), HttpStatus.OK); 
		  }
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?>  deletePendidikan(@PathVariable(value = "id") Long id) {
        RiwayatPendidikanModel pendidikanModel  = pendidikanService.findById(id).get();
        pendidikanModel.setIsDelete(true);
        pendidikanModel.setDeletedBy(1L);
        pendidikanModel.setDeletedOn(new Date());
        return new ResponseEntity<>(pendidikanService.save(pendidikanModel), HttpStatus.OK);
    }
	
	@GetMapping("/findPendidikanByBio")
	public ResponseEntity<?> findPendidikanByBio(
			@RequestParam Long biodataId){
		return new ResponseEntity<>(pendidikanService.findPendidikanByBio(biodataId), HttpStatus.OK);
	}

}
