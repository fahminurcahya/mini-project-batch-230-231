package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.PermissionModel;
import com.xsis.batch23x.service.PermissionService;

@RestController
@RequestMapping(path="/api/permission",produces="application/json")
@CrossOrigin(origins="*")
public class PermissionRestController {
	
	@Autowired
	private PermissionService permissionService;
	
	@GetMapping("/")
	public ResponseEntity<?>showAll() {
		return new ResponseEntity<>(permissionService.showAll(), HttpStatus.OK);
	}
	
	@GetMapping("/page")
	public ResponseEntity<?>findAllPermission(@RequestParam Integer pageNo, @RequestParam Integer pageSize,
			@RequestParam String sortBy,@RequestParam String sortType){
		return new ResponseEntity<>(permissionService.findAllPermission(pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> savePermission(@RequestBody PermissionModel permission){
		
		permission.setCreatedBy(1L);
		permission.setCreatedOn(new Date());
		permission.setIsDelete(false);
		permission.setStatus("Submitted");
		permission.setIsAbsent(false);
		permission.setIsComingLate(false);
		permission.setIsEarlyLeave(false);
		permission.setIsSick(false);
		permission.setOthers(false);
		return new ResponseEntity<>(permissionService.save(permission), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id){
		return new ResponseEntity<>(permissionService.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> editLeaveRequest(@RequestBody PermissionModel permission){
		
		permission.setModifiedBy(2L);
		permission.setModifiedOn(new Date());
		return new ResponseEntity<>(permissionService.save(permission), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteLeaveRequest(@PathVariable("id") Long id) {
		PermissionModel permission = permissionService.findById(id).get();
		permission.setIsDelete(true);
		permission.setDeletedBy(3L);
		permission.setDeletedOn(new Date());
		return new ResponseEntity<>(permissionService.save(permission), HttpStatus.OK);
	}

}

