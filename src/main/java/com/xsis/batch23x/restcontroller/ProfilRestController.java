package com.xsis.batch23x.restcontroller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.ProfilModel;
import com.xsis.batch23x.repositories.ProfilRepo;
import com.xsis.batch23x.service.ProfilService;

@RestController
@RequestMapping(path="/api/profil", produces="application/json")
@CrossOrigin(origins="*")
public class ProfilRestController {
	
	@Autowired ProfilService profilService;
	
	@Autowired
	ProfilRepo profilRepo;
	
	@GetMapping("/") 
	public ResponseEntity<?> findAllProfil(){ 
	return new ResponseEntity<>(profilRepo.findAll(), HttpStatus.OK); 
		 }

}
