package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.RencanaJadwalDetailModel;
import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.service.RencanaJadwalDetailService;
import com.xsis.batch23x.service.UndanganDetailService;

@RestController
@RequestMapping(path="/api/jadwalDetail", produces="application/json")
@CrossOrigin(origins = "*")
public class RencanaJadwalDetailRestController {

	@Autowired
	private RencanaJadwalDetailService rencanaJadwalDetailService;
	

	//delete
	@DeleteMapping("/delete/{rencanaJadwalId}")
    public ResponseEntity<?>  deletejadwalDetail(@PathVariable(value = "rencanaJadwalId") Long rencanaJadwalId) {
		RencanaJadwalDetailModel rencanaJadwalDetailModel = rencanaJadwalDetailService.showjadwalDetailById(rencanaJadwalId).get();
		rencanaJadwalDetailModel.setIsDelete(true);
		rencanaJadwalDetailModel.setDeletedBy(1L);
		rencanaJadwalDetailModel.setDeletedOn(new Date());
        return new ResponseEntity<>(rencanaJadwalDetailService.save(rencanaJadwalDetailModel), HttpStatus.OK);
    }
}
