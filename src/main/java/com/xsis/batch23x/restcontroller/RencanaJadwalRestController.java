package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.RencanaJadwalModel;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.service.RencananJadwalService;
import com.xsis.batch23x.service.UndanganService;

@RestController
@RequestMapping(path="/api/jadwal", produces="application/json")
@CrossOrigin(origins = "*")
public class RencanaJadwalRestController {
	
	@Autowired
	private RencananJadwalService rencananJadwalService;
	
	@GetMapping("/showDetailJadwal")
	public ResponseEntity<?> showDetailJadwal(
			@RequestParam Long idP) {
		
		return new ResponseEntity<>(rencananJadwalService.showDetailJadwal(idP), HttpStatus.OK);
	}
	
	//delete
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?>  deleteJadwal(@PathVariable(value = "id") Long id) {
		RencanaJadwalModel rencanaJadwalModel = rencananJadwalService.findById(id).get();
		rencanaJadwalModel.setIsDelete(true);
		rencanaJadwalModel.setDeletedBy(1L);
		rencanaJadwalModel.setDeletedOn(new Date());
        return new ResponseEntity<>(rencananJadwalService.save(rencanaJadwalModel), HttpStatus.OK);
    }

}
