package com.xsis.batch23x.restcontroller;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.dto.RescheduleDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.service.RescheduleService;
import com.xsis.batch23x.service.UndanganDetailService;
import com.xsis.batch23x.service.UndanganService;
import com.xsis.batch23x.service.impl.MailService;


@RestController
@RequestMapping(path="/api/reschedule", produces="application/json")
@CrossOrigin(origins = "*")
public class RescheduleRestController {
	@Autowired
	private RescheduleService reSer;
	@Autowired
	private UndanganDetailService uDetailSer;
	@Autowired
	private UndanganService undanganSer;
	
	@Autowired
	private MailService notificationService;
	

	@GetMapping("/All/")
	public ResponseEntity<?> showReschedule(@RequestParam String fullname) {
		
		return new ResponseEntity<>(reSer.showAllRsch(fullname), HttpStatus.OK);
	}
	
	@GetMapping("/showNameReschedule")
	public ResponseEntity<?> showReschedule(
			@RequestParam String fullname, 
			@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, 
			@RequestParam String sortBy,
			@RequestParam String sortType) {
		
		return new ResponseEntity<>(reSer.showReschedule(fullname, pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@GetMapping("/showUndanganById")
	public ResponseEntity<?> showUndanganById(
			@RequestParam Long idUD) {
		
		return new ResponseEntity<>(reSer.showUndanganById(idUD), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveReschedule(@RequestBody UndanganModel undanganModel){
		
		undanganModel.setCreatedBy(1L);
		undanganModel.setCreatedOn(new Date());
		undanganModel.setIsDelete(false);
		undanganModel.setStatus("sent");
		
		return new ResponseEntity<>(reSer.save(undanganModel), HttpStatus.OK);
	}
	
	@PostMapping("uDetail/add")
	public ResponseEntity<?> saveUndanganDetail(@RequestBody UndanganDetailModel undanganDetailModel){
		
		undanganDetailModel.setCreatedBy(1L);
		undanganDetailModel.setCreatedOn(new Date());
		undanganDetailModel.setIsDelete(false);
		
		return new ResponseEntity<>(uDetailSer.save(undanganDetailModel), HttpStatus.OK);
	}
	
	@GetMapping("validasi/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> validasi(@PathVariable("id") Long id) {
		return new ResponseEntity<>(reSer.cekUndangan(id), HttpStatus.OK);
	}
	
	@GetMapping("validasi_date/{tgl}/{jam}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> validasi2(@DateTimeFormat(iso =DateTimeFormat.ISO.DATE)
										@PathVariable("tgl") LocalDate tgl,
										@PathVariable("jam") String jam) {
		return new ResponseEntity<>(reSer.cekRoTro(tgl,jam), HttpStatus.OK);
	}
	
	
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?>  deleteUndangan(@PathVariable(value = "id") Long id)  {
		UndanganModel undanganModel = undanganSer.findById(id).get();
		undanganModel.setIsDelete(true);
		undanganModel.setDeletedBy(1L);
		undanganModel.setDeletedOn(new Date());
		
//		System.out.println(email);
//		try {
//			notificationService.sendEmailReschedule(email);;
//		} catch (MailException mailException) {
//			System.out.println(mailException);
//		}
		
        return new ResponseEntity<>(undanganSer.save(undanganModel), HttpStatus.OK);
    }
	
	@DeleteMapping("/deleteDetail/{idUndangan}")
    public ResponseEntity<?>  deleteUndanganDetail(@PathVariable(value = "idUndangan") Long idUndangan) {
		UndanganDetailModel undanganDetailModel = uDetailSer.showUndanganDetailById(idUndangan).get();
		undanganDetailModel.setIsDelete(true);
		undanganDetailModel.setDeletedBy(1L);
		undanganDetailModel.setDeletedOn(new Date());
        return new ResponseEntity<>(uDetailSer.save(undanganDetailModel), HttpStatus.OK);
    }
	
	
	//kirim email
	@PostMapping("/kirim/{idUD}/{ro}/{tro}/{type}")
	public ResponseEntity<?> kirim(@PathVariable("idUD") Long idUD,
			@PathVariable("ro") String ro,
			@PathVariable("tro") String tro,
			@PathVariable("type") String type){
		UndanganDto uDto = reSer.showUndanganById(idUD).get(0);
		System.out.println(uDto.getFullname());
		System.out.println(uDto.getEmail());
		
		try {
			notificationService.sendEmailReschedule(uDto,ro,tro,type);;
		} catch (MailException mailException) {
			System.out.println(mailException);
		}
		
		return new ResponseEntity<>(reSer.showUndanganById(idUD), HttpStatus.OK);
	}

	
	//untuk mengecek nama ro
	@GetMapping("ro/{idUD}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> ro(@PathVariable("idUD") Long idUD) {
		return new ResponseEntity<>(reSer.nameRo(idUD), HttpStatus.OK);
	}
	
	//cek nama tro
	@GetMapping("tro/{idUD}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> tro(@PathVariable("idUD") Long idUD) {
		return new ResponseEntity<>(reSer.nameTro(idUD), HttpStatus.OK);
	}
	
	//cek nama type
	@GetMapping("type/{idUD}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> type(@PathVariable("idUD") Long idUD) {
		return new ResponseEntity<>(reSer.namesType(idUD), HttpStatus.OK);
	}
	
	
//	@GetMapping("validasiRo/{tgl}/{jam}/{ro}") //id disini harus sama dengan pathvariable
//	public ResponseEntity<?> validasiRo(@DateTimeFormat(iso =DateTimeFormat.ISO.DATE)
//										@PathVariable("tgl") LocalDate tgl,
//										@PathVariable("jam") String jam,
//										@PathVariable("ro") Long ro) {
//		return new ResponseEntity<>(reSer.cekRo(tgl, jam, ro), HttpStatus.OK);
//	}
	
	
//	@GetMapping("validasiTro/{tgl}/{jam}/{tro}") //id disini harus sama dengan pathvariable
//	public ResponseEntity<?> validasiTro(@DateTimeFormat(iso =DateTimeFormat.ISO.DATE)
//										@PathVariable("tgl") LocalDate tgl,
//										@PathVariable("jam") String jam,
//										@PathVariable("tro") Long tro) {
//		return new ResponseEntity<>(reSer.cekRo(tgl, jam, tro), HttpStatus.OK);
//	}
}
