package com.xsis.batch23x.restcontroller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.ResourceProjectModel;
import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.service.ResourceProjectService;

@RestController
@RequestMapping(path = "/api/resource", produces = "application/json")
@CrossOrigin(origins = "*")
public class ResourceProjectRestController {
	
	@Autowired
	private ResourceProjectService rps;
	
	
	@GetMapping("/")
	public ResponseEntity<?>showAll(Long idAddr) {
		return new ResponseEntity<>(rps.showAll(idAddr), HttpStatus.OK);
	}

	@GetMapping("/{id}") //id disini harus sama dengan
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(rps.findById(id), HttpStatus.OK);
	}
	

	@GetMapping("/page")
	public ResponseEntity<?> findAllResource(Long idAddr,@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,@RequestParam String sortType) {
		return new ResponseEntity<>(rps.findAllResource(idAddr,pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}

	@PostMapping("/add")
	public ResponseEntity<?> saveResource(@RequestBody ResourceProjectModel rpm) {
		rpm.setCreatedBy(1L);
		rpm.setCreatedOn(new Date());
		rpm.setIsDelete(false);
		return new ResponseEntity<>(rps.save(rpm), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> putResource(@RequestBody ResourceProjectModel rpm) {
		rpm.setModifiedBy(1L);
		rpm.setModifiedOn(new Date());
		return new ResponseEntity<>(rps.save(rpm), HttpStatus.OK);
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) throws Exception {
	    final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	    final CustomDateEditor dateEditor = new CustomDateEditor(df, true) {
	        @Override
	        public void setAsText(String text) throws IllegalArgumentException {
	            if ("today".equals(text)) {
	                setValue(new Date());
	            } else {
	                super.setAsText(text);
	            }
	        }
	    };
	    binder.registerCustomEditor(Date.class, dateEditor);
	}
	
	   @GetMapping("/search")
	    public ResponseEntity<Iterable<ResourceProjectModel>> searchResource(@RequestParam(defaultValue="%%") String name ,
	    		@DateTimeFormat(iso =DateTimeFormat.ISO.DATE)
	    		@RequestParam(defaultValue="2000-01-01") Date date1,@DateTimeFormat(iso =DateTimeFormat.ISO.DATE) @RequestParam (defaultValue="today") Date date2,Long idAddr){
	        Iterable<ResourceProjectModel> list =rps.searchResource(name, date1,date2,idAddr);
	        return new ResponseEntity<Iterable<ResourceProjectModel>>(list, HttpStatus.OK);
	    }
	   
	   
	   @GetMapping("/search-name")
	    public ResponseEntity<Iterable<ResourceProjectModel>> searchResourceName(@RequestParam(defaultValue="%%") String name ,Long idAddr){
	        Iterable<ResourceProjectModel> list =rps.searchResourceByName(name, idAddr);
	        return new ResponseEntity<Iterable<ResourceProjectModel>>(list, HttpStatus.OK);
	    }
		@DeleteMapping("/{id}")
		public ResponseEntity<?> deleteResource(@PathVariable("id")Long id){
			ResourceProjectModel rpm = rps.findById(id).get();
			rpm.setIsDelete(true);
			rpm.setDeletedBy(1L);
			rpm.setDeletedOn(new Date());
			return new ResponseEntity<>(rps.save(rpm), HttpStatus.OK);
		}
	   
}
