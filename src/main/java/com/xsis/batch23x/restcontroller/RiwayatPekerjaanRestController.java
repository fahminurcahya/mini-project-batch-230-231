package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.RiwayatPekerjaanModel;
import com.xsis.batch23x.service.RiwayatPekerjaanService;

@RestController
@RequestMapping(path="/api/pekerjaan", produces="application/json")
@CrossOrigin(origins="*")
public class RiwayatPekerjaanRestController {
	
	@Autowired
	private RiwayatPekerjaanService riwayatPekerjaanService;
	
	@GetMapping("/findPekerjaanByIdBio/{biodataId}")
	public ResponseEntity<?> findPekerjaanByIdBio(@PathVariable("biodataId") Long biodataId){
		return new ResponseEntity<>(riwayatPekerjaanService.findPekerjaanByIdBio(biodataId), HttpStatus.OK);		
	}
	
	@GetMapping("/")
	public ResponseEntity<?> findAllPekerjaan(){ 
		  return new ResponseEntity<>(riwayatPekerjaanService.findAllPekerjaan(), HttpStatus.OK); 
		  }

	@PostMapping("/add")
	public ResponseEntity<?> savePekerjaan(@RequestBody RiwayatPekerjaanModel riwayatpekerjaanModel) {
		riwayatpekerjaanModel.setCreatedBy(1L);
		riwayatpekerjaanModel.setCreatedOn(new Date());
		riwayatpekerjaanModel.setIsDelete(false);
		return new ResponseEntity<>(riwayatPekerjaanService.save(riwayatpekerjaanModel), HttpStatus.OK);
	}
	
	@GetMapping("/page")
	public ResponseEntity<?> findAllPekerjaan(@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,@RequestParam String sortType) {
		return new ResponseEntity<>(riwayatPekerjaanService.findAllPekerjaan(pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(riwayatPekerjaanService.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> putPekerjaan(@RequestBody RiwayatPekerjaanModel riwayatpekerjaanModel) {
		riwayatpekerjaanModel.setIsDelete(false);
		riwayatpekerjaanModel.setModifiedBy(1L);
		riwayatpekerjaanModel.setModifiedOn(new Date());
		return new ResponseEntity<>(riwayatPekerjaanService.save(riwayatpekerjaanModel), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?>  deleteSerti(@PathVariable(value = "id") Long id) {
		RiwayatPekerjaanModel riwayatpekerjaanModel = riwayatPekerjaanService.findById(id).get();
		riwayatpekerjaanModel.setIsDelete(true);
		riwayatpekerjaanModel.setDeletedBy(1L);
		riwayatpekerjaanModel.setDeletedOn(new Date());
        return new ResponseEntity<>(riwayatPekerjaanService.save(riwayatpekerjaanModel), HttpStatus.OK);
    }
}
