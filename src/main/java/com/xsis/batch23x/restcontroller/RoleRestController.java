package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.service.RoleService;

@RestController
@RequestMapping(path = "/api/role", produces = "application/json")
@CrossOrigin(origins = "*")
public class RoleRestController {
	@Autowired
	private RoleService roleService;
	
	@GetMapping("/")
	public ResponseEntity<?>showAll() {
		return new ResponseEntity<>(roleService.showAll(), HttpStatus.OK);
	}
	
	@GetMapping("/page")
	public ResponseEntity<?> findAllRole(@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,@RequestParam String sortType) {
		return new ResponseEntity<>(roleService.findAllRole(pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveRole(@RequestBody RoleModel roleModel) {
		
		roleModel.setCreatedBy(1L);
		roleModel.setCreatedOn(new Date());
		roleModel.setIsDelete(false);
		
		return new ResponseEntity<>(roleService.save(roleModel), HttpStatus.OK);
	}

	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(roleService.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> putRole(@RequestBody RoleModel roleModel) {
		
		roleModel.setModifiedBy(1L);
		roleModel.setModifiedOn(new Date());
		System.out.println(roleModel.getName());
		return new ResponseEntity<>(roleService.save(roleModel), HttpStatus.OK);
	}
	
	
	   @GetMapping("/search")
	    public ResponseEntity<Iterable<RoleModel>> searchRole(@RequestParam String code, @RequestParam String name){
	        Iterable<RoleModel> list = roleService.searchRole(code, name);
	        return new ResponseEntity<Iterable<RoleModel>>(list, HttpStatus.OK);
	    }
	   
	   
	   @DeleteMapping("/delete/{id}")
	    public ResponseEntity<?>  deleteRole(@PathVariable(value = "id") Long id) {
	        RoleModel roleModel = roleService.findById(id).get();
	        roleModel.setIsDelete(true);
	        roleModel.setDeletedBy(1L);
	        roleModel.setDeletedOn(new Date());
	        return new ResponseEntity<>(roleService.save(roleModel), HttpStatus.OK);
	    }
	
	
}
