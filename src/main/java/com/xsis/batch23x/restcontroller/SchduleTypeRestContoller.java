package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.ScheduleTypeService;
import com.xsis.batch23x.service.UndanganService;

@RestController
@RequestMapping(path="/api/st", produces="application/json")
@CrossOrigin(origins = "*")
public class SchduleTypeRestContoller {
	
	@Autowired
	private ScheduleTypeService scheduletypeservice;
	
	@GetMapping("/showStById")
	public ResponseEntity<?> showStById(
			@RequestParam Long idSt) {
		
		return new ResponseEntity<>(scheduletypeservice.showStById(idSt), HttpStatus.OK);
	}
	
	@GetMapping("/showAllSt")
	public ResponseEntity<?> showAllSt(){
		return new ResponseEntity<>(scheduletypeservice.showAllSt(), HttpStatus.OK);
	}

}
