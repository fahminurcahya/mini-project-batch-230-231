package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.SertifikasiModel;
import com.xsis.batch23x.service.SertifikasiService;

@RestController
@RequestMapping(path = "/api/sertifikasi", produces = "application/json")
@CrossOrigin(origins = "*")
public class SertifikasiRestController {

	@Autowired
	private SertifikasiService sertifikasiService;
	
	@GetMapping("/")
	public ResponseEntity<?>findAll() {
		return new ResponseEntity<>(sertifikasiService.findAllSertifikasi(), HttpStatus.OK);
	}
	
	@GetMapping("/page")
	public ResponseEntity<?> findAllSertifikasi(@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,@RequestParam String sortType) {
		return new ResponseEntity<>(sertifikasiService.findAllSertifikasi(pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveSertifikasi(@RequestBody SertifikasiModel sertifikasiModel) {
		sertifikasiModel.setCreatedBy(1L);
		sertifikasiModel.setCreatedOn(new Date());
		sertifikasiModel.setIsDelete(false);
		return new ResponseEntity<>(sertifikasiService.save(sertifikasiModel), HttpStatus.OK);
	}

	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(sertifikasiService.findById(id), HttpStatus.OK);
	}
	
	@GetMapping("/findSertifikasiByIdBio/{biodataId}")
	public ResponseEntity<?> findSertifikasiByIdBio(@PathVariable("biodataId") Long biodataId){
		return new ResponseEntity<>(sertifikasiService.findSertifikasiByIdBio(biodataId), HttpStatus.OK);		
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> putSertifikasi(@RequestBody SertifikasiModel sertifikasiModel) {
		sertifikasiModel.setIsDelete(false);
		sertifikasiModel.setModifiedBy(1L);
		sertifikasiModel.setModifiedOn(new Date());
		return new ResponseEntity<>(sertifikasiService.save(sertifikasiModel), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?>  deleteSerti(@PathVariable(value = "id") Long id) {
		SertifikasiModel sertifikasiModel = sertifikasiService.findById(id).get();
		sertifikasiModel.setIsDelete(true);
		sertifikasiModel.setDeletedBy(1L);
		sertifikasiModel.setDeletedOn(new Date());
        return new ResponseEntity<>(sertifikasiService.save(sertifikasiModel), HttpStatus.OK);
    }
}
