package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.SkillLevelModel;
import com.xsis.batch23x.service.SkillLevelService;

@RestController
@RequestMapping(path="/api/skill_level",produces="application/json")
@CrossOrigin(origins="*")
public class SkillLevelRestController {
	
	@Autowired
	private SkillLevelService skill_levelService;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllSkillLevel(){
		return new ResponseEntity<>(skill_levelService.findAllSkillLevel(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?>saveSkillLevel(@RequestBody SkillLevelModel skill_level){
		return new ResponseEntity<>(skill_levelService.save(skill_level), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?>putSkillLevel(@RequestBody SkillLevelModel skill_level){
		return new ResponseEntity<>(skill_levelService.save(skill_level), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void delete(@PathVariable("id")Long id) {
			try {
				skill_levelService.delete(id);
			}catch (EmptyResultDataAccessException e) {		
				// TODO: handle exception
			}
		}
	
	@GetMapping("/findSkillLevelById/{id}")
	public ResponseEntity<?> findSkillLevelById(@PathVariable ("id") Long id){
		return new ResponseEntity<>(skill_levelService.findSkillLevelById(id), HttpStatus.OK);
	}
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(skill_levelService.findById(id), HttpStatus.OK);
	}
}

