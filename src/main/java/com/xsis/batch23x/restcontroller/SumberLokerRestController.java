package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.SumberLokerModel;
import com.xsis.batch23x.service.SumberLokerService;

@RestController
@RequestMapping(path="/api/sumber", produces="application/json")
@CrossOrigin(origins="*")
public class SumberLokerRestController {

	@Autowired
	private SumberLokerService sls;
	
	@GetMapping("/findSumberLokerByBio/{biodataId}")
	public ResponseEntity<?> findSumberLokerByBio(@PathVariable("biodataId") Long biodataId) {
		return new ResponseEntity<>(sls.findSumberLokerByBio(biodataId), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> putSumberLoker(@RequestBody SumberLokerModel slm) {
		slm.setIsDelete(false);
		slm.setModifiedBy(2L);
		slm.setModifiedOn(new Date());
		return new ResponseEntity<>(sls.save(slm), HttpStatus.OK);
	}
}
