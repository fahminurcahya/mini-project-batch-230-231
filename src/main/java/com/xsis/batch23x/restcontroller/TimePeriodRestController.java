package com.xsis.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.TimePeriodService;

@RestController
@RequestMapping(path="/api/timeperiod",produces="application/json")
@CrossOrigin(origins="*")
public class TimePeriodRestController {
	

@Autowired
private TimePeriodService tps;


	@GetMapping("/all")
	public ResponseEntity<?> showAllPeriod(){
		return new ResponseEntity<>(tps.showTimePeriod(),HttpStatus.OK);
	}

}
