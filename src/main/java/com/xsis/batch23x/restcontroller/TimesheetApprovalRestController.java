package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.TimesheetModel;
import com.xsis.batch23x.service.TimesheetApprovalService;

@RestController
@RequestMapping(path = "/api/approval", produces = "application/json")
@CrossOrigin(origins = "*")
public class TimesheetApprovalRestController {
	
	@Autowired
	private TimesheetApprovalService tas;

	@GetMapping("/all/{id}")
	public ResponseEntity<?> all(@PathVariable("id") Long id,
			@RequestParam Integer tahun, @RequestParam Integer bulan){
		return new ResponseEntity<>(tas.showTimesheet(id, tahun, bulan), HttpStatus.OK);
	}
	
	
	/*
	 * @GetMapping("/show/{id}") public ResponseEntity<?>
	 * showTimesheetApproval(@PathVariable("id") Long id,
	 * 
	 * @RequestParam String tahun, @RequestParam String bulan, @RequestParam String
	 * sortBy, @RequestParam String sortType){ return new
	 * ResponseEntity<>(tas.showTimesheetApproval(id, tahun, bulan, sortBy,
	 * sortType), HttpStatus.OK); }
	 */
	 
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findTimesheetApprovalById(@PathVariable("id") Long id){
		return new ResponseEntity<>(tas.findTimesheetById(id), HttpStatus.OK);
	}
	
	@PutMapping("/simpan")
	public ResponseEntity<?> putTimesheet(@RequestBody TimesheetModel tsMod) {
		
		tsMod.setModifiedBy(1L);
		tsMod.setModifiedOn(new Date());
		tsMod.setIsDelete(false);
		return new ResponseEntity<>(tas.save(tsMod), HttpStatus.OK);
	}
}
