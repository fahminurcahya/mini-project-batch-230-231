package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.TimesheetAssessment;
import com.xsis.batch23x.service.TimesheetAssessmentService;

@RestController
@RequestMapping(path = "/api/assessment", produces = "application/json")
@CrossOrigin(origins = "*")
public class TimesheetAssessmentRestController {

	@Autowired
	private TimesheetAssessmentService tas;
	
	@PostMapping("/save")
	public ResponseEntity<?> saveAssessment(@RequestBody TimesheetAssessment tsa){
		
		tsa.setCreatedBy(1L);
		tsa.setCreatedOn(new Date());
		tsa.setIsDelete(false);
		return new ResponseEntity<>(tas.save(tsa), HttpStatus.OK);
	}
}
