package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.service.TimesheetCollectionService;
import com.xsis.batch23x.service.TimesheetService;
@RestController
@RequestMapping(path = "/api/timesheet/collection", produces = "application/json")
@CrossOrigin(origins = "*")
public class TimesheetCollectionRestController {

	@Autowired
	private TimesheetCollectionService tsService;

	@GetMapping("/all/{id}")
	public ResponseEntity<?> allCollect(@PathVariable("idAddr") Long idAddr,String client, String  pegawai,Integer bulan,
			Integer tahun) {
		return new ResponseEntity<>(tsService.showAllForCollection(idAddr,client,pegawai,bulan, tahun) , HttpStatus.OK);
	}
}
