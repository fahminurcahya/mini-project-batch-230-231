package com.xsis.batch23x.restcontroller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.models.TimesheetModel;
import com.xsis.batch23x.service.ResourceProjectService;
import com.xsis.batch23x.service.TimesheetService;

@RestController
@RequestMapping(path = "/api/timesheet", produces = "application/json")
@CrossOrigin(origins = "*")
public class TimesheetRestController {
	

	@Autowired
	private TimesheetService tsService;
	
	
	@GetMapping("/all/{id}")
	public ResponseEntity<?> all(@PathVariable("id") Long id, 
			@DateTimeFormat(iso =DateTimeFormat.ISO.DATE)
			@RequestParam(defaultValue="1000-01-01") Date date1,
			@DateTimeFormat(iso =DateTimeFormat.ISO.DATE) 
			@RequestParam(defaultValue="1000-01-01") Date date2) {
		return new ResponseEntity<>(tsService.showAll(id, date1, date2) , HttpStatus.OK);
	}
	
	@GetMapping("/show/{id}/")
	public ResponseEntity<?> showTimesheet(@PathVariable("id") Long id, 
	@DateTimeFormat(iso =DateTimeFormat.ISO.DATE)
	@RequestParam(defaultValue="1000-01-01") Date date1,
	@DateTimeFormat(iso =DateTimeFormat.ISO.DATE) 
	@RequestParam(defaultValue="1000-01-01") Date date2,
	@RequestParam Integer pageNo,
	@RequestParam Integer pageSize, 
	@RequestParam String sortBy,
	@RequestParam String sortType){
		return new ResponseEntity<>(tsService.showTimesheet(id,date1,date2,pageNo,pageSize,sortBy,sortType) , HttpStatus.OK);
	}

	@PostMapping("/add")
	public ResponseEntity<?> saveRole(@RequestBody TimesheetModel tsMod) {
		
		tsMod.setSubmittedOn(new Date());
		tsMod.setCreatedBy(1L);
		tsMod.setCreatedOn(new Date());
		tsMod.setIsDelete(false);
		
		return new ResponseEntity<>(tsService.save(tsMod), HttpStatus.OK);
	}
//
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findTimesheetById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(tsService.findTimesheetById(id), HttpStatus.OK);
	}
	
	@GetMapping("find/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(tsService.findById(id), HttpStatus.OK);
	}
//	
	@PutMapping("/edit")
	public ResponseEntity<?> putRole(@RequestBody TimesheetModel tsMod) {
		
		tsMod.setModifiedBy(1L);
		tsMod.setModifiedOn(new Date());
		tsMod.setIsDelete(false);
		return new ResponseEntity<>(tsService.save(tsMod), HttpStatus.OK);
	}
	
	@GetMapping("validasi/{id}/{tanggal}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> validasi(@PathVariable("id") Long id, 
								@DateTimeFormat(iso =DateTimeFormat.ISO.DATE)
								@PathVariable("tanggal") Date tanggal) {
		System.out.println(tanggal);
		return new ResponseEntity<>(tsService.validasi2(id,tanggal), HttpStatus.OK);
	}
	
	 @DeleteMapping("/delete/{id}")
	    public ResponseEntity<?>  deletetimesheet(@PathVariable(value = "id") Long id) {
	        TimesheetModel tsMod = tsService.findById(id).get();
	        tsMod.setIsDelete(true);
	        tsMod.setDeletedBy(1L);
	        tsMod.setDeletedOn(new Date());
	        return new ResponseEntity<>(tsService.save(tsMod), HttpStatus.OK);
	    }

}
