package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.service.TimesheetSubmissionService;
import com.xsis.batch23x.service.impl.MailService;


@RestController
@RequestMapping(path = "/api/timesheetsub", produces = "application/json")
@CrossOrigin(origins = "*")
public class TimesheetSubmissionRestController {
	
	@Autowired
	private TimesheetSubmissionService tsSubSer;
	
	@Autowired
	private MailService notificationService;

	@GetMapping("/show/{id}")
	public ResponseEntity<?> showTimesheet(@PathVariable("id") Long id, 
			@RequestParam Integer bulan,
			@RequestParam Integer tahun){
		return new ResponseEntity<>(tsSubSer.showTimesheet(id, bulan, tahun), HttpStatus.OK);
	}
	
	
	//kirim email
	@PostMapping("/lewati/{idAddr}")
	public ResponseEntity<?> skip(
			@PathVariable("idAddr") Long idAddr,
			@RequestParam Integer bulan,
			@RequestParam Integer tahun,
			@RequestParam String email,
			@RequestParam String tglAwal,
			@RequestParam String tglAkhir){
		System.out.println(email);
		try {
			notificationService.sendEmailSubmitted(email, tglAwal, tglAkhir);
		} catch (MailException mailException) {
			System.out.println(mailException);
		}
		
		return new ResponseEntity<>(tsSubSer.submit(idAddr,bulan,tahun), HttpStatus.OK);
	}
	
	@PostMapping("/kirim/{idAddr}")
	public ResponseEntity<?> kirim(
			@PathVariable("idAddr") Long idAddr,
			@RequestParam Integer bulan,
			@RequestParam Integer tahun,
			@RequestParam String email,
			@RequestParam String tglAwal,
			@RequestParam String tglAkhir,
			@RequestParam String cc){
		
		System.out.println(cc);
		try {
			notificationService.sendEmailSubmittedCc(email, tglAwal, tglAkhir,cc);
		} catch (MailException mailException) {
			System.out.println(mailException);
		}
		return new ResponseEntity<>(tsSubSer.submit(idAddr,bulan,tahun), HttpStatus.OK);
	}
}
