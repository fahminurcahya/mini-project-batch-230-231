package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.service.UndanganDetailService;


@RestController
@RequestMapping(path="/api/undanganDetail", produces="application/json")
@CrossOrigin(origins = "*")
public class UndaganDetailRestController {
	@Autowired
	private UndanganDetailService undangandetailservice;
	
	//delete
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?>  deleteUndanganDetail(@PathVariable(value = "id") Long id) {
		UndanganDetailModel undangandetailmodel = undangandetailservice.findById(id).get();
		undangandetailmodel.setIsDelete(true);
		undangandetailmodel.setDeletedBy(1L);
		undangandetailmodel.setDeletedOn(new Date());
        return new ResponseEntity<>(undangandetailservice.save(undangandetailmodel), HttpStatus.OK);
    }
	//save
	@PostMapping("/add")
	public ResponseEntity<?> saveUndanganDetail(@RequestBody UndanganDetailModel undangandetailmodel){
		
		undangandetailmodel.setCreatedBy(1L);
		undangandetailmodel.setCreatedOn(new Date());
		undangandetailmodel.setIsDelete(false);
		
		return new ResponseEntity<>(undangandetailservice.save(undangandetailmodel), HttpStatus.OK);
	}
	//edit
		@PutMapping("/put")
		public ResponseEntity<?> putUndanganDetail(@RequestBody UndanganDetailModel undangandetailmodel){
			undangandetailmodel.setCreatedBy(undangandetailmodel.getCreatedBy());
			undangandetailmodel.setCreatedOn(undangandetailmodel.getCreatedOn());
			undangandetailmodel.setIsDelete(false);
			undangandetailmodel.setModifiedBy(1L);
			undangandetailmodel.setModifiedOn(new Date());
			return new ResponseEntity<>(undangandetailservice.save(undangandetailmodel), HttpStatus.OK);
		}
	
}
