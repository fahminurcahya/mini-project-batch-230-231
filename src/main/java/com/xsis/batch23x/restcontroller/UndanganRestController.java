package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.UndanganModel;


import com.xsis.batch23x.service.UndanganService;

@RestController
@RequestMapping(path="/api/undangan", produces="application/json")
@CrossOrigin(origins = "*")
public class UndanganRestController {
	
	@Autowired
	private UndanganService undanganservice;
	
	@GetMapping("/CekJadwalRoTro")
	public ResponseEntity<?>CekJadwalRoTro() {
		return new ResponseEntity<>(undanganservice.CekJadwalRoTro(), HttpStatus.OK);
	}
	@GetMapping("/")
	public ResponseEntity<?>showAllUndangan(@RequestParam String fullname) {
		return new ResponseEntity<>(undanganservice.showAllUndangan(fullname), HttpStatus.OK);
	}
	@GetMapping("/viewForLastid")
	public ResponseEntity<?>viewForLastid() {
		return new ResponseEntity<>(undanganservice.viewForLastid(), HttpStatus.OK);
	}
	@GetMapping("/showNameUndangan")
	public ResponseEntity<?> showNameUndangan(
			@RequestParam Long educationLevelId,
			@RequestParam String fullname, 
			@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, 
			@RequestParam String sortBy,
			@RequestParam String sortType) {
		
		return new ResponseEntity<>(undanganservice.showNameUndangan(educationLevelId,fullname,pageNo, pageSize, sortBy,sortType), HttpStatus.OK);
	}
	@GetMapping("/showUndanganById")
	public ResponseEntity<?> showUndanganById(
			@RequestParam Long idUndangan) {
		
		return new ResponseEntity<>(undanganservice.showUndanganById(idUndangan), HttpStatus.OK);
	}
	
	@GetMapping("/showDetailUndangan")
	public ResponseEntity<?> showDetailUndangan(
			@RequestParam Long idP) {
		
		return new ResponseEntity<>(undanganservice.showDetailUndangan(idP), HttpStatus.OK);
	}
	//delete
		@DeleteMapping("/delete/{id}")
	    public ResponseEntity<?>  deleteUndangan(@PathVariable(value = "id") Long id) {
			UndanganModel undanganmodel = undanganservice.findById(id).get();
			undanganmodel.setIsDelete(true);
			undanganmodel.setDeletedBy(1L);
			undanganmodel.setDeletedOn(new Date());
	        return new ResponseEntity<>(undanganservice.save(undanganmodel), HttpStatus.OK);
	    }
		//save
		@PostMapping("/add")
		public ResponseEntity<?> saveUndangan(@RequestBody UndanganModel undanganmodel){
			
			undanganmodel.setStatus("sent");
			undanganmodel.setCreatedBy(1L);
			undanganmodel.setCreatedOn(new Date());
			undanganmodel.setIsDelete(false);
			
			return new ResponseEntity<>(undanganservice.save(undanganmodel), HttpStatus.OK);
		}
		//edit
		@PutMapping("/put")
		public ResponseEntity<?> putUndangan(@RequestBody UndanganModel undanganmodel){
			undanganmodel.setStatus("modified");
			undanganmodel.setCreatedBy(undanganmodel.getCreatedBy());
			undanganmodel.setCreatedOn(undanganmodel.getCreatedOn());
			undanganmodel.setIsDelete(false);
			undanganmodel.setModifiedBy(1L);
			undanganmodel.setModifiedOn(new Date());
			return new ResponseEntity<>(undanganservice.save(undanganmodel), HttpStatus.OK);
		}


}
