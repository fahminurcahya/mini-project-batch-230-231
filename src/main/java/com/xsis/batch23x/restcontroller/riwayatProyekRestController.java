package com.xsis.batch23x.restcontroller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.models.RiwayatProyekModel;
import com.xsis.batch23x.service.RiwayatProyekService;

@RestController
@RequestMapping(path="/api/proyek", produces="application/json")
@CrossOrigin(origins="*")
public class riwayatProyekRestController {

	@Autowired
	private RiwayatProyekService rps;
	
	
	
	@GetMapping("/find/{id}")
	public ResponseEntity<?> findpelatihan(@PathVariable("id") Long Id) {
		return new ResponseEntity<>(rps.findById(Id), HttpStatus.OK);
	}

	@GetMapping("/{idPekerjaan}")
	public ResponseEntity<?> findProyekByIdPekerjaan(@PathVariable("idPekerjaan") Long idPekerjaan) {
		return new ResponseEntity<>(rps.findProyekByIdPekerjaan(idPekerjaan), HttpStatus.OK);
	}
	@PostMapping("/add")
	public ResponseEntity<?> savePelatihan(@RequestBody RiwayatProyekModel rp){
		rp.setCreatedBy(1L);
		rp.setCreatedOn(new Date());
		rp.setIsDelete(false);
		return new ResponseEntity<>(rps.save(rp),HttpStatus.OK);
	}
	@PutMapping("/put")
	public ResponseEntity<?> editPelatihan(@RequestBody RiwayatProyekModel rp){
		rp.setModifiedOn(new Date());
		rp.setModifiedBy(1L); 
		rp.setIsDelete(false);
		return new ResponseEntity<>(rps.save(rp),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> setdeletePelatihan(@PathVariable("id")Long id){
		RiwayatProyekModel rpm = rps.findById(id).get();
		rpm.setIsDelete(true);
		rpm.setDeletedBy(1L);
		rpm.setDeletedOn(new Date());
		return new ResponseEntity<>(rps.save(rpm), HttpStatus.OK);
	}

}
