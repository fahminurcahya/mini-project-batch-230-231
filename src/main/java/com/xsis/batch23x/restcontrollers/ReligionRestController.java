package com.xsis.batch23x.restcontrollers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xsis.batch23x.models.ReligionModel;
import com.xsis.batch23x.service.ReligionService;

@RestController
@RequestMapping(path="/api/religion", produces="application/json")
@CrossOrigin(origins="*")
public class ReligionRestController {

	@Autowired
	private ReligionService rs;
	
	@GetMapping("/")
	public ResponseEntity<?>showAll() {
		return new ResponseEntity<>(rs.showAll(), HttpStatus.OK);
	}
	
	@GetMapping("/page")
	public ResponseEntity<?> findAllReligion(@RequestParam Integer pageNo, @RequestParam Integer pageSize,
			@RequestParam String sortBy,@RequestParam String sortType){
		return new ResponseEntity<>(rs.findAllReligion(pageNo, pageSize, sortBy, sortType), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveReligion(@RequestBody ReligionModel rm){
		
		rm.setCreatedBy(1L);
		rm.setCreatedOn(new Date());
		rm.setIsDelete(false);
		return new ResponseEntity<>(rs.save(rm), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id){
		return new ResponseEntity<>(rs.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> editReligion(@RequestBody ReligionModel rm){
		
		rm.setModifiedBy(2L);
		rm.setModifiedOn(new Date());
		return new ResponseEntity<>(rs.save(rm), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
//	public ResponseEntity<?> deleteReligion(@RequestBody ReligionModel rm) {	
	public ResponseEntity<?> deleteReligion(@PathVariable("id") Long id) {
		ReligionModel rm = rs.findById(id).get();
		rm.setIsDelete(true);
		rm.setDeletedBy(3L);
		rm.setDeletedOn(new Date());
		return new ResponseEntity<>(rs.save(rm), HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<Iterable<ReligionModel>> searchReligion(@RequestParam String name, @RequestParam String description){
		Iterable<ReligionModel> list = rs.searchReligion(name, description);
		return new ResponseEntity<Iterable<ReligionModel>>(list, HttpStatus.OK);
	}
}
