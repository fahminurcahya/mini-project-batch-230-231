package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.AddressModel;

public interface AddressService {

	List<AddressModel> getAll();
	Optional<AddressModel> getById(Long id);
	AddressModel save(AddressModel address);
	
}
