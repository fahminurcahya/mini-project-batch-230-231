package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.xsis.batch23x.dto.Biodata2Dto;
import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.models.RoleModel;


public interface BiodataService {

	List<BiodataDto> viewProsesP(Long idEducation,Integer pageNo, Integer pageSize, String sortBy,String sortType);
	List<BiodataDto> findByNamePag(Long idEducation,String fullname, String nickName,Integer pageNo, Integer pageSize, String sortBy,String sortType);
	List<BiodataModel> showBioById(Long idBio);
	List<BiodataModel> showBioProfil(Long idBio);
	Optional<BiodataModel>  findById(Long id);
	BiodataModel save(BiodataModel biodataModel);
	List<BiodataModel> showAllBio();
	List<BiodataModel> viewById(Long id);
	List<BiodataDto> showAllProsesP(Long idPendidikan);
	List<BiodataModel> showAllBiodataForCariPelamar();
	List<Biodata2Dto> showBiodata(Long id);
	List<BiodataModel> findAll();
	
	  List<Biodata2Dto> validasiIdentitas(Long id, String noin, Long idBio);
	  List<Biodata2Dto> validasiEmail(String email, Long id);


}
