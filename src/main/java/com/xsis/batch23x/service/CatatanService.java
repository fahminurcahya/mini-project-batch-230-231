package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.CatatanDto;
import com.xsis.batch23x.models.CatatanModel;

public interface CatatanService {
	
	List<CatatanModel>findAllCatatan();
	
	CatatanModel save(CatatanModel catatan);
	
	void delete(Long id);
	
	Optional<CatatanModel> findById(Long id);
	
	List <CatatanModel> findCatatanByIdBio(Long biodataId);
	
	List<CatatanDto> findCatatanByBio(Long biodataId);
}

