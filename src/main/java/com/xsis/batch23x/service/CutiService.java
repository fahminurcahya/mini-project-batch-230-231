package com.xsis.batch23x.service;

import java.util.Optional;

import com.xsis.batch23x.models.CutiTahunLaluModel;

public interface CutiService {
	
	void delete(Long id);
	
	CutiTahunLaluModel save(CutiTahunLaluModel cutiTahunLaluModel);
	
	Optional<CutiTahunLaluModel>  showCutiByIdEmployee(Long leaveId);


}
