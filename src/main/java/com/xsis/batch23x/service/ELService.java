package com.xsis.batch23x.service;

import java.util.List;

import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeLeaveModel;

public interface ELService {

	List<CutiDto> findPYLQByIdEmployee(Long id, Integer tahun);
	
	List<CutiDto> findByIdEmployee(Long id, Integer tahun);
	
	EmployeeLeaveModel save(EmployeeLeaveModel ELM);
}
