package com.xsis.batch23x.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeLeaveModel;
import com.xsis.batch23x.models.EmployeeModel;
import com.xsis.batch23x.models.UndanganModel;

public interface EmployeeLeaveService {
	
	List<EmployeeLeaveModel>findAllEmpLeave();
	
	List<CutiDto>findByNameKaryawan(String fullname,Date date1, Date date2,
			 String tahun,
			Integer pageNo, Integer pageSize, String sortBy,String sortType);
	
	EmployeeLeaveModel save(EmployeeLeaveModel employeeLeaveModel);
	
	List<CutiDto>findAllCuti(Date date1, Date date2);
	
	Optional<EmployeeLeaveModel>  findById(Long id);
	
	List<EmployeeLeaveModel> viewForLastid();
	
	List<CutiDto>findCutiById(Long idLeave,Date date1, Date date2);

}
