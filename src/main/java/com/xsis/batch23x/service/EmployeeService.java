package com.xsis.batch23x.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.CutiComboDto;
import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeModel;
import com.xsis.batch23x.models.OrganisasiModel;

public interface EmployeeService {
	
	List<EmployeeModel>findAllEmp();
	
	List<CutiComboDto> findKaryawanCombo();
	List<CutiDto> findKaryawanComboByIdTahunSekarang(Long id,Date date1,Date date2);
	List<EmployeeModel> showEmployeById(Long idEmp);
	List<EmployeeModel> showEmployeAllIsEro();
}
