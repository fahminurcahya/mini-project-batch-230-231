package com.xsis.batch23x.service;

import java.util.List;

import com.xsis.batch23x.models.FamilyRelationModel;

public interface FamilyRelationService {
	
	List<FamilyRelationModel>findAllFamilyRelation();

}
