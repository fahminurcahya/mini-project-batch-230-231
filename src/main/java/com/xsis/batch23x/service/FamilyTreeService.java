package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.FamilyTreeTypeModel;

public interface FamilyTreeService {
	
	List<FamilyTreeTypeModel>findAllFamilyTree(Integer pageNo, Integer pageSize, String sortBy, String sortType);
	
	FamilyTreeTypeModel save(FamilyTreeTypeModel familytree);
	
	void delete(Long id);
	
	List<FamilyTreeTypeModel> findFamilyTreeById(Long id);
	
	Optional<FamilyTreeTypeModel>	findById(Long id);
	
	List<FamilyTreeTypeModel> showAll();

	Iterable<FamilyTreeTypeModel> searchFamilyTree(String name, String description);


}
