package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.IdentityTypeModel;


public interface IdentityTypeService {

	Optional<IdentityTypeModel>  findById(Long id);
	List<IdentityTypeModel> findAllIdentity();
}
