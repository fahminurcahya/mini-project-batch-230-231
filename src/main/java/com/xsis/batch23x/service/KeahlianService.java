package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.KeahlianModel;

public interface KeahlianService {
	
	List<KeahlianModel>findAllKeahlian();

	KeahlianModel save(KeahlianModel keahlian);
	
	void delete(Long id);
	
	Optional<KeahlianModel> findById(Long id);

	List <KeahlianModel> findKeahlianByIdBio(Long biodataId);
	
	List<KeahlianModel> findKeahlian();
}
