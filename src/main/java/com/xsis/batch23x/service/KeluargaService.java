package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.KeluargaDto;
import com.xsis.batch23x.models.KeluargaModel;
import com.xsis.batch23x.models.RiwayatPendidikanModel;

public interface KeluargaService {
	
	List<KeluargaModel> findAllKeluarga();
	List<KeluargaModel> findKeluargaByIdBio(Long biodataId);
	List<KeluargaDto> findKeluargaByBio(Long biodataId);
	Optional<KeluargaModel> findById(Long id);
	KeluargaModel save(KeluargaModel keluarga);
	void delete(Long id);

}
