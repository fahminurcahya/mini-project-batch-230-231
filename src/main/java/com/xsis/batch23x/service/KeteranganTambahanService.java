package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.KeteranganTambahanModel;

public interface KeteranganTambahanService {

	KeteranganTambahanModel save(KeteranganTambahanModel keterangan);

	List<KeteranganTambahanModel>  findKeteranganByBioId(Long BioId);
	
	Optional<KeteranganTambahanModel>  findById(Long Id);
	
//	void delete(Long Id);
}
