package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.LeaveNameModel;

public interface LeaveNameService {
	
	List<LeaveNameModel>findAllLeaveName();
	
	LeaveNameModel save(LeaveNameModel leavename);
	
	void delete(Long id);
	
	List<LeaveNameModel> findLeaveNameById(Long id);
	
	Optional<LeaveNameModel>  findById(Long id);
}

