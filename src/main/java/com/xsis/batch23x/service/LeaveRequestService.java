package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.LeaveRequestModel;

public interface LeaveRequestService {

	List<LeaveRequestModel> findAllLeaveRequest(Long createdBy, Integer pageNo, Integer pageSize, String sortBy, String sortType);
	
	LeaveRequestModel save(LeaveRequestModel lrm);
	
	Optional<LeaveRequestModel> findById(Long id);
	
	List<LeaveRequestModel> showAll(Long createdBy);
	
	Iterable<LeaveRequestModel> searchLeaveRequest(Long createdBy);
	
	Iterable<LeaveRequestModel> searchLeaveRequestforKhusus(Long createdBy);
}
