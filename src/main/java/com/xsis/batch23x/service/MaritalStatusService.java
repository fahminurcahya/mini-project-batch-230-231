package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.MaritalStatusModel;

public interface MaritalStatusService {

	Optional<MaritalStatusModel> findById(Long id);
	List<MaritalStatusModel> findAllMarital();
}
