package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.NoteTypeModel;

public interface NoteTypeService {
	
	List<NoteTypeModel>findAllNoteType();
	
	NoteTypeModel save(NoteTypeModel skill_level);
	
	void delete(Long id);
	
	List<NoteTypeModel> findNoteTypeById(Long id);
	
	Optional<NoteTypeModel>  findById(Long id);
}

