package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.OrganisasiModel;

public interface OrganisasiService {
	
	Optional<OrganisasiModel>  findById(Long id);
	
	List<OrganisasiModel> findOrganisasiByIdBio(Long biodataId);
	OrganisasiModel save(OrganisasiModel organisasi);
	void delete(Long id);

}
