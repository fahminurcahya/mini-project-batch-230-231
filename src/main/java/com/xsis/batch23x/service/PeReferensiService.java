package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.PeReferensiModel;


public interface PeReferensiService {
	
	PeReferensiModel save(PeReferensiModel pereferensi);

	List<PeReferensiModel>  findPeReferensiByBioId(Long BioId);

	Optional<PeReferensiModel>  findById(Long Id);
	
	void delete(Long Id);

}
