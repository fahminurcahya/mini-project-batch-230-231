package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.RiwayatPelatihanModel;


public interface PelatihanService {
	
	RiwayatPelatihanModel save(RiwayatPelatihanModel pelatihan);

	List<RiwayatPelatihanModel>  findRiwayatPelatihanByBioId(Long BioId);
	

	Optional<RiwayatPelatihanModel>  findById(Long Id);
	
	void delete(Long Id);
}
