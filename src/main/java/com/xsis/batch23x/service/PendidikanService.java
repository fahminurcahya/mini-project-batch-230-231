package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.PendidikanDto;
import com.xsis.batch23x.models.KeahlianModel;
import com.xsis.batch23x.models.RiwayatPendidikanModel;

public interface PendidikanService {
	
	List<RiwayatPendidikanModel> findAllPendidikan();
	
	List<RiwayatPendidikanModel> findPendidikanByIdBio(Long biodataId);
	
	RiwayatPendidikanModel save(RiwayatPendidikanModel pendidikan);
	
	Optional<RiwayatPendidikanModel> findById(Long id);
	
	List<PendidikanDto> findPendidikanByBio(Long biodataId);
	
	void delete(Long id);

}
