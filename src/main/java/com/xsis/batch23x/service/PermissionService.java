package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.PermissionModel;

public interface PermissionService {
	
	List<PermissionModel> findAllPermission(Integer pageNo, Integer pageSize, String sortBy, String sortType);
	
	PermissionModel save(PermissionModel permission);
	
	Optional<PermissionModel> findById(Long id);
	
	List<PermissionModel> showAll();
}

