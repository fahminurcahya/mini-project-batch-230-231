package com.xsis.batch23x.service;

import java.util.List;

import com.xsis.batch23x.dto.PlacementDto;

public interface PlacementService {
	List<PlacementDto> findClientById(Long id);
}
