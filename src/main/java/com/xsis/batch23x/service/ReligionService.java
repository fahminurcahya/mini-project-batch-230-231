package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.ReligionModel;

public interface ReligionService {

	List<ReligionModel> findAllReligion(Integer pageNo, Integer pageSize, String sortBy, String sortType);
	
	ReligionModel save(ReligionModel rm);
	
	Optional<ReligionModel> findById(Long id);
	
	List<ReligionModel> showAll();
	
	Iterable<ReligionModel> searchReligion(String name, String description);
}
