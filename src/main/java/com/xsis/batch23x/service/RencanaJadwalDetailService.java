package com.xsis.batch23x.service;

import java.util.Optional;

import com.xsis.batch23x.models.RencanaJadwalDetailModel;
import com.xsis.batch23x.models.UndanganDetailModel;

public interface RencanaJadwalDetailService {
	
	void delete(Long id);
	
	RencanaJadwalDetailModel save(RencanaJadwalDetailModel rencanaJadwalDetailModel);
	
	Optional<RencanaJadwalDetailModel> showjadwalDetailById(Long rencanaJadwalId);

}
