package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.RencanaJadwalDto;
import com.xsis.batch23x.models.RencanaJadwalModel;
import com.xsis.batch23x.models.UndanganModel;

public interface RencananJadwalService {
	

	List<RencanaJadwalDto> showDetailJadwal(Long idP);
	
	void delete(Long id);
	
	RencanaJadwalModel save(RencanaJadwalModel rencanaJadwalModel);
	
	Optional<RencanaJadwalModel>  findById(Long id);

}
