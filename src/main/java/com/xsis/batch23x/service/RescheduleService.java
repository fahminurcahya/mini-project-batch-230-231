package com.xsis.batch23x.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.xsis.batch23x.dto.RescheduleDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.UndanganModel;

public interface RescheduleService {
	List<RescheduleDto> showReschedule(String fullname, Integer pageNo, Integer pageSize, String sortBy,String sortType);
	List<RescheduleDto> showAllRsch(String fullname);
	UndanganModel save(UndanganModel undanganModel);
	
	List<UndanganDto> showUndanganById(Long idUD);
	//Long cekUndangan(Long id);
	List<RescheduleDto> cekRoTro(LocalDate tgl, String jam);
	//List<RescheduleDto> cekRoTro(LocalDate tgl);
	
	List<?> cekUndangan(Long id);
	
	List<?> nameRo(Long idUD);
	List<?> nameTro(Long idUD);
	List<?> namesType(Long idUD);
//	Long cekRo(LocalDate tgl, String jam, Long ro);
//	Long cekTro(LocalDate tgl, String jam, Long tro);

//	List<RescheduleDto> cekUndangan(Long id);
}
