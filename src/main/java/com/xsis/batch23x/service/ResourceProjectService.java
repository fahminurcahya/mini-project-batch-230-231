package com.xsis.batch23x.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.ResourceProjectModel;
import com.xsis.batch23x.models.RoleModel;


public interface ResourceProjectService {


	List<ResourceProjectModel> findAllResource(Long idAddr, Integer pageNo, Integer pageSize, String sortBy, String sortType);

	Optional<ResourceProjectModel>  findById(Long id);
	
	Iterable<ResourceProjectModel> searchResource(String name, Date date1,Date date2,Long idAddr);  

	Iterable<ResourceProjectModel> searchResourceByName(String name,Long idAddr);  
	List<ResourceProjectModel> showAll(Long idAddr);
	
	ResourceProjectModel save(ResourceProjectModel roleMod);
	
}