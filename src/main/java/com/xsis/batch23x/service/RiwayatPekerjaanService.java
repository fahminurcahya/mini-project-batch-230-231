package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.RiwayatPekerjaanModel;

public interface RiwayatPekerjaanService {
	
	List<RiwayatPekerjaanModel> findAllPekerjaan(Integer pageNo, Integer pageSize, String sortBy, String sortType);
	List<RiwayatPekerjaanModel> findAllPekerjaan();
	List<RiwayatPekerjaanModel> findPekerjaanByIdBio(Long biodataId);
	RiwayatPekerjaanModel save(RiwayatPekerjaanModel RiwayatPekerjaan);
	void delete(Long id);
	Optional<RiwayatPekerjaanModel> findById(Long id);

}
