package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.RiwayatProyekModel;

public interface RiwayatProyekService {

	RiwayatProyekModel save(RiwayatProyekModel proyek);
	
	List<RiwayatProyekModel> findProyekByIdPekerjaan(Long pekerjaanId);	

	Optional<RiwayatProyekModel>  findById(Long Id);

	
}
