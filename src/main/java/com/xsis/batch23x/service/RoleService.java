package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.RoleModel;

public interface RoleService {
	
	List<RoleModel> findAllRole(Integer pageNo, Integer pageSize, String sortBy, String sortType);
	RoleModel save(RoleModel roleMod);
	Optional<RoleModel>  findById(Long id);
	List<RoleModel> showAll();
	Iterable<RoleModel> searchRole(String code, String name);   
	
}
