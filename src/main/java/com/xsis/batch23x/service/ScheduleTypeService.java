package com.xsis.batch23x.service;

import java.util.List;

import com.xsis.batch23x.models.ScheduleTypeModel;

public interface ScheduleTypeService {
	
	List<ScheduleTypeModel> showStById(Long idSt);
	
	List<ScheduleTypeModel> showAllSt();

}
