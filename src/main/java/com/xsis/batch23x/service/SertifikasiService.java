package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.models.SertifikasiModel;

public interface SertifikasiService {

	List<SertifikasiModel> findAllSertifikasi(Integer pageNo, Integer pageSize, String sortBy, String sortType);
	List<SertifikasiModel>findAllSertifikasi();
	SertifikasiModel save(SertifikasiModel Sertifikasi);
	void delete(Long id);
	Optional<SertifikasiModel>  findById(Long id);
	
	List<SertifikasiModel> findSertifikasiByIdBio(Long biodataId);
}
