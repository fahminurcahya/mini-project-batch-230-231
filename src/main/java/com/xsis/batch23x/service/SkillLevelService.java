package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.SkillLevelModel;

public interface SkillLevelService {
	
	List<SkillLevelModel>findAllSkillLevel();
	
	SkillLevelModel save(SkillLevelModel skill_level);
	
	void delete(Long id);
	
	List<SkillLevelModel> findSkillLevelById(Long id);
	
	Optional<SkillLevelModel>  findById(Long id);
}

