package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.models.SumberLokerModel;

public interface SumberLokerService {
	
	SumberLokerModel save(SumberLokerModel slm);
	Optional<SumberLokerModel> findById(Long id);
	List<SumberLokerModel> findSumberLokerByBio(Long biodataId);
}
