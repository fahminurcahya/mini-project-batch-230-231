package com.xsis.batch23x.service;

import java.util.List;

import com.xsis.batch23x.models.TimePeriodModel;

public interface TimePeriodService {
List<TimePeriodModel> showTimePeriod();
}
