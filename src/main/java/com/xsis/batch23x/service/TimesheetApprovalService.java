package com.xsis.batch23x.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.TimesheetApprovalDto;
import com.xsis.batch23x.models.TimesheetModel;

public interface TimesheetApprovalService {

	TimesheetModel save(TimesheetModel tm);
	Optional<TimesheetModel> findById(Long id);
	List<TimesheetApprovalDto> showTimesheet(Long id, Integer tahun, Integer bulan);
	Optional<TimesheetApprovalDto> findTimesheetById(Long id);
	List<TimesheetApprovalDto> validasi(Long id, Date tanggal);
	Long validasi2(Long id, Date tanggal);
}
