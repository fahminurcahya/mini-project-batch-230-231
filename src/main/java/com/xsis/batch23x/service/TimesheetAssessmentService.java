package com.xsis.batch23x.service;

import com.xsis.batch23x.models.TimesheetAssessment;

public interface TimesheetAssessmentService {

	TimesheetAssessment save(TimesheetAssessment tsa);
}
