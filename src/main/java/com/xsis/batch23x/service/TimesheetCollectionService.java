package com.xsis.batch23x.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.models.TimesheetModel;

public interface TimesheetCollectionService {
	
	TimesheetModel save(TimesheetModel tsMod);
	List<TimesheetDto> showAllForCollection(Long idAddr,String Client,String Pegawai, Integer bulan, Integer tahun);
	Optional<TimesheetDto> findTimesheetById(Long id);
	
	

}
