package com.xsis.batch23x.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.models.TimesheetModel;

public interface TimesheetService {
	TimesheetModel save(TimesheetModel tsMod);
	Optional<TimesheetModel>  findById(Long id);
	List<TimesheetDto> showAll(Long id, Date date1, Date date2);
	List<TimesheetDto> showTimesheet(Long id, Date date1, Date date2, Integer pageNo, Integer pageSize, String sortBy, String sortType);
	Optional<TimesheetDto> findTimesheetById(Long id);
	List<TimesheetDto> validasi(Long id, Date tanggal);
	Long validasi2(Long id, Date tanggal);

}
