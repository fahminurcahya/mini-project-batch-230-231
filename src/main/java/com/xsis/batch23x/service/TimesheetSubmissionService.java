package com.xsis.batch23x.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.dto.TimesheetSubmissionDto;

public interface TimesheetSubmissionService {
	
	List<TimesheetSubmissionDto> showTimesheet(Long id, Integer bulan, Integer tahun);
	int submit(Long idAddr, Integer bulan, Integer tahun);
}
