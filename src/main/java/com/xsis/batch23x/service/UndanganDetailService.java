package com.xsis.batch23x.service;

import java.util.Optional;

import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.models.UndanganModel;

public interface UndanganDetailService {
	
	void delete(Long id);
	
	UndanganDetailModel save(UndanganDetailModel undangandetailmodel);
	
	Optional<UndanganDetailModel>  showUndanganDetailById(Long idUndangan);

	Optional<UndanganDetailModel>  findById(Long id);
}
