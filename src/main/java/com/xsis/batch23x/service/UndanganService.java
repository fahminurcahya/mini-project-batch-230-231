package com.xsis.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.UndanganModel;

public interface UndanganService {
	
	List<UndanganModel> viewForLastid();
	
	List<UndanganDto> showDetailUndangan(Long idP);
	
	List<UndanganDto> CekJadwalRoTro();
	
	List<UndanganDto> showAllUndangan(String fullname);
	
	List<UndanganDto> showUndanganById(Long idUndangan);
	
	List<UndanganDto>showNameUndangan(Long educationLevelId,String fullname, Integer pageNo, Integer pageSize, String sortBy,String sortType);
	
	void delete(Long id);
	
	UndanganModel save(UndanganModel undanganmodel);
	
	Optional<UndanganModel>  findById(Long id);
	
	
	
}
