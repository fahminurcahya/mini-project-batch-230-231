package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.AddressModel;
import com.xsis.batch23x.repositories.AddressRepo;
import com.xsis.batch23x.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService{
	
	@Autowired
	private AddressRepo addressrepo;

	@Override
	public List<AddressModel> getAll() {
		// TODO Auto-generated method stub
		return addressrepo.findAll();
	}

	@Override
	public Optional<AddressModel> getById(Long id) {
		// TODO Auto-generated method stub
		return addressrepo.findById(id);
	}

	@Override
	public AddressModel save(AddressModel address) {
		// TODO Auto-generated method stub
		return addressrepo.save(address);
	}

}
