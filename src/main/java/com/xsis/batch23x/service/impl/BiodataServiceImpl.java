package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.Biodata2Dto;
import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.repositories.BiodataRepo;
import com.xsis.batch23x.service.BiodataService;

@Service
public class BiodataServiceImpl implements BiodataService{

	@Autowired
	private BiodataRepo biodatarepo;
	
	
	

	@Override
	public Optional<BiodataModel> findById(Long id) {
		// TODO Auto-generated method stub
		return biodatarepo.findById(id);
	}



	@Override
	public List<BiodataDto> findByNamePag(Long idPendidikan,String fullname, String nickName,Integer pageNo, Integer pageSize,
			String sortBy,String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<BiodataDto> pageResult = biodatarepo.findByNamePag(pageable, idPendidikan,fullname,nickName);
		
		return pageResult.getContent();
	}



	@Override
	public BiodataModel save(BiodataModel biodataModel) {
		// TODO Auto-generated method stub
		return biodatarepo.save(biodataModel);
	}



	@Override
	public List<BiodataModel> showAllBio() {
		// TODO Auto-generated method stub
		return biodatarepo.showAllBio();
	}



	@Override
	public List<BiodataModel> showBioById(Long idBio) {
		// TODO Auto-generated method stub
		return biodatarepo.showBioById(idBio);
	}



	@Override
	public List<BiodataModel> showBioProfil(Long idBio) {
		// TODO Auto-generated method stub
		return biodatarepo.showBioProfil(idBio);
	}



	@Override
	public List<BiodataDto> viewProsesP(Long idEducation, Integer pageNo, Integer pageSize, String sortBy,
			String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<BiodataDto> pageResult = biodatarepo.viewProsesP(pageable, idEducation);
		
		return pageResult.getContent();
	}
	public List<Biodata2Dto> showBiodata(Long id) {
		// TODO Auto-generated method stub
		return biodatarepo.showBiodata(id);
	}



	@Override

	public List<BiodataModel> viewById(Long id) {
		// TODO Auto-generated method stub
		return biodatarepo.viewById(id);
	}

	public List<BiodataModel> findAll() {
		// TODO Auto-generated method stub
		return biodatarepo.findAll();

	}



	@Override
	public List<BiodataDto> showAllProsesP(Long idPendidikan) {
		// TODO Auto-generated method stub
		return biodatarepo.showAllProsesP(idPendidikan);
	}
	public List<Biodata2Dto> validasiIdentitas(Long id, String noin, Long idBio) {
		// TODO Auto-generated method stub
		return biodatarepo.validasiIdentitas(id, noin, idBio);
	}



	@Override
	public List<BiodataModel> showAllBiodataForCariPelamar() {
		// TODO Auto-generated method stub
		return biodatarepo.showAllBiodataForCariPelamar();
	}
	public List<Biodata2Dto> validasiEmail(String email, Long id) {
		// TODO Auto-generated method stub
		return biodatarepo.validasiEmail(email, id);
	}

	

}
