package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.CatatanDto;
import com.xsis.batch23x.models.CatatanModel;
import com.xsis.batch23x.repositories.CatatanRepo;
import com.xsis.batch23x.service.CatatanService;

@Service
public class CatatanServiceImpl implements CatatanService{
	
	@Autowired
	private CatatanRepo catatanrepo;

	@Override
	public List<CatatanModel> findAllCatatan() {
		// TODO Auto-generated method stub
		return catatanrepo.findAll();
	}

	@Override
	public CatatanModel save(CatatanModel catatan) {
		// TODO Auto-generated method stub
		return catatanrepo.save(catatan);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		catatanrepo.deleteById(id);
	}

	@Override
	public Optional<CatatanModel> findById(Long id) {
		// TODO Auto-generated method stub
		return catatanrepo.findById(id);
	}

	@Override public List<CatatanModel> findCatatanByIdBio(Long biodataId) { 
		//
	  return catatanrepo.findCatatanByIdBio(biodataId); 
	 }
	 

	@Override
	public List<CatatanDto> findCatatanByBio(Long biodataId) {
		// TODO Auto-generated method stub
		return catatanrepo.findCatatanByBio(biodataId);
	}
	
}
