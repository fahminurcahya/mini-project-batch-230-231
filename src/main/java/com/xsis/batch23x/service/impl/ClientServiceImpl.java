package com.xsis.batch23x.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.ClientModel;
import com.xsis.batch23x.repositories.ClientRepo;
import com.xsis.batch23x.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepo cr;
	
	@Override
	public List<ClientModel> showClientbyIdAddr(Long idAddr){
			
			return cr.showClientbyIdAddr(idAddr);
		}
	
}
