package com.xsis.batch23x.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.CutiTahunLaluModel;
import com.xsis.batch23x.repositories.CutiRepo;
import com.xsis.batch23x.service.CutiService;

@Service
public class CutiServiceImpl implements CutiService{
	
	@Autowired
	private CutiRepo a;

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		a.deleteById(id);
		
	}

	@Override
	public CutiTahunLaluModel save(CutiTahunLaluModel cutiTahunLaluModel) {
		// TODO Auto-generated method stub
		return a.save(cutiTahunLaluModel);
	}

	@Override
	public Optional<CutiTahunLaluModel> showCutiByIdEmployee(Long leaveId) {
		// TODO Auto-generated method stub
		return a.showCutiByIdEmployee(leaveId);
	}
	

}
