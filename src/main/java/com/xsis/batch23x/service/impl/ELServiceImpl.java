package com.xsis.batch23x.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeLeaveModel;
import com.xsis.batch23x.repositories.ELRepo;
import com.xsis.batch23x.service.ELService;

@Service
public class ELServiceImpl implements ELService {

	@Autowired
	private ELRepo ELR;
	
	@Override
	public List<CutiDto> findPYLQByIdEmployee(Long id, Integer tahun) {
		return ELR.findPYLQByIdEmployee(id, tahun);
	}
	
	@Override
	public List<CutiDto> findByIdEmployee(Long id, Integer tahun) {
		return ELR.findByIdEmployee(id, tahun);
	}
	
	@Override
	public EmployeeLeaveModel save(EmployeeLeaveModel ELM) {
		return ELR.save(ELM);
	}
}
