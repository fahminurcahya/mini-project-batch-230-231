package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.EducationLevelModel;
import com.xsis.batch23x.repositories.EducationLevelRepo;
import com.xsis.batch23x.service.EducationLevelService;

@Service
public class EducationServiceImpl implements EducationLevelService {
	
	@Autowired
	private EducationLevelRepo  educationLevelRepo;
	
	@Override
	public List<EducationLevelModel> findAllEducationLevel() {
		// TODO Auto-generated method stub
		return educationLevelRepo.findAll();
	}

	
	
//	@Override
//	public EducationLevelModel save(EducationLevelModel educationLevel) {
//		// TODO Auto-generated method stub
//		educationLevel.setCreatedBy(Long.valueOf(1));
//		educationLevel.setCreatedOn(new Date());
//		educationLevel.setIsDelete(false);
//		return educationLevelRepo.save(educationLevel);
//	}
//
//	@Override
//	public void delete(Long id) {
//		// TODO Auto-generated method stub
//		educationLevelRepo.deleteById(id);
//	}
//
//	@Override
//	public List<EducationLevelModel> findEducationLevelById(Long id) {
//		// TODO Auto-generated method stub
//		return educationLevelRepo.findEducationLevelById(id);
//	}
//
//	@Override
//	public Optional<EducationLevelModel> findById(Long id) {
//		// TODO Auto-generated method stub
//		return educationLevelRepo.findById(id);
//	}
}
