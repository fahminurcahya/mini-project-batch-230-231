package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeLeaveModel;
import com.xsis.batch23x.models.EmployeeModel;
import com.xsis.batch23x.repositories.EmployeeLeaveRepo;
import com.xsis.batch23x.service.EmployeeLeaveService;

@Service
public class EmployeeLeaveImpl implements EmployeeLeaveService {
	
	@Autowired
	private EmployeeLeaveRepo employeeleaveRepo;

	@Override
	public List<EmployeeLeaveModel> findAllEmpLeave() {
		// TODO Auto-generated method stub
		return employeeleaveRepo.findAll();
	}

	@Override
	public List<CutiDto> findByNameKaryawan(String fullname,Date date1,Date date2,  String tahun,Integer pageNo, Integer pageSize, String sortBy,
			String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		Page<CutiDto> pageResult = employeeleaveRepo.findByNameKaryawan(pageable, fullname,date1,date2,tahun);
		
		return pageResult.getContent();
	}
	
	@Override
	public EmployeeLeaveModel save(EmployeeLeaveModel employeeLeaveModel) {
		// TODO Auto-generated method stub
		return  employeeleaveRepo.save(employeeLeaveModel);
	}

	@Override
	public List<CutiDto> findAllCuti(Date date1, Date date2) {
		// TODO Auto-generated method stub
		return employeeleaveRepo.findAllCuti(date1, date2);
	}

	@Override
	public Optional<EmployeeLeaveModel> findById(Long id) {
		// TODO Auto-generated method stub
		return employeeleaveRepo.findById(id);
	}

	@Override
	public List<EmployeeLeaveModel> viewForLastid() {
		// TODO Auto-generated method stub
		return employeeleaveRepo.viewForLastid();
	}

	@Override
	public List<CutiDto> findCutiById(Long idLeave, Date date1, Date date2) {
		// TODO Auto-generated method stub
		return employeeleaveRepo.findCutiById(idLeave, date1, date2);
	}

	

}
