package com.xsis.batch23x.service.impl;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.CutiComboDto;
import com.xsis.batch23x.dto.CutiDto;
import com.xsis.batch23x.models.EmployeeModel;
import com.xsis.batch23x.repositories.CatatanRepo;
import com.xsis.batch23x.repositories.EmployeeRepo;
import com.xsis.batch23x.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepo employeeRepo;

	@Override
	public List<EmployeeModel> findAllEmp() {
		// TODO Auto-generated method stub
		return employeeRepo.findAll();
	}
	
	@Override
	public List<CutiComboDto> findKaryawanCombo() {
		// TODO Auto-generated method stub
		return employeeRepo.findKaryawanCombo();
	}

	@Override
	public List<CutiDto> findKaryawanComboByIdTahunSekarang(Long id, Date date1, Date date2) {
		// TODO Auto-generated method stub
		return employeeRepo.findKaryawanComboByIdTahunSekarang(id,date1,date2);
	}

	@Override
	public List<EmployeeModel> showEmployeById(Long idEmp) {
		// TODO Auto-generated method stub
		return employeeRepo.showEmployeById(idEmp);
	}

	@Override
	public List<EmployeeModel> showEmployeAllIsEro() {
		// TODO Auto-generated method stub
		return employeeRepo.showEmployeAllIsEro();
	}

	

}
