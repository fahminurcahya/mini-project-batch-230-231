package com.xsis.batch23x.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.FamilyRelationModel;
import com.xsis.batch23x.repositories.FamilyRelationRepo;
import com.xsis.batch23x.service.FamilyRelationService;

@Service
public class FamilyRelationServiceImpl implements FamilyRelationService {
	
	@Autowired
	private FamilyRelationRepo familyRelationRepo;

	@Override
	public List<FamilyRelationModel> findAllFamilyRelation() {
		// TODO Auto-generated method stub
		return familyRelationRepo.findAll();
	}

	

}
