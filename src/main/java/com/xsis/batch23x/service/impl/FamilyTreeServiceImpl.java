package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.FamilyTreeTypeModel;
import com.xsis.batch23x.repositories.FamilyTreeRepo;
import com.xsis.batch23x.service.FamilyTreeService;

@Service
public class FamilyTreeServiceImpl implements FamilyTreeService {
	
	@Autowired
	private FamilyTreeRepo familyTreeRepo;
	
	@Override
	public List<FamilyTreeTypeModel> findAllFamilyTree(Integer pageNo, Integer pageSize, String sortBy, String sortType){
		Pageable pageable = null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
		} else {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending());
		}
		Page<FamilyTreeTypeModel> pageResult = familyTreeRepo.findAllFamilyTree(pageable);
		return pageResult.getContent();
	}
	
	@Override
	public FamilyTreeTypeModel save(FamilyTreeTypeModel familytree) {
		return familyTreeRepo.save(familytree);
	}

	@Override
	public Optional<FamilyTreeTypeModel> findById(Long id) {
		return familyTreeRepo.findById(id);
	}

	@Override
	public List<FamilyTreeTypeModel> showAll() {
		return familyTreeRepo.findAll();
	}

	@Override
	public Iterable<FamilyTreeTypeModel> searchFamilyTree(String name, String description) {
		return familyTreeRepo.findByNameOrDesc(name, description);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<FamilyTreeTypeModel> findFamilyTreeById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
}
