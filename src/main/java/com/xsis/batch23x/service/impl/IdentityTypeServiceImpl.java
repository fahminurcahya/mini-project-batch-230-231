package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.IdentityTypeModel;
import com.xsis.batch23x.repositories.IdentityTypeRepo;
import com.xsis.batch23x.service.IdentityTypeService;

@Service
public class IdentityTypeServiceImpl implements IdentityTypeService{

	@Autowired
	IdentityTypeRepo identityrepo;
	
	@Override
	public Optional<IdentityTypeModel> findById(Long id) {
		// TODO Auto-generated method stub
		return identityrepo.findById(id);
	}

	@Override
	public List<IdentityTypeModel> findAllIdentity() {
		// TODO Auto-generated method stub
		return identityrepo.findAll();
	}

}
