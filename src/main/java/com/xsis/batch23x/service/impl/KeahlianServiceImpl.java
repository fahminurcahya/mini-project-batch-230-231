package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.KeahlianModel;
import com.xsis.batch23x.repositories.KeahlianRepo;
import com.xsis.batch23x.service.KeahlianService;

@Service
public class KeahlianServiceImpl implements KeahlianService{
	
	@Autowired
	private KeahlianRepo keahlianrepo;
	
	@Override
	public List<KeahlianModel> findAllKeahlian() {
		// TODO Auto-generated method stub
		return keahlianrepo.findAll();
	}

	@Override
	public KeahlianModel save(KeahlianModel keahlian) {
		return keahlianrepo.save(keahlian);
	}

	@Override
	public void delete(Long id) {
		keahlianrepo.deleteById(id);
	}

	@Override
	public Optional<KeahlianModel> findById(Long id) {
		// TODO Auto-generated method stub
		return keahlianrepo.findById(id);
	}
	
	@Override
	public List<KeahlianModel> findKeahlianByIdBio(Long biodataId) {
		// TODO Auto-generated method stub
		return keahlianrepo.findKeahlianByIdBio(biodataId);
	}

	@Override
	public List<KeahlianModel> findKeahlian() {
		// TODO Auto-generated method stub
		return keahlianrepo.findKeahlian();
	}
}
