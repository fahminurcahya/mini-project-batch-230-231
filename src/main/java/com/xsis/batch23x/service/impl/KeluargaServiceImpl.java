package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.KeluargaDto;
import com.xsis.batch23x.models.KeluargaModel;
import com.xsis.batch23x.repositories.KeluargaRepo;
import com.xsis.batch23x.service.KeluargaService;

@Service
public class KeluargaServiceImpl implements KeluargaService {
	
	@Autowired
	private KeluargaRepo keluargaRepo;

	@Override
	public List<KeluargaModel> findKeluargaByIdBio(Long biodataId) {
		// TODO Auto-generated method stub
		return keluargaRepo.findKeluargaByIdBio(biodataId);
	}

	@Override
	public List<KeluargaDto> findKeluargaByBio(Long biodataId) {
		// TODO Auto-generated method stub
		return keluargaRepo.findKeluargaByBio(biodataId);
	}

	@Override
	public Optional<KeluargaModel> findById(Long id) {
		// TODO Auto-generated method stub
		return keluargaRepo.findById(id);
	}

	@Override
	public KeluargaModel save(KeluargaModel keluarga) {
		// TODO Auto-generated method stub
		return keluargaRepo.save(keluarga);
	}

	@Override
	public List<KeluargaModel> findAllKeluarga() {
		// TODO Auto-generated method stub
		return keluargaRepo.findAll();
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		keluargaRepo.deleteById(id);
	}

}
