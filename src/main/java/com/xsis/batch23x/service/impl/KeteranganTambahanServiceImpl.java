package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.KeteranganTambahanModel;
import com.xsis.batch23x.repositories.KeteranganTambahanRepo;
import com.xsis.batch23x.service.KeteranganTambahanService;

@Service
public class KeteranganTambahanServiceImpl implements KeteranganTambahanService{


@Autowired
private KeteranganTambahanRepo ktr;

@Override
public List<KeteranganTambahanModel> findKeteranganByBioId(Long bioId) {
	// TODO Auto-generated method stub                         
	return ktr.findKeteranganByBioId(bioId);
}


@Override 
public KeteranganTambahanModel save (KeteranganTambahanModel keterangan) {
	return ktr.save(keterangan);
}

	/*
	 * @Override public void delete(Long Id) { ktr.deleteById(Id); }
	 */

@Override
public Optional<KeteranganTambahanModel> findById(Long Id) {
	// TODO Auto-generated method stub
	return ktr.findById(Id);
}

}
