package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.xsis.batch23x.models.LeaveNameModel;
import com.xsis.batch23x.repositories.LeaveNameRepo;
import com.xsis.batch23x.service.LeaveNameService;

@Service
public class LeaveNameServiceImpl implements LeaveNameService{
	
	@Autowired
	private LeaveNameRepo leavenamerepo;
	
	@Override
	public List<LeaveNameModel> findAllLeaveName() {
		// TODO Auto-generated method stub
		return leavenamerepo.findAll();
	}

	@Override
	public LeaveNameModel save(LeaveNameModel leavename) {
		// TODO Auto-generated method stub
		leavename.setCreatedBy(Long.valueOf(1));
		leavename.setCreatedOn(new Date());
		leavename.setIsDelete(false);
		return leavenamerepo.save(leavename);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		leavenamerepo.deleteById(id);
	}

	@Override
	public List<LeaveNameModel> findLeaveNameById(Long id) {
		// TODO Auto-generated method stub
		return leavenamerepo.findLeaveNameById(id);
	}

	@Override
	public Optional<LeaveNameModel> findById(Long id) {
		// TODO Auto-generated method stub
		return leavenamerepo.findById(id);
	}


	
}
