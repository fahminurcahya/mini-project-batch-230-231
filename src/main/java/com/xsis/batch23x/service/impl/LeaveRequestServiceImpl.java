package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.LeaveRequestModel;
import com.xsis.batch23x.repositories.LeaveRequestRepo;
import com.xsis.batch23x.service.LeaveRequestService;

@Service
public class LeaveRequestServiceImpl implements LeaveRequestService {

	@Autowired
	private LeaveRequestRepo lrr;
	
	@Override
	public List<LeaveRequestModel> findAllLeaveRequest(Long createdBy, Integer pageNo, Integer pageSize, String sortBy, String sortType){
		Pageable pageable = null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
		} else {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending());
		}
		Page<LeaveRequestModel> pageResult = lrr.findAllLeaveRequest(pageable, createdBy);
		return pageResult.getContent();
	}
	
	@Override
	public LeaveRequestModel save(LeaveRequestModel rm) {
		return lrr.save(rm);
	}

	@Override
	public Optional<LeaveRequestModel> findById(Long id) {
		return lrr.findById(id);
	}

	@Override
	public List<LeaveRequestModel> showAll(Long createdBy) {
		return lrr.showAll(createdBy);
	}

	@Override
	public Iterable<LeaveRequestModel> searchLeaveRequest(Long createdBy) {
		return lrr.findBy(createdBy);
	}
	
	@Override
	public Iterable<LeaveRequestModel> searchLeaveRequestforKhusus(Long createdBy) {
		return lrr.findByKhusus(createdBy);
	}
}
