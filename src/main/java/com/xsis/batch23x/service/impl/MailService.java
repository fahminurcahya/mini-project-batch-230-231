package com.xsis.batch23x.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.RescheduleDto;
import com.xsis.batch23x.dto.TimesheetSubmissionDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.BiodataModel;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.repositories.BiodataRepo;
import com.xsis.batch23x.repositories.RescheduleRepo;
import com.xsis.batch23x.repositories.RoleRepo;

@Service
public class MailService {

	@Autowired
	private BiodataRepo biodataRepo;
	
	
	private JavaMailSender javaMailSender;


	@Autowired
	public MailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendEmail(BiodataModel biodataModel) throws MailException {

		SimpleMailMessage mail = new SimpleMailMessage();
		
		mail.setTo(biodataModel.getEmail());
		mail.setSubject("NEW TOKEN");
		mail.setText("Token : " + biodataModel.getToken() +"\n" +
		"Expired Token : " + biodataModel.getExpiredToken());

		
		javaMailSender.send(mail);
	
	}
	
	public void sendEmailSubmittedCc(String email, String tglAwal, String tglAkhir, String cc) throws MailException {

		SimpleMailMessage mail = new SimpleMailMessage();
		
		mail.setTo(email);
		mail.setCc(cc);
		mail.setSubject("Submitted");
		mail.setText("Status Timesheet pada tanggal : " +tglAwal+ " - "+tglAkhir+" telah berubah menjadi 'Submitted'");
		

		
		javaMailSender.send(mail);
	
	}
	
	public void sendEmailSubmitted(String email, String tglAwal, String tglAkhir) throws MailException {

		SimpleMailMessage mail = new SimpleMailMessage();
		
		mail.setTo(email);
		mail.setSubject("Submitted");
		mail.setText("Status Timesheet pada tanggal : " +tglAwal+ " - "+tglAkhir+" telah berubah menjadi 'Submitted'");
		
		javaMailSender.send(mail);
	
	}
	
	public void sendEmailReschedule(UndanganDto undDto, String ro, String tro, String type) throws MailException {

		SimpleMailMessage mail = new SimpleMailMessage();
		String tanggal[] = undDto.getInvitationDate().toString().split("-");
		
		String tahun = tanggal[0];
		String hari = tanggal[2]; 
	    String bulan="";
	    switch(tanggal[1]) {
	    case "01" : bulan = "January"; break;
	    case "02" : bulan = "February"; break;
	    case "03" : bulan = "March"; break;
	    case "04" : bulan = "April"; break;
	    case "05" : bulan = "May"; break;
	    case "06" : bulan = "June"; break;
	    case "07" : bulan = "July"; break;
	    case "08" : bulan = "August"; break;
	    case "09" : bulan = "September"; break;
	    case "10" : bulan = "October"; break;
	    case "11" : bulan = "November"; break;
	    case "12" : bulan = "December"; break;
	    }
	    
	    String tgl =  hari+" "+bulan+" "+tahun;

		
		mail.setTo(undDto.getEmail());
		mail.setSubject("Reschedule");
		mail.setText("Hallo "+undDto.getFullname()+", "
				+ "Terjadi perubahan jadwal, kami mengundang kamu pada : \n"
				+ "Tanggal : "+tgl+ "\n"
				+ "Jam : " + undDto.getTime() + "\n"
				+ "Jenis Tes : " + type + "\n"
				+ "RO : " + ro + "\n"
				+ "TRO : " + tro + "\n"
				+ "Lokasi : " + undDto.getLocation() + "\n"
				+ "Notes : " + undDto.getNotes() + "\n"
				+ "Kami berharap agar datang tepat waktu");
		javaMailSender.send(mail);
	
	}



}
