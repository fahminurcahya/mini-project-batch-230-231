package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.MaritalStatusModel;
import com.xsis.batch23x.repositories.MaritalStatusRepo;
import com.xsis.batch23x.service.MaritalStatusService;

@Service
public class MaritalStatusServiceImpl implements MaritalStatusService  {

	@Autowired
	MaritalStatusRepo maritalrepo;

	@Override
	public Optional<MaritalStatusModel> findById(Long id) {
		// TODO Auto-generated method stub
		return maritalrepo.findById(id);
	}

	@Override
	public List<MaritalStatusModel> findAllMarital() {
		// TODO Auto-generated method stub
		return maritalrepo.findAll();
	}
}
