package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.NoteTypeModel;
import com.xsis.batch23x.repositories.NoteTypeRepo;
import com.xsis.batch23x.service.NoteTypeService;

@Service
public class NoteTypeServiceImpl implements NoteTypeService{
	
	@Autowired
	private NoteTypeRepo notetyperepo;

	@Override
	public List<NoteTypeModel> findAllNoteType() {
		// TODO Auto-generated method stub
		return notetyperepo.findAll();
	}

	@Override
	public NoteTypeModel save(NoteTypeModel notetype) {
		// TODO Auto-generated method stub
		notetype.setCreatedBy(Long.valueOf(1));
		notetype.setCreatedOn(new Date());
		notetype.setIsDelete(false);
		return notetyperepo.save(notetype);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		notetyperepo.deleteById(id);
	}

	@Override
	public List<NoteTypeModel> findNoteTypeById(Long id) {
		// TODO Auto-generated method stub
		return notetyperepo.findNoteTypeById(id);
	}

	@Override
	public Optional<NoteTypeModel> findById(Long id) {
		// TODO Auto-generated method stub
		return notetyperepo.findById(id);
	}
	
}
