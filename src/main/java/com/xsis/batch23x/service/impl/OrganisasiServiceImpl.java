package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.repositories.OrganisasiRepo;
import com.xsis.batch23x.service.OrganisasiService;

@Service
public class OrganisasiServiceImpl implements OrganisasiService {
	
	@Autowired
	private OrganisasiRepo organisasirepo;

	@Override
	public Optional<OrganisasiModel> findById(Long id) {
		// TODO Auto-generated method stub
		return organisasirepo.findById(id);
	}

	@Override
	public List<OrganisasiModel> findOrganisasiByIdBio(Long biodataId) {
		// TODO Auto-generated method stub
		return organisasirepo.findOrganisasiByIdBio(biodataId);
	}

	@Override
	public OrganisasiModel save(OrganisasiModel organisasi) {
		// TODO Auto-generated method stub
		return organisasirepo.save(organisasi);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		organisasirepo.deleteById(id);
		
	}

}
