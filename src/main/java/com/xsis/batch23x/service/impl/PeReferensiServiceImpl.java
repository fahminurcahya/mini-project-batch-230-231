package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.PeReferensiModel;
import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.repositories.PeReferensiRepo;
import com.xsis.batch23x.service.PeReferensiService;

@Service
public class PeReferensiServiceImpl implements PeReferensiService{

@Autowired
private PeReferensiRepo prr;


@Override
public List<PeReferensiModel> findPeReferensiByBioId(Long bioId) {
	// TODO Auto-generated method stub                         
	return prr.findPeReferensiByBioId(bioId);
}


@Override 
public PeReferensiModel save (PeReferensiModel pereferensi) {
	return prr.save(pereferensi);
}
@Override
public void delete(Long Id) {
	prr.deleteById(Id);
}

@Override
public Optional<PeReferensiModel> findById(Long Id) {
	// TODO Auto-generated method stub
	return prr.findById(Id);
}




}
