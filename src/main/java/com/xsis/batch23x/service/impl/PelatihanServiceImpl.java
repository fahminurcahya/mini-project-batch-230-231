package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.RiwayatPelatihanModel;
import com.xsis.batch23x.repositories.PelatihanRepo;
import com.xsis.batch23x.service.PelatihanService;


@Service
public class PelatihanServiceImpl implements PelatihanService {
	
@Autowired
private PelatihanRepo pr;
	
	@Override
	public List<RiwayatPelatihanModel> findRiwayatPelatihanByBioId(Long bioId) {
		// TODO Auto-generated method stub
		return pr.findRiwayatPelatihanByBioId(bioId);
	}
	

@Override 
public RiwayatPelatihanModel save (RiwayatPelatihanModel variant) {
	return pr.save(variant);
}
@Override
public void delete(Long Id) {
	pr.deleteById(Id);
}

@Override
public Optional<RiwayatPelatihanModel> findById(Long Id) {
	// TODO Auto-generated method stub
	return pr.findById(Id);
}


}
