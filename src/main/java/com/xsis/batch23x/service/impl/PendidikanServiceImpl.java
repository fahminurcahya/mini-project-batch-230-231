package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.PendidikanDto;
import com.xsis.batch23x.models.RiwayatPendidikanModel;
import com.xsis.batch23x.repositories.PendidikanRepo;
import com.xsis.batch23x.service.PendidikanService;

@Service
public class PendidikanServiceImpl implements PendidikanService {
	
	@Autowired
	private PendidikanRepo pendidikanRepo;

	@Override
	public List<RiwayatPendidikanModel> findAllPendidikan() {
		// TODO Auto-generated method stub
		return pendidikanRepo.findAll();
	}

	@Override
	public RiwayatPendidikanModel save(RiwayatPendidikanModel pendidikan) {
		// TODO Auto-generated method stub
		
		return pendidikanRepo.save(pendidikan);
	}
	
	@Override
	public Optional<RiwayatPendidikanModel> findById(Long id) {
		// TODO Auto-generated method stub
		return pendidikanRepo.findById(id);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		pendidikanRepo.deleteById(id);
	}

	@Override
	public List<RiwayatPendidikanModel> findPendidikanByIdBio(Long biodataId) {
		// TODO Auto-generated method stub
		return pendidikanRepo.findPendidikanByIdBio(biodataId);
	}

	@Override
	public List<PendidikanDto> findPendidikanByBio(Long biodataId) {
		// TODO Auto-generated method stub
		return pendidikanRepo.findPendidikanByBio(biodataId);
	}


}
