package com.xsis.batch23x.service.impl;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.PermissionModel;
import com.xsis.batch23x.repositories.PermissionRepo;
import com.xsis.batch23x.service.PermissionService;

@Service
public class PermissionServiceImpl implements PermissionService{
	
	@Autowired
	private PermissionRepo permissionrepo;
	
	@Override
	public List<PermissionModel> findAllPermission(Integer pageNo, Integer pageSize, String sortBy, String sortType){
		Pageable pageable = null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
		} else {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending());
		}
		Page<PermissionModel> pageResult = permissionrepo.findAllPermission(pageable);
		return pageResult.getContent();
	}
	
	@Override
	public PermissionModel save(PermissionModel permission) {
		return permissionrepo.save(permission);
	}

	@Override
	public Optional<PermissionModel> findById(Long id) {
		return permissionrepo.findById(id);
	}

	@Override
	public List<PermissionModel> showAll() {
		return permissionrepo.showAll();
	}
}
