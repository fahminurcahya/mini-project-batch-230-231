package com.xsis.batch23x.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.PlacementDto;
import com.xsis.batch23x.repositories.PlacementRepo;
import com.xsis.batch23x.service.PlacementService;

@Service
public class PlacementServiceImpl implements PlacementService {

	@Autowired
	private PlacementRepo placRep;
	
	@Override
	public List<PlacementDto> findClientById(Long id) {
		// TODO Auto-generated method stub
		return placRep.findClientById(id);
	}

}
