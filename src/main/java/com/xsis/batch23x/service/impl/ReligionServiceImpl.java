package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.ReligionModel;
import com.xsis.batch23x.repositories.ReligionRepo;
import com.xsis.batch23x.service.ReligionService;

@Service
public class ReligionServiceImpl implements ReligionService {

	@Autowired
	private ReligionRepo rr;
	
	@Override
	public List<ReligionModel> findAllReligion(Integer pageNo, Integer pageSize, String sortBy, String sortType){
		Pageable pageable = null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
		} else {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending());
		}
		Page<ReligionModel> pageResult = rr.findAllReligion(pageable);
		return pageResult.getContent();
	}
	
	@Override
	public ReligionModel save(ReligionModel rm) {
		return rr.save(rm);
	}

	@Override
	public Optional<ReligionModel> findById(Long id) {
		return rr.findById(id);
	}

	@Override
	public List<ReligionModel> showAll() {
		return rr.findAll();
	}

	@Override
	public Iterable<ReligionModel> searchReligion(String name, String description) {
		return rr.findByNameOrDesc(name, description);
	}

}
