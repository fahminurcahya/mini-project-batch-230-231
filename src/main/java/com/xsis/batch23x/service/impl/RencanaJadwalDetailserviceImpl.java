package com.xsis.batch23x.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.RencanaJadwalDetailModel;
import com.xsis.batch23x.repositories.RencanaJadwalDetailRepo;
import com.xsis.batch23x.repositories.UndanganDetailRepo;
import com.xsis.batch23x.service.RencanaJadwalDetailService;

@Service
public class RencanaJadwalDetailserviceImpl implements RencanaJadwalDetailService{

	@Autowired
	private RencanaJadwalDetailRepo rencanaJadwalDetailRepo;
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		rencanaJadwalDetailRepo.deleteById(id);
	}
	
	@Override
	public RencanaJadwalDetailModel save(RencanaJadwalDetailModel rencanaJadwalDetailModel) {
		// TODO Auto-generated method stub
		return rencanaJadwalDetailRepo.save(rencanaJadwalDetailModel);
	}

	@Override
	public Optional<RencanaJadwalDetailModel> showjadwalDetailById(Long rencanaJadwalId) {
		// TODO Auto-generated method stub
		return rencanaJadwalDetailRepo.showjadwalDetailById(rencanaJadwalId);
	}
	
	

}
