package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.RencanaJadwalDto;
import com.xsis.batch23x.models.RencanaJadwalModel;
import com.xsis.batch23x.repositories.RencanaJadwalRepo;
import com.xsis.batch23x.repositories.UndanganRepo;
import com.xsis.batch23x.service.RencananJadwalService;

@Service
public class RencanaJadwalServiceImpl implements RencananJadwalService{
	
	@Autowired
	private RencanaJadwalRepo rencanaJadwalRepo;

	@Override
	public List<RencanaJadwalDto> showDetailJadwal(Long idP) {
		// TODO Auto-generated method stub
		return rencanaJadwalRepo.showDetailJadwal(idP);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		rencanaJadwalRepo.deleteById(id);
	}

	@Override
	public RencanaJadwalModel save(RencanaJadwalModel rencanaJadwalModel) {
		// TODO Auto-generated method stub
		return rencanaJadwalRepo.save(rencanaJadwalModel);
	}

	@Override
	public Optional<RencanaJadwalModel> findById(Long id) {
		// TODO Auto-generated method stub
		return rencanaJadwalRepo.findById(id);
	}

}
