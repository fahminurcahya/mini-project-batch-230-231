package com.xsis.batch23x.service.impl;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.RescheduleDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.repositories.RescheduleRepo;
import com.xsis.batch23x.service.RescheduleService;

@Service
public class RescheduleServiceImpl implements RescheduleService {
	
	@Autowired
	private RescheduleRepo resRepo;

	@Override
	public List<RescheduleDto> showReschedule(String fullname, Integer pageNo, Integer pageSize, String sortBy,
			String sortType) {
		
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<RescheduleDto> pageResult = resRepo.showReschedule(pageable, fullname);
		
		return pageResult.getContent();
	}

	@Override
	public UndanganModel save(UndanganModel undanganModel) {
		// TODO Auto-generated method stub
		return resRepo.save(undanganModel);
	}

	@Override
	public List<UndanganDto> showUndanganById(Long idUD) {
		// TODO Auto-generated method stub
		return resRepo.showUndanganById(idUD);
	}

	@Override
	public 	List<RescheduleDto> cekRoTro(LocalDate tgl, String jam) {
		// TODO Auto-generated method stub
		return resRepo.cekRoTro(tgl, jam);
	}

	@Override
	public List<?> cekUndangan(Long id) {
		// TODO Auto-generated method stub
		return resRepo.cekUndangan(id);
	}

	@Override
	public List<?> nameRo(Long idUD) {
		// TODO Auto-generated method stub
		return resRepo.nameRo(idUD);
	}
	
	@Override
	public List<?> nameTro(Long idUD) {
		// TODO Auto-generated method stub
		return resRepo.nameTro(idUD);
	}

	@Override
	public List<?> namesType(Long idUD) {
		// TODO Auto-generated method stub
		return resRepo.namesType(idUD);
	}

	@Override
	public List<RescheduleDto> showAllRsch(String fullname) {
		// TODO Auto-generated method stub
		return resRepo.showAllRsch(fullname);
	}

//	@Override
//	public Long cekRo(LocalDate tgl, String jam, Long ro) {
//		// TODO Auto-generated method stub
//		return resRepo.cekRo(tgl, jam, ro);
//	}
//
//	@Override
//	public Long cekTro(LocalDate tgl, String jam, Long tro) {
//		// TODO Auto-generated method stub
//		return resRepo.cekTro(tgl, jam, tro);
//	}



	


}
