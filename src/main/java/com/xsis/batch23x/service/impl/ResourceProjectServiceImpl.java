package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.ResourceProjectModel;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.repositories.ResourceProjectRepo;
import com.xsis.batch23x.service.ResourceProjectService;

@Service
public class ResourceProjectServiceImpl implements ResourceProjectService{

	@Autowired
	private ResourceProjectRepo resRepo;
	
	@Override
	public ResourceProjectModel save(ResourceProjectModel rpm) {
		return resRepo.save(rpm);
	}
	

	@Override
	public Optional<ResourceProjectModel> findById(Long id) {
		// TODO Auto-generated method stub
		return resRepo.findById(id);
	}
	@Override
	public List<ResourceProjectModel> showAll(Long idAddr) {
		// TODO Auto-generated method stub
		return resRepo.showAll(idAddr);
	}
	
	public List<ResourceProjectModel> findAllResource(Long idAddr,Integer pageNo, Integer pageSize, String sortBy, String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<ResourceProjectModel> pageResult = resRepo.findAllResource(idAddr, pageable);
		return pageResult.getContent();
	}
	
	@Override
	public Iterable<ResourceProjectModel> searchResource(String name, Date date1,Date date2,Long idAddr) {
		// TODO Auto-generated method stub
		return resRepo.findByNameOrDate(name, date1 ,date2,idAddr);
	}
	
	@Override
	public Iterable<ResourceProjectModel> searchResourceByName(String name,Long idAddr) {
		// TODO Auto-generated method stub
		return resRepo.findByNameOnly(name,idAddr);
	}

}//class
