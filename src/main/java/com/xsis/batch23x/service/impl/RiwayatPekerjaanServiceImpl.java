package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.RiwayatPekerjaanModel;
import com.xsis.batch23x.repositories.PendidikanRepo;
import com.xsis.batch23x.repositories.RiwayatPekerjaanRepo;
import com.xsis.batch23x.service.RiwayatPekerjaanService;

@Service
public class RiwayatPekerjaanServiceImpl implements RiwayatPekerjaanService {
	
	@Autowired
	private RiwayatPekerjaanRepo riwayatPekerjaanRepo;

	@Override
	public List<RiwayatPekerjaanModel> findAllPekerjaan() {
		// TODO Auto-generated method stub
		return riwayatPekerjaanRepo.findAll();
	}

	@Override
	public List<RiwayatPekerjaanModel> findPekerjaanByIdBio(Long biodataId) {
		// TODO Auto-generated method stub
		return riwayatPekerjaanRepo.findPekerjaanByIdBio(biodataId);
	}

//	@Override
//	public List<RiwayatPekerjaanModel> findAllPekerjaan(Integer pageNo, Integer pageSize, String sortBy,String sortType) {
//			Pageable pageable=null;
//	if (sortType.equalsIgnoreCase("desc")) {
//		pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
//	}else {
//		pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
//	}
//	
//	Page<RiwayatPekerjaanModel> pageResult = riwayatPekerjaanRepo.findAllPekerjaan(pageable);
//	return pageResult.getContent();
//	}

	@Override
	public RiwayatPekerjaanModel save(RiwayatPekerjaanModel RiwayatPekerjaan) {
		// TODO Auto-generated method stub
		return riwayatPekerjaanRepo.save(RiwayatPekerjaan);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		riwayatPekerjaanRepo.deleteById(id);
	}

	@Override
	public Optional<RiwayatPekerjaanModel> findById(Long id) {
		// TODO Auto-generated method stub
		return riwayatPekerjaanRepo.findById(id);
	}

	@Override
	public List<RiwayatPekerjaanModel> findAllPekerjaan(Integer pageNo, Integer pageSize, String sortBy,
			String sortType) {
		// TODO Auto-generated method stub
		return null;
	}

}
