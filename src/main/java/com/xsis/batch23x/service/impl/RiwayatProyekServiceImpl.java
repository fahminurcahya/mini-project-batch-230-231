package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.RiwayatProyekModel;
import com.xsis.batch23x.repositories.RiwayatProyekRepo;
import com.xsis.batch23x.service.RiwayatProyekService;


@Service
public class RiwayatProyekServiceImpl implements RiwayatProyekService{
	
	@Override 
	public RiwayatProyekModel save (RiwayatProyekModel proyek) {
		return rpr.save(proyek);
	}
	
	@Autowired
	private RiwayatProyekRepo rpr;
	

	@Override
	public List<RiwayatProyekModel> findProyekByIdPekerjaan(Long idPekerjaan) {
		// TODO Auto-generated method stub
		return rpr.findProyekByIdPekerjaan(idPekerjaan);
	}

	
	
	@Override
	public Optional<RiwayatProyekModel> findById(Long Id) {

		return rpr.findById(Id);
	}


}
