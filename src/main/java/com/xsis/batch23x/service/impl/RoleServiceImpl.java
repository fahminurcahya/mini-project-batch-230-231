package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.repositories.RoleRepo;
import com.xsis.batch23x.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleRepo roleRepo;



	@Override
	public RoleModel save(RoleModel roleMod) {
		
		// TODO Auto-generated method stub
		return roleRepo.save(roleMod);
	}

	@Override
	public Optional<RoleModel> findById(Long id) {
		// TODO Auto-generated method stub
		return roleRepo.findById(id);
	}


	@Override
	public List<RoleModel> findAllRole(Integer pageNo, Integer pageSize, String sortBy, String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<RoleModel> pageResult = roleRepo.findAllRole(pageable);
		return pageResult.getContent();
	}

	@Override
	public List<RoleModel> showAll() {
		// TODO Auto-generated method stub
		return roleRepo.showAll();
	}

	@Override
	public Iterable<RoleModel> searchRole(String code, String name) {
		// TODO Auto-generated method stub
		return roleRepo.findByCodeOrName(code, name);
	}


	


	
	


	

}
