package com.xsis.batch23x.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.ScheduleTypeModel;
import com.xsis.batch23x.repositories.ScheduleTypeRepo;
import com.xsis.batch23x.repositories.UndanganRepo;
import com.xsis.batch23x.service.ScheduleTypeService;

@Service
public class SchduleserviceImpl implements ScheduleTypeService {
	
	@Autowired
	private ScheduleTypeRepo scheduletyperepo;

	@Override
	public List<ScheduleTypeModel> showStById(Long idSt) {
		// TODO Auto-generated method stub
		return scheduletyperepo.showStById(idSt);
	}

	@Override
	public List<ScheduleTypeModel> showAllSt() {
		// TODO Auto-generated method stub
		return scheduletyperepo.showAllSt();
	}
	
	

}
