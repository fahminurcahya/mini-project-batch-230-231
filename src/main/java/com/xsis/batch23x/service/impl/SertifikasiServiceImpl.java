package com.xsis.batch23x.service.impl;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.SertifikasiModel;
import com.xsis.batch23x.repositories.SertifikasiRepo;
import com.xsis.batch23x.service.SertifikasiService;

@Service
public class SertifikasiServiceImpl implements SertifikasiService{

	@Autowired
	SertifikasiRepo sertifikasiRepo;

	@Override
	public List<SertifikasiModel> findAllSertifikasi(Integer pageNo, Integer pageSize, String sortBy, String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<SertifikasiModel> pageResult = sertifikasiRepo.findAllSertifikasi(pageable);
		return pageResult.getContent();
	}

	@Override
	public List<SertifikasiModel> findAllSertifikasi() {
		// TODO Auto-generated method stub
		return sertifikasiRepo.findAll();
	}

	@Override
	public SertifikasiModel save(SertifikasiModel Sertifikasi) {
		
		return sertifikasiRepo.save(Sertifikasi);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		sertifikasiRepo.deleteById(id);
	}

	@Override
	public Optional<SertifikasiModel> findById(Long id) {
		
		return sertifikasiRepo.findById(id);
	}

	@Override
	public List<SertifikasiModel> findSertifikasiByIdBio(Long biodataId) {
		// TODO Auto-generated method stub
		return sertifikasiRepo.findSertifikasiByIdBio(biodataId);
	}
	
	
}
