package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.xsis.batch23x.models.SkillLevelModel;
import com.xsis.batch23x.repositories.SkillLevelRepo;
import com.xsis.batch23x.service.SkillLevelService;

@Service
public class SkillLevelServiceImpl implements SkillLevelService{
	
	@Autowired
	private SkillLevelRepo skill_levelrepo;
	
	@Override
	public List<SkillLevelModel> findAllSkillLevel() {
		// TODO Auto-generated method stub
		return skill_levelrepo.findAll();
	}

	@Override
	public SkillLevelModel save(SkillLevelModel skill_level) {
		// TODO Auto-generated method stub
		skill_level.setCreatedBy(Long.valueOf(1));
		skill_level.setCreatedOn(new Date());
		skill_level.setIsDelete(false);
		return skill_levelrepo.save(skill_level);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		skill_levelrepo.deleteById(id);
	}

	@Override
	public List<SkillLevelModel> findSkillLevelById(Long id) {
		// TODO Auto-generated method stub
		return skill_levelrepo.findSkillLevelById(id);
	}

	@Override
	public Optional<SkillLevelModel> findById(Long id) {
		// TODO Auto-generated method stub
		return skill_levelrepo.findById(id);
	}


	
}
