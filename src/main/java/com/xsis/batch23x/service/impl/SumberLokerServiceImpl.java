package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.SumberLokerModel;
import com.xsis.batch23x.repositories.SumberLokerRepo;
import com.xsis.batch23x.service.SumberLokerService;

@Service
public class SumberLokerServiceImpl implements SumberLokerService{

	@Autowired
	SumberLokerRepo slr;
	
	@Override
	public SumberLokerModel save(SumberLokerModel slm) {
		return slr.save(slm);
	}
	
	@Override
	public Optional<SumberLokerModel> findById(Long id){
		return slr.findById(id);
	}
	
	@Override
	public List<SumberLokerModel> findSumberLokerByBio(Long biodataId){
		return slr.findSumberLokerByBio(biodataId);
	}
}
