package com.xsis.batch23x.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.TimePeriodModel;
import com.xsis.batch23x.repositories.TimePeriodRepo;
import com.xsis.batch23x.service.TimePeriodService;

@Service
public class TimePeriodServiceImpl implements TimePeriodService{

	@Autowired
	private TimePeriodRepo tp;
	
	public List<TimePeriodModel> showTimePeriod(){
		return tp.findAll();
	}
}