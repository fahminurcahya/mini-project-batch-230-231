package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.TimesheetApprovalDto;
import com.xsis.batch23x.models.TimesheetModel;
import com.xsis.batch23x.repositories.TimesheetApprovalRepo;
import com.xsis.batch23x.service.TimesheetApprovalService;

@Service
public class TimesheetApprovalServiceImpl implements TimesheetApprovalService {

	@Autowired
	private TimesheetApprovalRepo tar;
	
	@Override
	public TimesheetModel save(TimesheetModel tm) {
		return tar.save(tm);
	}
	
//	@Override
//	public Optional<TimesheetModel> findById(Long id) {
//		// TODO Auto-generated method stub
//		return tsRepo.findById(id);
//	}
		
	@Override
	public Optional<TimesheetApprovalDto> findTimesheetById(Long id){
		return tar.findTimesheetApprovalById(id);
	}
	
	@Override
	public Optional<TimesheetModel> findById(Long id) {
		return tar.findById(id);
	}
	
	@Override
	public List<TimesheetApprovalDto> validasi(Long id, Date tanggal){
		return tar.validasi(id, tanggal);
	}
	
	@Override
	public Long validasi2(Long id, Date tanggal) {
		return tar.validasi2(id, tanggal);
	}
	
	@Override
	public List<TimesheetApprovalDto> showTimesheet(Long id, Integer tahun, Integer bulan){
		return tar.showTimesheet(id, tahun, bulan);
	}
}
