package com.xsis.batch23x.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.TimesheetAssessment;
import com.xsis.batch23x.repositories.TimesheetAssessmentRepo;
import com.xsis.batch23x.service.TimesheetAssessmentService;

@Service
public class TimesheetAssessmentServiceImpl implements TimesheetAssessmentService{

	@Autowired
	private TimesheetAssessmentRepo tar;
	
	@Override
	public TimesheetAssessment save(TimesheetAssessment tsa) {
		return tar.save(tsa);
	}
}
