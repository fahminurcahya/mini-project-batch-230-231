package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.dto.TimesheetSubmissionDto;
import com.xsis.batch23x.models.TimesheetModel;
import com.xsis.batch23x.repositories.TimesheetCollectionRepo;
import com.xsis.batch23x.repositories.TimesheetSubmissionRepo;
import com.xsis.batch23x.service.TimesheetCollectionService;


@Service
public class TimesheetCollectionServiceImpl implements TimesheetCollectionService {
	
	@Autowired
	private TimesheetCollectionRepo TCR;
	
	@Override
	public List<TimesheetDto> showAllForCollection(Long id,String client,String pegawai, Integer bulan, Integer tahun) {
		// TODO Auto-generated method stub
		return TCR.showAllForCollection(id,client,pegawai, bulan, tahun);
	}

	@Override
	public TimesheetModel save(TimesheetModel tsMod) {
		// TODO Auto-generated method stub
		return TCR.save(tsMod);
	}

	@Override
	public Optional<TimesheetDto> findTimesheetById(Long id) {
		// TODO Auto-generated method stub
		return TCR.findTimesheetById(id);
	}
}
