package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.models.RoleModel;
import com.xsis.batch23x.models.TimesheetModel;
import com.xsis.batch23x.repositories.TimesheetRepo;
import com.xsis.batch23x.service.TimesheetService;

@Service
public class TimesheetSeviceImpl implements TimesheetService {

	
	@Autowired
	private TimesheetRepo tsRepo;

	@Override
	public TimesheetModel save(TimesheetModel tsMod) {
		// TODO Auto-generated method stub
		return tsRepo.save(tsMod);
	}
//
//	@Override
//	public Optional<TimesheetModel> findById(Long id) {
//		// TODO Auto-generated method stub
//		return tsRepo.findById(id);
//	}


//	@Override
//	List<TimesheetDto> showTimesheet(Long id, Date date1, Date date2, Integer pageNo, Integer pageSize, String sortBy, String sortType){
//		// TODO Auto-generated method stub
//		return tsRepo.showTimesheet(id, date1, date2);
//	}

	@Override
	public Optional<TimesheetDto> findTimesheetById(Long id) {
		// TODO Auto-generated method stub
		return tsRepo.findTimesheetById(id);
	}

	@Override
	public Optional<TimesheetModel> findById(Long id) {
		// TODO Auto-generated method stub
		return tsRepo.findById(id);
	}

	@Override
	public List<TimesheetDto> validasi(Long id, Date tanggal) {
		// TODO Auto-generated method stub
		return tsRepo.validasi(id, tanggal);
	}

	@Override
	public Long validasi2(Long id, Date tanggal) {
		// TODO Auto-generated method stub
		return tsRepo.validasi2(id, tanggal);
	}
	@Override
	public List<TimesheetDto> showTimesheet(Long id, Date date1, Date date2, Integer pageNo, Integer pageSize,
			String sortBy, String sortType) {
			Pageable pageable =null;
			if (sortType.equalsIgnoreCase("desc")) {
				pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
			}else {
				pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
			}
			
			Page<TimesheetDto> pageResult = tsRepo.showTimesheet(id, date1, date2,pageable);
			return pageResult.getContent();
	}
	@Override
	public List<TimesheetDto> showAll(Long id, Date date1, Date date2) {
		// TODO Auto-generated method stub
		return tsRepo.showAll(id, date1, date2);
	}

		


}
