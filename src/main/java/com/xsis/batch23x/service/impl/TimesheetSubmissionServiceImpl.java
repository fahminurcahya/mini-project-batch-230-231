package com.xsis.batch23x.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.TimesheetDto;
import com.xsis.batch23x.dto.TimesheetSubmissionDto;
import com.xsis.batch23x.models.TimesheetModel;
import com.xsis.batch23x.repositories.TimesheetSubmissionRepo;
import com.xsis.batch23x.service.TimesheetSubmissionService;

@Service
@Transactional
public class TimesheetSubmissionServiceImpl implements TimesheetSubmissionService {
	
	@Autowired
	private TimesheetSubmissionRepo tsSubRep;

	@Override
	public List<TimesheetSubmissionDto> showTimesheet(Long id, Integer bulan, Integer tahun) {
		// TODO Auto-generated method stub
		return tsSubRep.showTimesheet(id, bulan, tahun);
	}

	@Override
	public int submit(Long idAddr, Integer bulan, Integer tahun) {
		// TODO Auto-generated method stub
		return tsSubRep.submit(idAddr, bulan, tahun);
	}

}
