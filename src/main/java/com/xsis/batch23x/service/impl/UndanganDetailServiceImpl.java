package com.xsis.batch23x.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.models.UndanganDetailModel;
import com.xsis.batch23x.repositories.UndanganDetailRepo;
import com.xsis.batch23x.repositories.UndanganRepo;
import com.xsis.batch23x.service.UndanganDetailService;

@Service
public class UndanganDetailServiceImpl implements UndanganDetailService{
	
	@Autowired
	private UndanganDetailRepo undangandetailRepo;

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		undangandetailRepo.deleteById(id);
	}

	@Override
	public UndanganDetailModel save(UndanganDetailModel undangandetailmodel) {
		// TODO Auto-generated method stub
		return undangandetailRepo.save(undangandetailmodel);
	}

	@Override
	public Optional<UndanganDetailModel> showUndanganDetailById(Long idUndangan) {
		// TODO Auto-generated method stub
		return undangandetailRepo.showUndanganDetailById(idUndangan);
	}

	@Override
	public Optional<UndanganDetailModel> findById(Long id) {
		// TODO Auto-generated method stub
		return undangandetailRepo.findById(id);
	}

}
