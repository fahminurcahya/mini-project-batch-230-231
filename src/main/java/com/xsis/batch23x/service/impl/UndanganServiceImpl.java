package com.xsis.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.xsis.batch23x.dto.BiodataDto;
import com.xsis.batch23x.dto.UndanganDto;
import com.xsis.batch23x.models.OrganisasiModel;
import com.xsis.batch23x.models.UndanganModel;
import com.xsis.batch23x.repositories.UndanganRepo;
import com.xsis.batch23x.service.UndanganService;

@Service
public class UndanganServiceImpl implements UndanganService {
	
	@Autowired
	private UndanganRepo undanganRepo;

	@Override
	public List<UndanganDto> showAllUndangan(String fullname) {
		// TODO Auto-generated method stub
		return undanganRepo.showAllUndangan(fullname);
	}

	@Override
	public List<UndanganDto> showNameUndangan(Long educationLevelId,String fullname, Integer pageNo, Integer pageSize, String sortBy,
			String sortType) {
		Pageable pageable=null;
		if (sortType.equalsIgnoreCase("desc")) {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
		}else {
			pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
		}
		
		Page<UndanganDto> pageResult = undanganRepo.showNameUndangan(pageable,educationLevelId,fullname);
		
		return pageResult.getContent();
	}

	@Override
	public List<UndanganDto> showUndanganById(Long idUndangan) {
		// TODO Auto-generated method stub
		return undanganRepo.showUndanganById(idUndangan);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		undanganRepo.deleteById(id);
	}

	@Override
	public Optional<UndanganModel> findById(Long id) {
		// TODO Auto-generated method stub
		return undanganRepo.findById(id);
	}

	@Override
	public UndanganModel save(UndanganModel undanganmodel) {
		// TODO Auto-generated method stub
		return undanganRepo.save(undanganmodel);
	}

	@Override
	public List<UndanganDto> CekJadwalRoTro() {
		// TODO Auto-generated method stub
		return undanganRepo.CekJadwalRoTro();
	}

	@Override
	public List<UndanganDto> showDetailUndangan(Long idP) {
		// TODO Auto-generated method stub
		return undanganRepo.showDetailUndangan(idP);
	}

	@Override
	public List<UndanganModel> viewForLastid() {
		// TODO Auto-generated method stub
		return undanganRepo.viewForLastid();
	}

	
	

	

	
	

}
